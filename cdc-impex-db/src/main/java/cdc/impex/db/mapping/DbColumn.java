package cdc.impex.db.mapping;

import java.util.function.Function;

import cdc.util.lang.Checks;

/**
 * Description of a DB column.
 * <p>
 * It contains:
 * <ul>
 * <li>The column name.
 * <li>The <em>memory</em> data type of the column, which may differ from the <em>storage</em> data type.
 * <li>A converter from <em>storage</em> data type to <em>memory</em> data type.
 * <li>A converter from <em>memory</em> data type to <em>storage</em> data type.
 * <li>A boolean indicating whether this column is a key column.
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <T> The memory data type. It may differ from the DB type.
 */
public final class DbColumn<T> {
    private final String name;
    private final Class<T> dataType;
    private final Function<T, Object> importConverter;
    private final Function<Object, T> exportConverter;
    private final boolean isKey;

    private DbColumn(String name,
                     Class<T> dataType,
                     Function<T, Object> importConverter,
                     Function<Object, T> exportConverter,
                     boolean isKey) {
        this.name = Checks.isNotNull(name, "name");
        this.dataType = Checks.isNotNull(dataType, "dataType");
        this.importConverter = importConverter == null ? x -> x : importConverter;
        this.exportConverter = exportConverter == null ? dataType::cast : exportConverter;
        this.isKey = isKey;
    }

    /**
     * @return The column name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The column data type.
     */
    public Class<T> getDataType() {
        return dataType;
    }

    /**
     * @return the converter used to convert data type to DB type.
     */
    public Function<T, Object> getImportConverter() {
        return importConverter;
    }

    /**
     * @return the converter used to convert DB type to data type.
     */
    public Function<Object, T> getExportConverter() {
        return exportConverter;
    }

    public Object importConvert(T value) {
        return importConverter.apply(value);
    }

    public Object importConvertRaw(Object value) {
        return importConverter.apply(dataType.cast(value));
    }

    public T exportConvert(Object value) {
        return exportConverter.apply(value);
    }

    public boolean isKey() {
        return isKey;
    }

    @Override
    public String toString() {
        return "[" + getName()
                + " " + getDataType().getSimpleName()
                + "]";
    }

    public static <T> Builder<T> builder(String name,
                                         Class<T> dataType) {
        return new Builder<>(name, dataType);
    }

    public static final class Builder<T> {
        private final String name;
        private final Class<T> dataType;
        private Function<T, Object> importConverter;
        private Function<Object, T> exportConverter;
        private boolean isKey = false;

        private Builder(String name,
                        Class<T> dataType) {
            this.name = name;
            this.dataType = dataType;
        }

        public Builder<T> importConverter(Function<T, Object> converter) {
            this.importConverter = converter;
            return this;
        }

        public Builder<T> exportConverter(Function<Object, T> converter) {
            this.exportConverter = converter;
            return this;
        }

        public Builder<T> isKey(boolean isKey) {
            this.isKey = isKey;
            return this;
        }

        public Builder<T> isKey() {
            return isKey(true);
        }

        public DbColumn<T> build() {
            return new DbColumn<>(name,
                                  dataType,
                                  importConverter,
                                  exportConverter,
                                  isKey);
        }
    }
}