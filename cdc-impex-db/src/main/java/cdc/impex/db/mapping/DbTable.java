package cdc.impex.db.mapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.rdb.RdbColumnOrder;
import cdc.rdb.RdbColumnSorting;
import cdc.util.lang.Checks;

/**
 * Description of a DB table.
 * <p>
 * It contains:
 * <ul>
 * <li>The table schema.
 * <li>The table name.
 * <li>The table columns.
 * <li>Optional sorting constraints on some columns.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class DbTable {
    private final String schema;
    private final String name;
    private final List<DbColumn<?>> columns;
    private final Map<String, DbColumn<?>> nameToColumn = new HashMap<>();
    private final List<RdbColumnSorting> sortings;

    private DbTable(String schema,
                    String name,
                    List<DbColumn<?>> columns,
                    List<RdbColumnSorting> sortings) {
        this.schema = schema;
        this.name = Checks.isNotNull(name, "name");
        this.columns = Collections.unmodifiableList(columns);
        this.sortings = Collections.unmodifiableList(sortings);

        for (final DbColumn<?> column : columns) {
            if (column == null) {
                throw new IllegalArgumentException("Null column");
            }
            nameToColumn.put(column.getName(), column);
        }
        if (nameToColumn.size() != columns.size()) {
            throw new IllegalArgumentException("Duplicate column names");
        }
    }

    /**
     * @return The DB schema name.
     */
    public String getSchema() {
        return schema;
    }

    /**
     * @return The DB table name
     */
    public String getName() {
        return name;
    }

    /**
     * @return The columns of this table.
     */
    public List<DbColumn<?>> getColumns() {
        return columns;
    }

    public List<DbColumn<?>> getKeyColumns() {
        final List<DbColumn<?>> list = new ArrayList<>();
        for (final DbColumn<?> column : columns) {
            if (column.isKey()) {
                list.add(column);
            }
        }
        return list;
    }

    public List<DbColumn<?>> getNonKeyColumns() {
        final List<DbColumn<?>> list = new ArrayList<>();
        for (final DbColumn<?> column : columns) {
            if (!column.isKey()) {
                list.add(column);
            }
        }
        return list;
    }

    public List<String> getColumnsNames() {
        final List<String> list = new ArrayList<>();
        for (final DbColumn<?> column : columns) {
            list.add(column.getName());
        }
        return list;
    }

    public List<String> getKeyColumnsNames() {
        final List<String> list = new ArrayList<>();
        for (final DbColumn<?> column : columns) {
            if (column.isKey()) {
                list.add(column.getName());
            }
        }
        return list;
    }

    public List<String> getNonKeyColumnsNames() {
        final List<String> list = new ArrayList<>();
        for (final DbColumn<?> column : columns) {
            if (!column.isKey()) {
                list.add(column.getName());
            }
        }
        return list;
    }

    /**
     * @return The sortings of columns.
     */
    public List<RdbColumnSorting> getSortings() {
        return sortings;
    }

    /**
     * Returns the column that has a given name.
     *
     * @param name The name.
     * @return The column that is named {@code name}.
     * @throws IllegalArgumentException When no column named {@code name } is found.
     */
    public DbColumn<?> getColumn(String name) {
        final DbColumn<?> result = nameToColumn.get(name);
        if (result == null) {
            throw new IllegalArgumentException("Invalid column name '" + name + "' for table '" + this.name + "'");
        } else {
            return result;
        }
    }

    /**
     * @param name The name.
     * @return {@code true} if {@code name} is the name of a column of this table.
     */
    public boolean containsColumn(String name) {
        return nameToColumn.containsKey(name);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String schema;
        private String name;
        private final List<DbColumn<?>> columns = new ArrayList<>();
        private final List<RdbColumnSorting> sortings = new ArrayList<>();

        private Builder() {
        }

        public Builder schema(String schema) {
            this.schema = schema;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder column(DbColumn<?> column) {
            this.columns.add(column);
            return this;
        }

        public Builder column(DbColumn<?> column,
                              RdbColumnOrder order) {
            this.columns.add(column);
            this.sortings.add(new RdbColumnSorting(column.getName(), order));
            return this;
        }

        public DbTable build() {
            return new DbTable(schema,
                               name,
                               columns,
                               sortings);
        }
    }
}