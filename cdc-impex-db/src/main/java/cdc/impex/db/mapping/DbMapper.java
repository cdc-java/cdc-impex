package cdc.impex.db.mapping;

import java.util.HashMap;
import java.util.Map;

import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.util.lang.Checks;

/**
 * Mapping between a Sheet template and a DB table.
 *
 * @author Damien Carbonne
 */
public final class DbMapper {
    private final DbTable table;
    private final SheetTemplate template;
    private final Map<String, String> dbToTemplate = new HashMap<>();
    private final Map<String, String> templateToDB = new HashMap<>();

    private DbMapper(DbTable table,
                     SheetTemplate template,
                     Map<String, String> dbToTemplate) {
        this.table = Checks.isNotNull(table, "table");
        this.template = Checks.isNotNull(template, "template");

        // Check that names are valid and their are no duplicates
        for (final Map.Entry<String, String> entry : dbToTemplate.entrySet()) {
            final String dbName = entry.getKey();
            final String tpName = entry.getValue();
            Checks.assertFalse(dbName == null, "null db column name");
            Checks.assertFalse(tpName == null, "null template column name");
            Checks.assertFalse(this.templateToDB.containsKey(tpName), "Duplicate mapping to template column '{}'", tpName);
            Checks.assertTrue(table.containsColumn(dbName), "Invalid DB column name '{}'", dbName);
            Checks.assertTrue(template.containsColumn(tpName), "Invalid template column name '{}'", tpName);
            this.dbToTemplate.put(dbName, tpName);
            this.templateToDB.put(tpName, dbName);
        }

        // Check that all template columns are mapped
        // Check that types are consistent
        for (final ColumnTemplate<?> tpColumn : template.getColumns()) {
            if (!template.isActionColumn(tpColumn)) {
                Checks.assertTrue(templateToDB.containsKey(tpColumn.getName()), // FIXME
                                  "No DB column mapped to template column '{}'",
                                  tpColumn.getName());
                final DbColumn<?> dbColumn = table.getColumn(templateToDB.get(tpColumn.getName())); // FIXME
                Checks.assertTrue(tpColumn.getDataType().equals(dbColumn.getDataType()),
                                  "Non consistant types for '{}' and '{}'",
                                  tpColumn.getName(),
                                  dbColumn.getName());
            }
        }
    }

    public DbTable getTable() {
        return table;
    }

    public SheetTemplate getTemplate() {
        return template;
    }

    /**
     * Returns the column template name corresponding to a DB coluln name.
     *
     * @param dbColumnName The DB column name.
     * @return The column template name corresponding to {@code dbColumnName}.
     */
    public String toTemplateName(String dbColumnName) {
        return dbToTemplate.getOrDefault(dbColumnName, dbColumnName);
    }

    /**
     * Returns the DB column name corresponding to a column template name.
     *
     * @param columnTemplateName The column template name.
     * @return The DB column name corresponding to {@code columnTemplateName}.
     */
    public String toDBName(String columnTemplateName) {
        return templateToDB.getOrDefault(columnTemplateName, columnTemplateName);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private DbTable table;
        private SheetTemplate template;
        private final Map<String, String> dbToTemplate = new HashMap<>();

        private Builder() {
        }

        public Builder table(DbTable table) {
            this.table = table;
            return this;
        }

        public Builder template(SheetTemplate template) {
            this.template = template;
            return this;
        }

        public Builder dbToTemplate(String dbColumnName,
                                    String templateColumnName) {
            this.dbToTemplate.put(dbColumnName, templateColumnName);
            return this;
        }

        public Builder map(DbColumn<?> dbColumn,
                           ColumnTemplate<?> templateColumn) {
            this.dbToTemplate.put(dbColumn.getName(), templateColumn.getName()); // FIXME
            return this;
        }

        public DbMapper build() {
            return new DbMapper(table,
                                template,
                                dbToTemplate);
        }
    }
}