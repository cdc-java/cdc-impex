package cdc.impex.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.db.mapping.DbColumn;
import cdc.impex.db.mapping.DbMapper;
import cdc.impex.imports.ImportIssueType;
import cdc.impex.imports.ImportIssues;
import cdc.impex.imports.ImportRow;
import cdc.impex.imports.SheetImporter;
import cdc.impex.templates.ImportAction;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesHandler;
import cdc.rdb.RdbHelper;
import cdc.util.lang.Checks;
import cdc.util.lang.ExceptionWrapper;

public final class DbSheetImporter implements SheetImporter {
    private static final Logger LOGGER = LogManager.getLogger(DbSheetImporter.class);

    private final DbMapper mapper;
    private final Supplier<Connection> connectionSupplier;
    private final Optional<ImportAction> defaultAction;
    private RdbHelper helper;
    private PreparedStatement insertPS;
    private PreparedStatement deletePS;
    private PreparedStatement updatePS;

    private DbSheetImporter(DbMapper mapper,
                            Supplier<Connection> connectionSupplier,
                            ImportAction defaultAction) {
        this.mapper = Checks.isNotNull(mapper, "mapper");
        this.connectionSupplier = Checks.isNotNull(connectionSupplier, "connectionSupplier");
        this.defaultAction = Optional.ofNullable(defaultAction);
    }

    public static DbSheetImporter create(DbMapper mapper,
                                         Supplier<Connection> connectionSupplier,
                                         ImportAction defaultAction) {
        return new DbSheetImporter(mapper, connectionSupplier, defaultAction);
    }

    private void cleanup() {
        helper = null;
        try (PreparedStatement x = insertPS) {
            insertPS = null;
        } catch (final SQLException e) {
            // Ignore
        }
        try (PreparedStatement x = deletePS) {
            deletePS = null;
        } catch (final SQLException e) {
            // Ignore
        }
        try (PreparedStatement x = updatePS) {
            updatePS = null;
        } catch (final SQLException e) {
            // Ignore
        }
    }

    @Override
    public void beginSheetImport(String systemId,
                                 String sheetName,
                                 SheetTemplate template,
                                 IssuesHandler<Issue> issuesHandler) {
        Checks.assertTrue(template == mapper.getTemplate(), "Unexpected template");
        final Connection connection = connectionSupplier.get();
        try {
            this.helper = new RdbHelper(connection);
            this.insertPS = connection.prepareStatement(helper.getInsertIntoTableQuery(mapper.getTable().getSchema(),
                                                                                       mapper.getTable().getName(),
                                                                                       mapper.getTable().getColumnsNames()));
            this.deletePS = connection.prepareStatement(helper.getDeleteFromTableQuery(mapper.getTable().getSchema(),
                                                                                       mapper.getTable().getName(),
                                                                                       mapper.getTable().getKeyColumnsNames()));
            this.updatePS = connection.prepareStatement(helper.getUpdateTableQuery(mapper.getTable().getSchema(),
                                                                                   mapper.getTable().getName(),
                                                                                   mapper.getTable().getNonKeyColumnsNames(),
                                                                                   mapper.getTable().getKeyColumnsNames()));
        } catch (final SQLException e) {
            LOGGER.catching(e);
            throw new ExceptionWrapper(e);
        }
    }

    private void processCreate(ImportRow row,
                               IssuesHandler<Issue> issuesHandler) {
        try {
            int index = 1;
            for (final DbColumn<?> column : mapper.getTable().getColumns()) {
                final Object value = row.getDataOrNull(mapper.toTemplateName(column.getName()));
                final Object dbValue = column.importConvertRaw(value);
                insertPS.setObject(index, dbValue);
                index++;
            }
            insertPS.execute();
            if (insertPS.getUpdateCount() != 1) {
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.APP_FAILURE)
                                                .description("Failed to execute insert.")
                                                .severity(IssueSeverity.CRITICAL)
                                                .addLocation(row.getLocation())
                                                .build());
            }
        } catch (final SQLException e) {
            issuesHandler.issue(ImportIssues.builder()
                                            .name(ImportIssueType.APP_FAILURE)
                                            .description("Failed to execute insert. " + e.getMessage())
                                            .severity(IssueSeverity.CRITICAL)
                                            .addLocation(row.getLocation())
                                            .build());
        }
    }

    private void processDelete(ImportRow row,
                               IssuesHandler<Issue> issuesHandler) {
        try {
            int index = 1;
            for (final DbColumn<?> column : mapper.getTable().getKeyColumns()) {
                final Object value = row.getDataOrNull(mapper.toTemplateName(column.getName()));
                final Object dbValue = column.importConvertRaw(value);
                deletePS.setObject(index, dbValue);
                index++;
            }
            deletePS.execute();
            if (deletePS.getUpdateCount() != 1) {
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.APP_FAILURE)
                                                .description("Failed to execute delete.")
                                                .severity(IssueSeverity.CRITICAL)
                                                .addLocation(row.getLocation())
                                                .build());
            }
        } catch (final SQLException e) {
            issuesHandler.issue(ImportIssues.builder()
                                            .name(ImportIssueType.APP_FAILURE)
                                            .description("Failed to execute delete. " + e.getMessage())
                                            .severity(IssueSeverity.CRITICAL)
                                            .addLocation(row.getLocation())
                                            .build());
        }
    }

    private void processUpdate(ImportRow row,
                               IssuesHandler<Issue> issuesHandler) {
        try {
            int index = 1;
            for (final DbColumn<?> column : mapper.getTable().getNonKeyColumns()) {
                final Object value = row.getDataOrNull(mapper.toTemplateName(column.getName()));
                final Object dbValue = column.importConvertRaw(value);
                updatePS.setObject(index, dbValue);
                index++;
            }

            for (final DbColumn<?> column : mapper.getTable().getKeyColumns()) {
                final Object value = row.getDataOrNull(mapper.toTemplateName(column.getName()));
                final Object dbValue = column.importConvertRaw(value);
                updatePS.setObject(index, dbValue);
                index++;
            }
            updatePS.execute();
            if (updatePS.getUpdateCount() != 1) {
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.APP_FAILURE)
                                                .description("Failed to execute update.")
                                                .severity(IssueSeverity.CRITICAL)
                                                .addLocation(row.getLocation())
                                                .build());
            }
        } catch (final SQLException e) {
            issuesHandler.issue(ImportIssues.builder()
                                            .name(ImportIssueType.APP_FAILURE)
                                            .description("Failed to execute update " + e.getMessage())
                                            .severity(IssueSeverity.CRITICAL)
                                            .addLocation(row.getLocation())
                                            .build());
        }
    }

    @Override
    public void importHeader(SheetTemplateInstance templateInstance,
                             IssuesHandler<Issue> issuesHandler) {
        // TODO
    }

    @Override
    public void importRow(ImportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        switch (row.getAction(defaultAction)) {
        case CREATE:
            processCreate(row, issuesHandler);
            break;
        case DELETE:
            processDelete(row, issuesHandler);
            break;
        case IGNORE:
            // Nothing to do
            break;
        case UPDATE:
            processUpdate(row, issuesHandler);
            break;
        }
    }

    @Override
    public void endSheetImport(String systemId,
                               String sheetName,
                               SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        cleanup();
    }
}