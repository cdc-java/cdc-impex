package cdc.impex.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.db.mapping.DbMapper;
import cdc.impex.exports.ExportRow;
import cdc.impex.exports.SheetExporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.rdb.RdbHelper;
import cdc.util.lang.Checks;
import cdc.util.lang.ExceptionWrapper;

/**
 * Implementation of {@link SheetExporter} dedicated to a database table.
 *
 * @author Damien Carbonne
 */
public final class DbSheetExporter implements SheetExporter {
    private static final Logger LOGGER = LogManager.getLogger(DbSheetExporter.class);
    private final DbMapper mapper;
    private final Supplier<Connection> connectionSupplier;
    private RdbHelper helper;
    private Statement statement;
    /** The ResultSet used to extract data from table. */
    private ResultSet rs;
    /** true if rs has more data. */
    private boolean hasMore = false;
    /** The total number of rows to extract. */
    private long total = -1L;
    /** The number of extracted rows. */
    private long count = 0L;

    private DbSheetExporter(DbMapper mapper,
                            Supplier<Connection> connectionSupplier) {
        this.mapper = Checks.isNotNull(mapper, "mapper");
        this.connectionSupplier = Checks.isNotNull(connectionSupplier, "connectionSupplier");
    }

    public static DbSheetExporter create(DbMapper mapper,
                                         Supplier<Connection> connectionSupplier) {
        return new DbSheetExporter(mapper, connectionSupplier);
    }

    private void cleanup() {
        helper = null;
        try (Statement x = statement) {
            statement = null;
        } catch (final SQLException e) {
            // Ignore
        }
        try (ResultSet x = rs) {
            rs = null;
        } catch (final SQLException e) {
            // Ignore
        }
    }

    private String getSelectClause() {
        return helper.getSelectClause(mapper.getTable().getSchema(),
                                      mapper.getTable().getName());
    }

    private String getOrderClause() {
        return helper.getOrderClause(mapper.getTable().getSortings());
    }

    @Override
    public void beginSheetExport(SheetTemplateInstance templateInstance,
                                 IssuesHandler<Issue> issuesHandler) {
        Checks.assertTrue(templateInstance.getTemplate() == mapper.getTemplate(), "Unexpected template");
        final Connection connection = connectionSupplier.get();
        try {
            this.helper = new RdbHelper(connection);
            final String query = getSelectClause() + getOrderClause();
            LOGGER.debug("query: {}", query);
            this.statement = connection.createStatement();
            this.rs = statement.executeQuery(query);
            // TODO check compliance of template with rs
            // Move to first row
            this.hasMore = rs.next();
            // Compute total
            this.total = helper.getTableSize(connection,
                                             mapper.getTable().getSchema(),
                                             mapper.getTable().getName());
        } catch (final SQLException e) {
            LOGGER.catching(e);
            cleanup();
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public int getNumberOfRemainingRows() {
        if (hasMore) {
            if (total >= 0) {
                final long rem = total - count;
                final int res = (int) rem;
                if (res == rem) {
                    return res;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }

    @Override
    public void exportRow(ExportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        if (hasMore) {
            count++;
            // Extract current row
            try {
                for (final ColumnTemplate<?> column : mapper.getTemplate().getColumns()) {
                    if (!mapper.getTemplate().isActionColumn(column)) {
                        final String dbName = mapper.toDBName(column.getName()); // FIXME
                        final Object object = rs.getObject(dbName);
                        final Object data = mapper.getTable().getColumn(dbName).getExportConverter().apply(object);
                        row.setData(column.getName(), data); // FIXME
                    }
                }
                // Move to next row
                hasMore = rs.next();
            } catch (final SQLException e) {
                hasMore = false;
                cleanup();
                throw new ExceptionWrapper(e);
            }
        }
    }

    @Override
    public void endSheetExport(SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        cleanup();
    }
}