package cdc.impex.db;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.function.Function;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.impex.ImpExCatalog;
import cdc.impex.db.mapping.DbColumn;
import cdc.impex.db.mapping.DbMapper;
import cdc.impex.db.mapping.DbTable;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.Usage;
import cdc.rdb.RdbColumnOrder;
import cdc.rdb.RdbHelper;

class DbIOSupport {
    private static final Logger LOGGER = LogManager.getLogger(DbIOSupport.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    static final ColumnTemplate<Integer> NUM =
            ColumnTemplate.builder(Integer.class)
                          .name("Number")
                          .importConverter(Integer::valueOf)
                          .usage(Usage.MANDATORY_RO_ID)
                          .build();
    static final ColumnTemplate<String> ADDR =
            ColumnTemplate.builder(String.class)
                          .name("Address")
                          .importConverter(Function.identity())
                          .build();
    static final SheetTemplate LOC =
            SheetTemplate.builder()
                         .domain("Impex")
                         .name("Location")
                         .column(NUM)
                         .column(ADDR)
                         .build();

    static final DbColumn<Integer> DB_NUM =
            DbColumn.builder("NUM", Integer.class)
                    .exportConverter(o -> ((Integer) o).intValue())
                    .isKey()
                    .build();
    static final DbColumn<String> DB_ADDR =
            DbColumn.builder("ADDR", String.class)
                    .build();
    static final DbTable DB_LOC =
            DbTable.builder()
                   .name("LOCATION")
                   .schema("CDC")
                   .column(DB_NUM, RdbColumnOrder.ASCENDING)
                   .column(DB_ADDR)
                   .build();

    static final DbMapper MAPPER_LOC =
            DbMapper.builder()
                    .table(DB_LOC)
                    .template(LOC)
                    .map(DB_NUM, NUM)
                    .map(DB_ADDR, ADDR)
                    .build();

    static class ConnectionHolder {
        private Connection connection;

        public Connection getConnection() {
            return connection;
        }

        public void setConnection(Connection connection) {
            this.connection = connection;
        }
    }

    static final String PROTOCOL = "jdbc:derby:";

    final ConnectionHolder connectionHolder = new ConnectionHolder();
    final DbSheetExporter exportLoc = DbSheetExporter.create(MAPPER_LOC, connectionHolder::getConnection);
    final DbSheetImporter importLoc = DbSheetImporter.create(MAPPER_LOC, connectionHolder::getConnection, null);
    final ImpExCatalog catalog = new ImpExCatalog();

    DbIOSupport() {
        LOGGER.info("DbIOSupport()");
        catalog.register(LOC, exportLoc);
        catalog.register(LOC, importLoc);
    }

    Connection createDb(String dbName) throws SQLException {
        LOGGER.info("Create DB {}", dbName);
        System.setProperty("derby.system.home", "target/derby");

        // final String framework = "embedded";
        final Properties props = new Properties();
        props.put("user", "cdc");
        props.put("password", "cdc");

        final Connection connection = DriverManager.getConnection(PROTOCOL + dbName + ";create=true", props);
        LOGGER.info("Created DB");
        connectionHolder.setConnection(connection);
        connection.setAutoCommit(false);

        return connection;
    }

    static void fillDb(Connection connection,
                       int size) throws SQLException {
        try (final Statement s = connection.createStatement()) {
            s.execute("CREATE TABLE location(num INT PRIMARY KEY NOT NULL, addr VARCHAR(40))");
            LOGGER.info("Created Table location");

            try (final PreparedStatement psInsert = connection.prepareStatement("INSERT INTO location VALUES (?, ?)")) {
                for (int index = 0; index < size; index++) {
                    psInsert.setInt(1, index);
                    psInsert.setString(2, "String " + index);
                    psInsert.executeUpdate();
                }
            }
            connection.commit();
            dumpTable(connection);
        }
    }

    static void dumpTable(Connection connection) throws SQLException {
        LOGGER.info("Query data location");
        try (final Statement s = connection.createStatement()) {
            try (ResultSet rs = s.executeQuery("SELECT * FROM location ORDER BY num")) {
                RdbHelper.print(rs, OUT);
            }
        }
    }

    static void dropTable(Connection connection) throws SQLException {
        LOGGER.info("Drop Table location");
        try (final Statement s = connection.createStatement()) {
            s.execute("DROP TABLE location");
        }
        connection.commit();
        LOGGER.info("Dropped Table");
    }

    static void shutdown() {
        try {
            DriverManager.getConnection(PROTOCOL + ";shutdown=true");
        } catch (final SQLException se) {
            if (((se.getErrorCode() == 50000)
                    && ("XJ015".equals(se.getSQLState())))) {
                // we got the expected exception
                LOGGER.info("Derby shut down normally");
                // Note that for single database shutdown, the expected
                // SQL state is "08006", and the error code is 45000.
            } else {
                // if the error code or SQLState is different, we have
                // an unexpected exception (shutdown failed)
                LOGGER.error("Derby did not shut down normally");
                LOGGER.catching(se);
            }
        }
    }
}