package cdc.impex.db;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.Importer;
import cdc.impex.imports.VerboseWorkbookImporter;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.VoidIssuesHandler;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;

class DbImportTest extends DbIOSupport {
    private static final Logger LOGGER = LogManager.getLogger(DbImportTest.class);

    @Test
    void test() throws SQLException, IOException {
        final String dbName = "ImpExImport";

        try (final Connection conn = createDb(dbName)) {
            fillDb(conn, 40);

            final IssuesCollector<Issue> issuesHandler =
                    new IssuesCollector<>(VoidIssuesHandler.INSTANCE);

            final File file = new File("src/test/resources/impex.xlsx");
            final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
            final Importer importer = factory.createImporter(file);

            try {
                importer.importData(file,
                                    catalog.getTemplatesAsSet(LOC.getName()),
                                    new VerboseWorkbookImporter(catalog.createWorkbookImporterFor(LOC.getName())),
                                    issuesHandler,
                                    ProgressController.VERBOSE);
            } catch (final RuntimeException e) {
                LOGGER.catching(e);
            }

            dumpTable(conn);

            LOGGER.info("Save issues");
            IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                              OutSettings.ALL_DATA_ANSWERS,
                              new File("target/import-issues.xlsx"),
                              ProgressController.VOID,
                              IssuesIoFactoryFeatures.UTC_FASTEST);

            dropTable(conn);
            // shutdown();

            assertTrue(true);
        }
    }
}