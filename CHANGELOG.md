# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.55.1] - 2025-02-23
### Changed
- Updated dependencies:
    - cdc-issues-0.66.0
    - cdc-office-0.60.1
    - org.junit-5.12.0


## [0.55.0] - 2025-02-08
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - cdc-issues-0.65.0
    - cdc-office-0.60.0
- Show default value in comments of column templates. #70

### Added
- Added workbook, sheet an column meta data in generated import and export issues. #72
- Added builders to ease ceration of ColumnTemplates of Lists and Sets. #71

### Fixed
- Do not throw an exception during import when there are more cells in a row than declared in header. #66


## [0.54.0] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-io-0.53.1
    - cdc-issues-0.63.0
    - cdc-kernel-0.51.3
    - cdc-office-0.58.1
    - cdc-rdb-0.52.3
    - cdc-ui-0.31.3
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3
    
### Removed
- Removed code that was deprecated in 2023 (ExportIssue and ImportIssue).


## [0.53.2] - 2024-09-28
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-issues-0.61.0
    - cdc-kernel-0.51.2
    - cdc-office-0.57.2
    - cdc-rdb-0.52.2
    - cdc-ui-0.31.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - org.junit-5.11.1


## [0.53.1] - 2024-07-22
### Changed
- Updated dependencies:
    - cdc-issues-0.60.0
    - cdc-office-0.57.1


## [0.53.0] - 2024-07-21
### Changed
- **Breaking:** added support of pattern columns in import and export.  
  In several places `SheetTemplate` has been replaced by `SheetTemplateInstance` in order to provide the actual Header. #57
- Updated dependencies:
    - cdc-office-0.57.0

### Removed
- Removed code that was deprecated since 2022.

### Fixed
- Check default action before giving up when action column is missing and `IGNORE_MISSING_ACTION_COLUMN` is not set.


## [0.52.0] - 2024-06-29
### Added
- Default import action can now be set in `SheetTemplate` or globally in `ImpExFactoryFeatures`.  
  Previously it was alway ̀`IGNORE`. #62
- Added `ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN` to skip generation
  of action column in export and template generation. #62

### Changed
- Updated dependencies:
    - cdc-office-0.56.0
    - commons-cli-1.8.0

### Deprecated
- Deprecated `ImportRow.getAction()`. #62


## [0.51.3] - 2024-05-19
### Changed
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-issues-0.59.1
    - cdc-kernel-0.51.1
    - cdc-office-0.55.0
    - cdc-rdb-0.52.1
    - cdc-ui-0.31.1
    - cdc-util-0.52.1
    - commons-cli-1.7.0
- Updated maven plugins.


## [0.51.2] - 2024-04-17
### Changed
- Updated dependencies:
    - cdc-io-0.51.2
    - cdc-issues-0.59.0
    - cdc-office-0.54.0
    - org.junit-5.10.2
    - org.apache.log4j-2.23.1


## [0.51.1] - 2024-01-21
### Changed
- Updated dependencies:
    - cdc-issues-0.55.0
    - cdc-office-0.53.0
    - cdc-rdb-0.51.1


## [0.51.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-issues-0.52.0
    - cdc-kernel-0.51.0
    - cdc-office-0.52.0
    - cdc-rdb-0.51.0
    - cdc-ui-0.31.0
    - cdc-util-0.52.0
- Added `exec()` and call `System.exit()` in `ImpExToolbox`.
- Updated maven plugins.


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-issues-0.50.0
    - cdc-kernel-0.50.0
    - cdc-office-0.50.0
    - cdc-rdb-0.50.0
    - cdc-ui-0.30.0
    - cdc-utils-0.50.0

### Fixed
- Do not generate content validation for explicit lists that are too long. #61 


## [0.26.2] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-issues-0.34.0
    - cdc-kernel-0.23.2
    - cdc-office-0.31.0
    - cdc-rdb-0.23.0
    - cdc-ui-0.20.15
    - cdc-utils-0.33.2
    - derby-10.16.1.1
    - org.junit-5.10.1


## [0.26.1] - 2023-10-22
### Changed
- Updated dependencies:
    - cdc-issues-0.33.0


## [0.26.0] - 2023-10-21
### Added
- Created `ExportIssues` and  `ImportIssues`. #60

### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-issues-0.32.0
    - cdc-kernel-0.23.1
    - cdc-office-0.30.2
    - cdc-rdb-0.22.7
    - cdc-ui-0.20.14
    - cdc-utils-0.33.1
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom.
- In all possible places, used `Issue` instead of `ExportIssue` or `ImportIssue`. #60

### Removed
- Removed `ExportIssue` and `ImportIssue`. #60


## [0.25.0] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-issues-0.31.1
    - cdc-kernel-0.22.1
    - cdc-office-0.30.1
    - cdc-rdb-0.22.6
    - cdc-ui-0.20.13
    - cdc-utils-0.33.0

### Removed
- Removed code that was deprecated 2022-07-03.


## [0.24.5] - 2023-04-22
### Changed
- Updated dependencies:
    - cdc-issues-0.30.0
    - cdc-kernel-0.21.4
    - cdc-office-0.29.4
    - cdc-rdb-0.22.5
    - cdc-ui-0.20.12


## [0.24.4] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-io-0.26.0
    - cdc-issues-0.29.0
    - cdc-kernel-0.21.3
    - cdc-office-0.29.3
    - cdc-rdb-0.22.4
    - cdc-ui-0.20.11


## [0.24.3] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-issues-0.27.0
    - cdc-kernel-0.21.2
    - cdc-office-0.29.1
    - cdc-rdb-0.22.3
    - cdc-ui-0.20.10
    - cdc-utils-0.31.0
    - org.apache.log4j-2.20.0


## [0.24.2] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-issues-0.26.0
    - cdc-kernel-0.21.1
    - cdc-office-0.29.0
    - cdc-rdb-0.22.2
    - cdc-ui-0.20.9
    - cdc-utils-0.29.0
    - org.junit-5.9.2


## [0.24.1] - 2023-01-02
### Changed
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-issues-0.25.0
    - cdc-kernel-0.21.0
    - cdc-office-0.28.0
    - cdc-rdb-0.22.1
    - cdc-ui-0.20.8
    - cdc-utils-0.28.2


## [0.24.0] - 2022-11-12
### Added
- Added `name(String)` to `ColumnTemplate.Builder`.

### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-issues-0.24.0
    - cdc-kernel-0.20.7
    - cdc-office-0.27.0
    - cdc-rdb-0.22.0
    - cdc-ui-0.20.7
    - cdc-utils-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1
    
### Deprecated
- Deprecated some `ColumnTemplate.Builder` constructors.  
  For example, instead of using `ColumnTemplate.builder(name, class)`,
  one must use `ColumnTemplate.builder(class).name(name)`.


## [0.23.1] - 2022-08-24
### Changed
- Merged `README (Sheets)` and `README (Columns)` into a single `README` sheet. #56
- Added an early check for invalid description in `WorkbookImporterImpl`.
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-issues-0.23.4
    - cdc-kernel-0.20.6
    - cdc-office-0.24.0
    - cdc-rdb-0.21.2
    - cdc-ui-0.20.6
    - cdc-utils-0.28.0
    - org.junit-5.9.0


## [0.23.0] - 2022-07-08
### Changed
- Changed definition of `Usage` to support more cases. Old values been deprecated and will be removed. #53
- Improved `ColumnTemplate` comment.
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-issues-0.23.3
    - cdc-kernel-0.20.5
    - cdc-office-0.23.1
    - cdc-rdb-0.21.1
    - cdc-ui-0.20.5
    - cdc-utils-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1

### Added
- Added new `ColumnTemplate.Builder` and `SheetTemplate.Builder` functions.
- Added generation of a README sheet in templates, and exports (enabling `Hint.ADD_README`). #54

### Fixed
- Do not use unsupported features in `WorkbookStreamExporter`.
- Auto-filter is now there on template sheets. #55


## [0.22.0] - 2022-06-18
### Added
- Added computation of table size in `DbSheetExporter`. Now progress events are more precise. #28
- Created `DbSheetImporter` that can be used to import data to a DB. #28
- Added `getSheetName` and `getLocation` to `ImportRow`, news methods to `DbTable` and `DbColumn`. #28
- Added a default value to `ColumnTemplate` and created `ImportRow.getDataOrDef()`. #31
- Added content validation to `ColumnTemplate`.  
  Default content validation is automatically created for primitive types and enums. #33

### Changed
- Better handling of application errors in `WorkbookImporterImpl`.
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-issues-0.23.2
    - cdc-kernel-0.20.4
    - cdc-office-0.23.0
    - cdc-rdb-0.20.4
    - cdc-ui-0.20.4
    - cdc-utils-0.26.0


## [0.21.1] - 2022-05-21
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-issues-0.23.1
    - cdc-kernel-0.20.3
    - cdc-office-0.22.0
    - cdc-rdb-0.20.3
    - cdc-ui-0.20.3
    - cdc-utils-0.25.0


## [0.21.0] - 2022-03-11
### Added
- Added export to `OutputStream`. #52

### Changed
- Updated dependencies:
    - cdc-io-0.21.1
    - cdc-issues-0.22.0
    - cdc-kernel-0.20.2
    - cdc-office-0.21.1
    - cdc-rdb-0.20.2
    - cdc-ui-0.20.2
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2
- `Config` data is now retrieved from Manifest.
- Renamed `ExportDriver` to `StreamExporter`, and propagated that change to associated methods. #52


## [0.20.1] - 2022-02-13
### Changed
- Updated dependencies:
    - cdc-io-0.21.0
    - cdc-issues-0.21.0
    - cdc-kernel-0.20.1
    - cdc-office-0.21.0
    - cdc-rdb-0.20.1
    - cdc-ui-0.20.1
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-05
### Changed
- Upgraded to Java 11
- Updated dependencies:
    - cdc-io-0.20.0
    - cdc-issues-0.20.0
    - cdc-kernel-0.20.0
    - cdc-office-0.20.0
    - cdc-rdb-0.20.0
    - cdc-ui-0.20.0
    - derby-10.15.2.0


## [0.13.4] - 2022-01-29
### Changed
- Updated dependencies:
    - cdc-issues-0.15.1
    - cdc-office-0.15.0
    - cdc-rdb-0.12.3


## [0.13.3] - 2022-01-23
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-issues-0.15.0


## [0.13.2] - 2022-01-15
### Added
- `ImportRow.isEmpty()` method was added.
- Created `ActiveExporter`and related classes.  
  This offers active iteration for exports, which may be preferable in some cases. #50

### Changed
- Empty rows are now ignored during import. #49
- Some methods of `SheetExporter`, `SheetImporter`, `WoorkbookExporter`
  and `WoorkbookImporter` have a default empty implementation. #44
- When building a `ColumnTemplate`, an import converter is set by default for primitive types, strings and enums. #51

### Security
- Updated dependencies:
    - cdc-io-0.13.2
    - cdc-issues-0.14.2
    - cdc-kernel-0.14.2
    - cdc-office-0.14.2
    - cdc-rdb-0.12.2
    - cdc-ui-0.13.2
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1. #48


## [0.13.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-io-0.13.1
    - cdc-issues-0.14.1
    - cdc-kernel-0.14.1
    - cdc-office-0.14.1
    - cdc-rdb-0.12.1
    - cdc-ui-0.13.1
    - cdc-util-0.14.1
    - org.apache.log4j-2.17.0. #48


## [0.13.0] - 2021-12-15
### Added
- `ImpExFactoryFeatures.Hint.IGNORE_MISSING_ACTION_COLUMN` was added.  
  When loading a workbook, with this option enabled,
  absence of Action column does not interrupt loading. #47
- Added methods with default value to `ImportRow`. #46

### Security
- Updated dependencies:
    - cdc-io-0.13.0
    - cdc-issues-0.14.0
    - cdc-kernel-0.14.0
    - cdc-office-0.14.0
    - cdc-rdb-0.12.0
    - cdc-ui-0.13.0
    - cdc-util-0.14.0
    - org.apache.log4j-2.16.0. #48


## [0.12.3] - 2021-12-07
### Added
- Added `MISSING_ACTION_COLUMN` to `ImportIssueType`.

### Changed
- Updated dependencies:
    - cdc-office-0.13.1

### Fixed
- Fixed warnings.
- Additional columns are now ignored when loading office sheets, instead of throwing an exception. #43


## [0.12.2] - 2021-11-02
### Changed
- Updated dependencies


## [0.12.1] - 2021-11-01
### Fixed
- Fixed bom. #42

### Changed
- Updated dependencies


## [0.12.0] - 2021-10-02
### Added
- Added `Usage.DERIVED`. A derived column is optional, read-only.
  It can not be set manually. #39
- Added new values to `ImpExFactoryFeatures.Hint` and passed them to created
  factory in `WorkbookExportDriver`. Excel generation should be faster and
  use less memory. #41

### Changed
- Renamed `Usage.isMandatoryForExport` to `Usage.isMandatoryForFutureImport` which
  gives a better description of the intent of this function.

### Fixed
- When Action column is empty, it is now considered as IGNORE and an
  invalid/annoying message is no more generated. #40


## [0.11.1] - 2021-07-23
### Fixed
- Invalid pom dependencies.


## [0.11.0] - 2021-07-23  [YANKED]
### Changed
- Updated dependencies.
- Improved ImpExToolbox for checks of import files.

### Fixed
- Fixed typos in options.
- Generate a valid issue description from an exception. #38


## [0.10.0] - 2021-06-05
### Added
- `cdc-impex-tools` module. ImpExToolbox allows creation of template files and analysis of import files. #36

### Fixed
- When action column contains DELETE, do not generate warning message during imports. #35


## [0.9.0] - 2021-05-10
### Added
- `cdc-impex-db` module. It provides helpers to implement Import to / Export from databases using JDBC. #28

### Changed
- Renamed *extract* to *export* in `SheetExporter` and `WorkbookExporter`. #32


## [0.8.0] - 2021-05-03
### Added
- Created demos module and tests. #26

### Changed
- Renamed `cdc-impex.api` to `cdc.impex`. #27
- Used latest version of cdc-issues.
- Better documentation (added sequence diagrams).

### Fixed
- Known limitations in README.md.
- Dependencies in pom files.
- doc-files path. #29


## [0.7.0] - 2021-04-08
### Added
- This CHANGELOG file.
- JSON import. #17
- New XML module (export, import). #18

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.7.0 changes
- Improved README.md


## [0.6.0] - 2021-03-29
### Added
- Detection of duplicate templates in ExporterImpl. #24
- New helper functions in Exporter, Importer, ...
- New JSON module (Template generation, export). #17

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.6.0 changes
- Improved README.md
- Renamed (fix) createSheetExporterFor to createSheetExporterFor in ImpExCatalog.
- Added domain to SheetTemplate. #25


## [0.5.0] - 2021-03-21
### Added
- COPYING.LESSER. #22
- ImpExFormat. #21
- Export code. #16
- Report module. #23

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.5.0 changes
- Improved README.md
- Transformed AbstractBatchImportHandler into a filter (BatchImportHandler). #19
- Added progress controller parameter to Importer. #20
- Renamed Extractor to Exporter to reach a better symmetry with import naming.
- Reorganized packages.


## [0.4.0] - 2021-03-14
### Added
- AbstractBatchImportHandler #7
- ImportAnalyzer that can be used to analyze and import file. #12
- New issues (messages). #14
- Comments

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.4.0 changes
- Added size parameter to Exporter.beginSheet and progress controller parameter to WorkbookExprter. #11
- Added beginImport/endImport to ImportHandler. #13
- Added IssuesHandler parameter to ImportHandler methods. #15


## [0.3.0] - 2021-03-07
### Added
- Description to SheetTemplate. #1
- Handling of ERASE. #4
- Code to export data. #2
- WorkbookExportFeatures. #6
- ImportAction.IGNORE. #5
- ImpExFactory, ImpExFactoryFeatures. #8

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.3.0 changes
- Renamed ImportAction.REMOVE to ImportAction.DELETE. #3
- Renamed project and packages to impex. #9
- Split code to several modules. #8


## [0.2.0] - 2021-03-01
### Added
- Comments

### Changed
- Updated dependencies.
- Propagated cdc-issues-0.2.0 changes

### Fixed
- Messages


## [0.1.0] - 2021-02-22
### Added
- Initial design (Templates, Action, Usage, Importer, ImportHandler, ImportCatalog, ...)
