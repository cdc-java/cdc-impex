package cdc.impex.core;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.ImportAnalyzer;
import cdc.impex.imports.Importer;
import cdc.impex.imports.WorkbookImporter;
import cdc.impex.templates.SheetTemplate;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactory;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;

public class ImportAnalyzerImpl implements ImportAnalyzer {
    private final ImpExFactory factory;

    public ImportAnalyzerImpl(ImpExFactoryFeatures features) {
        this.factory = new ImpExFactory(features);
    }

    public ImportAnalyzerImpl(ImpExFactory factory) {
        this(factory.getFeatures());
    }

    @Override
    public void analyze(File file,
                        Set<SheetTemplate> templates,
                        File issuesFile,
                        ProgressController controller) throws IOException {
        final Importer importer = factory.createImporter(file);
        final IssuesCollector<Issue> issuesCollector = new IssuesCollector<>();
        final IssuesIoFactory issuesFactory = new IssuesIoFactory(IssuesIoFactoryFeatures.UTC_FASTEST);
        final IssuesWriter issuesWriter = issuesFactory.createIssuesWriter(issuesFile);

        importer.importData(file,
                            templates,
                            WorkbookImporter.QUIET_VOID,
                            issuesCollector,
                            controller);
        issuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesCollector.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          controller);
    }
}