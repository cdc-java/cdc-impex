package cdc.impex.core.io;

import java.io.IOException;
import java.util.List;

import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.io.xml.XmlWriter;
import cdc.util.strings.StringUtils;

public final class TemplateXml {
    public static final String CHECKER = "checker";
    public static final String CHECK_FAILURE_SEVERITY = "check-failure-severity";
    public static final String COLUMN = "column";
    public static final String COLUMNS = "columns";
    public static final String DATA_TYPE = "data-type";
    public static final String DESCRIPTION = "description";
    public static final String DOMAIN = "domain";
    public static final String NAME = "name";
    public static final String TEMPLATE = "template";
    public static final String TEMPLATES = "templates";
    public static final String USAGE = "usage";

    private TemplateXml() {
    }

    public static void write(XmlWriter writer,
                             List<SheetTemplate> templates) throws IOException {
        writer.beginElement(TEMPLATES);
        for (final SheetTemplate template : templates) {
            writer.beginElement(TEMPLATE);
            writer.addAttribute(DOMAIN, template.getDomain());
            writer.addAttribute(NAME, template.getName());

            if (!StringUtils.isNullOrEmpty(template.getDescription())) {
                writer.addElement(DESCRIPTION, template.getDescription());
            }

            writer.beginElement(COLUMNS);
            for (final ColumnTemplate<?> column : template.getColumns()) {
                writer.beginElement(COLUMN);
                writer.addAttribute(NAME, column.getName());
                writer.addAttribute(USAGE, column.getUsage());
                writer.addAttribute(DATA_TYPE, column.getDataType().getCanonicalName());
                if (column.getCheckFailureSeverity() != null) {
                    writer.addAttribute(CHECK_FAILURE_SEVERITY, column.getCheckFailureSeverity());
                }

                if (!StringUtils.isNullOrEmpty(column.getDescription())) {
                    writer.addElement(DESCRIPTION, column.getDescription());
                }

                writer.endElement();
            }
            writer.endElement();

            writer.endElement();
        }
        writer.endElement();
    }
}