package cdc.impex.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.impex.imports.ImportIssueType;
import cdc.impex.imports.ImportRow;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.ImportAction;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.impex.templates.Usage;
import cdc.issues.Issue;
import cdc.validation.checkers.defaults.IsGreaterOrEqual;

class ImportRowImplTest {
    private static final Logger LOGGER = LogManager.getLogger(ImportRowImplTest.class);
    private static final ColumnTemplate<String> M_RO_ID =
            ColumnTemplate.builder(String.class)
                          .name("M RO ID")
                          .usage(Usage.MANDATORY_RO_ID)
                          .build();
    private static final ColumnTemplate<Integer> O_RO_ID =
            ColumnTemplate.builder(Integer.class)
                          .name("O RO ID")
                          .usage(Usage.OPTIONAL_RO_ID)
                          .checker(IsGreaterOrEqual.from(0))
                          .build();
    private static final ColumnTemplate<String> M_RO_ATT =
            ColumnTemplate.builder(String.class)
                          .name("M RO ATT")
                          .usage(Usage.MANDATORY_RO_ATT)
                          .build();
    private static final ColumnTemplate<Integer> O_RO_ATT =
            ColumnTemplate.builder(Integer.class)
                          .name("O RO ATT")
                          .usage(Usage.OPTIONAL_RO_ATT)
                          .checker(IsGreaterOrEqual.from(0))
                          .build();
    private static final ColumnTemplate<String> M_RW_ATT =
            ColumnTemplate.builder(String.class)
                          .name("M RW ATT")
                          .usage(Usage.MANDATORY_RW_ATT)
                          .build();
    private static final ColumnTemplate<Integer> O_RW_ATT =
            ColumnTemplate.builder(Integer.class)
                          .name("O RW ATT")
                          .usage(Usage.OPTIONAL_RW_ATT)
                          .checker(IsGreaterOrEqual.from(0))
                          .build();
    private static final ColumnTemplate<String> D_RO_ATT =
            ColumnTemplate.builder(String.class)
                          .name("D RO ATT")
                          .usage(Usage.DERIVED_RO_ATT)
                          .build();

    private static final ColumnTemplate<Integer> PATTERN =
            ColumnTemplate.builder(Integer.class)
                          .pattern("I[0-9]")
                          .usage(Usage.OPTIONAL_RW_ATT)
                          .build();

    private static final SheetTemplate ST =
            SheetTemplate.builder()
                         .domain("Domain")
                         .name("Sheet")
                         .column(M_RO_ID)
                         .column(O_RO_ID)
                         .column(M_RO_ATT)
                         .column(O_RO_ATT)
                         .column(M_RW_ATT)
                         .column(O_RW_ATT)
                         .column(D_RO_ATT)
                         .column(PATTERN)
                         .build();

    private static void check(Consumer<ImportRowImpl.Builder> initRow,
                              ImportIssueType... expectedIssueTypes) {
        final ImportRowImpl.Builder builder = ImportRowImpl.builder()
                                                           .templateInstance(SheetTemplateInstance.replace(ST, "I1", "I2"))
                                                           .systemId("SystemId")
                                                           .sheetName("Sheet")
                                                           .number(0);
        initRow.accept(builder);
        final ImportRow row = builder.build();
        LOGGER.info("row {}", row);
        LOGGER.info("issues ({})", row.getIssues().size());
        for (final Issue issue : row.getIssues()) {
            LOGGER.info("   {}", issue);
        }
        assertSame(ST, row.getTemplate());
        assertEquals("SystemId", row.getSystemId());
        assertEquals("Sheet", row.getSheetName());

        assertSame(expectedIssueTypes.length, row.getIssues().size());

        // Collect and sort actual issue types
        final List<ImportIssueType> actual = new ArrayList<>();
        for (final Issue issue : row.getIssues()) {
            actual.add(issue.getName(ImportIssueType.class));
        }
        Collections.sort(actual);

        // Collect and sort expected issue types
        final List<ImportIssueType> expected = new ArrayList<>();
        Collections.addAll(expected, expectedIssueTypes);
        Collections.sort(expected);

        assertEquals(expected, actual);
    }

    @Test
    void testNoAction() {
        check(b -> {
            // Ignore
        });
    }

    @Test
    void testIgnore() {
        check(b -> {
            b.put(ST.getActionColumnName(), "IGNORE");
        });
    }

    @Test
    void testCreateMissingMandatoryData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE");
        },
              ImportIssueType.MISSING_MANDATORY_DATA,
              ImportIssueType.MISSING_MANDATORY_DATA,
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testCreateMissingMandatoryDataGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.CREATE);
        },
              ImportIssueType.MISSING_MANDATORY_DATA,
              ImportIssueType.MISSING_MANDATORY_DATA,
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testUpdateMissingMandatoryData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE");
        },
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testUpdateMissingMandatoryDataGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.UPDATE);
        },
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testDeleteMissingMandatoryData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE");
        },
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testDeleteMissingMandatoryDataGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.DELETE);
        },
              ImportIssueType.MISSING_MANDATORY_DATA);
    }

    @Test
    void testCreateMinimum() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA");
        });
    }

    @Test
    void testCreateMinimumGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.CREATE)
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA");
        });
    }

    @Test
    void testUpdateMinimum() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID");
        });
    }

    @Test
    void testUpdateMinimumGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.UPDATE)
             .put(M_RO_ID, "ID");
        });
    }

    @Test
    void testDeleteMinimum() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID");
        });
    }

    @Test
    void testDeleteMinimumGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.DELETE)
             .put(M_RO_ID, "ID");
        });
    }

    @Test
    void testCreateUnkownColumn() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put("XXX", "YYY");
        });
    }

    @Test
    void testUpdateUnkownColumn() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID")
             .put(M_RW_ATT, "RWA")
             .put("XXX", "YYY");
        });
    }

    @Test
    void testDeleteUnkownColumn() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID")
             .put("XXX", "YYY");
        });
    }

    @Test
    void testCreateFull() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RO_ATT, "20")
             .put(O_RW_ATT, "30");
        });
    }

    @Test
    void testCreateFullGlobal() {
        check(b -> {
            b.defaultAction(ImportAction.CREATE)
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RO_ATT, "20")
             .put(O_RW_ATT, "30");
        });
    }

    @Test
    void testUpdateFull() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RW_ATT, "30");
        });
    }

    @Test
    void testDeleteFull() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID")
             .put(O_RO_ID, "10");
        });
    }

    @Test
    void testCreateNonCompliantData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "-10")
             .put(O_RO_ATT, "-20")
             .put(O_RW_ATT, "-30");
        },
              ImportIssueType.NON_COMPLIANT_DATA,
              ImportIssueType.NON_COMPLIANT_DATA,
              ImportIssueType.NON_COMPLIANT_DATA);
    }

    @Test
    void testUpdateNonCompliantData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "-10")
             .put(O_RW_ATT, "-30");
        },
              ImportIssueType.NON_COMPLIANT_DATA,
              ImportIssueType.NON_COMPLIANT_DATA);
    }

    @Test
    void testDeleteNonCompliantData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID")
             .put(O_RO_ID, "-10");
        },
              ImportIssueType.NON_COMPLIANT_DATA);
    }

    @Test
    void testCreateUnexpectedData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RO_ATT, "20")
             .put(O_RW_ATT, "30")
             .put(D_RO_ATT, "DERIVED");
        },
              ImportIssueType.UNEXPECTED_DATA);
    }

    @Test
    void testUpdateUnexpectedData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RO_ATT, "20")
             .put(O_RW_ATT, "30")
             .put(D_RO_ATT, "DERIVED");
        },
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA);
    }

    @Test
    void testDeleteUnexpectedData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "10")
             .put(O_RO_ATT, "20")
             .put(O_RW_ATT, "30")
             .put(D_RO_ATT, "DERIVED");
        },
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA,
              ImportIssueType.UNEXPECTED_DATA);
    }

    @Test
    void testCreateUnexpectedErase() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ERASE")
             .put(M_RO_ATT, "ERASE")
             .put(M_RW_ATT, "ERASE")
             .put(O_RO_ID, "ERASE")
             .put(O_RO_ATT, "ERASE")
             .put(O_RW_ATT, "ERASE");
        },
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE);
    }

    @Test
    void testUpdateUnexpectedErase() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ERASE") // OK unexpected erase
             .put(M_RO_ATT, "ERASE") // OK unexpected erase
             .put(M_RW_ATT, "ERASE") // OK unexpected erase
             .put(O_RO_ID, "ERASE") // OK unexpected erase
             .put(O_RO_ATT, "ERASE") // OK unexpected erase
             .put(O_RW_ATT, "ERASE"); // OK
        },
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE);
    }

    @Test
    void testDeleteUnexpectedErase() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ERASE") // OK unexpected erase
             .put(M_RO_ATT, "ERASE") // OK unexpected erase
             .put(M_RW_ATT, "ERASE") // OK unexpected erase
             .put(O_RO_ID, "ERASE") // OK unexpected erase
             .put(O_RO_ATT, "ERASE") // OK unexpected erase
             .put(O_RW_ATT, "ERASE"); // OK unexpected erase
        },
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE,
              ImportIssueType.UNEXPECTED_ERASE);
    }

    @Test
    void testCreateNonConvertibleData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "CREATE")
             .put(M_RO_ID, "ID")
             .put(M_RO_ATT, "ROA")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "INVALID")
             .put(O_RO_ATT, "INVALID")
             .put(O_RW_ATT, "INVALID");
        },
              ImportIssueType.NON_CONVERTIBLE_DATA,
              ImportIssueType.NON_CONVERTIBLE_DATA,
              ImportIssueType.NON_CONVERTIBLE_DATA);
    }

    @Test
    void testUpdateNonConvertibleData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "UPDATE")
             .put(M_RO_ID, "ID")
             .put(M_RW_ATT, "RWA")
             .put(O_RO_ID, "INVALID")
             .put(O_RW_ATT, "INVALID");
        },
              ImportIssueType.NON_CONVERTIBLE_DATA,
              ImportIssueType.NON_CONVERTIBLE_DATA);
    }

    @Test
    void testDeleteNonConvertibleData() {
        check(b -> {
            b.put(ST.getActionColumnName(), "DELETE")
             .put(M_RO_ID, "ID")
             .put(O_RO_ID, "INVALID");
        },
              ImportIssueType.NON_CONVERTIBLE_DATA);
    }
}