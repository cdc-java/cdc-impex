# cdc-impex

Library containing code that can help importing / exporting flat data using
different formats.  
Imported or exported data is a set of sheets/tables, each containing rows
and columns.

For imports, the following responsibilities are taken:
- Check that data is compliant with expectations
- Convert data from text to internal data types

Data to import must be contained in one file containing any number of data
sheets (if supported by file format).

This library defines several concepts and tools:
- Templates
- Template Generators
- Importers, WorkbookImporters and SheetImporters
- Exporters, WorkbookExporters and SheetExporters
- Catalogs
- Factories

It also provides an SPI implementation based on JDBC.

![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/impex.svg)


Imports and exports are symmetric.

Templates are defined at 2 levels:
- `SheetTemplates`: they are used to describe a table / sheet of data.
- `ColumnTemplates`: they are used to describe a column in a sheet.

### Sheet templates
A `SheetTemplate` is characterized by;
- a `domain`;
- a `name`, that must not contain '#' character;
- a `description`;
- a list of `column templates` (order does not matter);
- the `default action`, used when action is not defined (empty action cell or missing action column);
- and the `name of the action column`.

In a `SheetTemplate`, all columns must have different names.  
The optional action column is used to describe the action that must be taken on each
line. If the action column is missing, or no action is defined in a row, a default action is taken.  
It is defined in the `SheetTemplate` and can be overriden in `ImpExFactoryFeatures`.

### Column templates
A `ColumnTemplate` is characterized by:
- a `name` or `pattern`;
- a `description` that can help user to fill data in loaded files;
- a `usage`;
- a `data type`;
- a `string to data type converter` which is used to import data;
- a `data type to string converter` which is used to export data;
- an optional `checker` that can make stricter control of values, that are
  otherwise valid from data type point of view;
- the `behavior to adopt when checks fail`;
- an optional `Content Validation type`;
- an optional `Content Validation operator`;
- and an optional `list of Content Validation values`.

**Note:** when `data type` is primitive, a default string to data type is automatically generated,
as well as default content validation.

:warning:
**WARNING:** Converters should raise an exception when conversion is impossible.

### Name vs. pattern column templates
A `name column template` defines a single column, with a particular name.  
A `pattern column template` defines a set of columns whose size is unknown.

The actual column names of a sheet must match the sheet template.  
`SheetTemplateInstance` is used to associate a `SheetTemplate` to an actual `Header` that only contains names.

:warning:
**WARNING:** a `pattern column template` should probably be optional. Otherwise, behaviour may be surprising.


### Column usage
The `column usage` is characterized by:
- The expected `Presence` of data when the action is `CREATE`.
- The expected `Presence` of data when the action is `UPDATE`.
- The expected `Presence` of data when the action is `DELETE`.
- The ability to erase data when action is `UPDATE`.

Presence can be:
- `IGNORED` to indicate that cell value will be ignored and should not be provided.
- `OPTIONAL` to indicate that cell can be empty or filled.
- `MANDATORY` to indicate that cell must be filled.

Predefined  `column usages` are:
- `MANDATORY_RO_ID`: the column is a **mandatory** **read-only** part of the **entity id**.  
   It is used to designate an entity.  
   It **can not** be **changed**
- `OPTIONAL_RO_ID`: the column is an **optional** **read-only** part of the **entity id**.  
   It is used to designate an entity.  
   It **can not** be **changed**.
- `MANDATORY_RO_ATT`: the column is a **mandatory** **read-only** **entity attribute**.  
  It **must** be set at **creation time**.  
  It **can not** be **changed**.
- `OPTIONAL_RO_ATT`: the column is an **optional** **read-only** **entity attribute**.  
  It **can** be set at **creation time**.  
  It **can not** be **changed**.
- `MANDATORY_RW_ATT`: the column is a **mandatory** **read-write** **entity attribute**.  
  It **can not** be **null**.  
  It **must** be set at **creation time**.  
  It **can** be **changed**.
- `OPTIONAL_RW_ATT`: the column is an **optional** **read-write** **entity attribute**.  
  It **can** be **null**.  
  It **can** be set at **creation time**.  
  It **can** be **changed**.
- `DERIVED_RO_ATT`: the column is a **read-only** **derived** (computed) **entity attribute**.  
  It is useless for imports but can be useful in exports.


**Note:** if necessary, other usages can be defined.


### Row actions
Possible `actions` on each row are:
- `CREATE`: the content of the row must be used to create something.  
  All `MANDATORY` and `MANDATORY_ON_CREATE` columns must have a valid
  content.
- `DELETE`: the content of the row must be used to delete something.  
  All `MANDATORY` columns must have a valid content and designate an
  existing thing.
- `UPDATE`: the content of the row must be used to update something.  
  All `MANDATORY` columns must have a valid content and designate an
  existing thing.
- `IGNORE`: the row can be ignored. By default, leaving an action cell empty is
  interpreted as `IGNORE`. However the default action to take when the action cell is empty
  can be set in sheet template or globally.

## Template generators
A `TemplateGenerator` is used to create an empty file that can help users
fill data to import.  
It contains a description of selected `SheetTemplates`.  
There exit a `TemplateGenator` for each supported format.

This image shows an example of generated sample Excel file.  
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/template-sample-excel.png)

This image shows an example of generated sample CSV file.  
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/template-sample-csv.png)

In that case, the generated sample file contains data that must be removed by user to be valid.


## Supported file formats
Supported file formats (for imports, exports and template files) are:
- `CSV`: in that case, an imported file must contain one data sheet.
  An exported file can contain any number of data sheets.
- Excel formats, `XLS`, `XLSX` and `XLSM`.
  Those formats can contain any number of sheets.
- OpenOffice format `ODS`. This format can contain any number of sheets.

**Note:** `JSON` and `XML` are considered as future formats.

## Imports
During import, all sheets are analyzed by an `Importer` in the order they
appear in the file.  
If a sheet name matches an authorized template, it is loaded, otherwise it
is ignored.  
A file can contain several sheets that match the same template.  

`ImpExFactory` must be used to create an `Importer` that is compliant with
the file to import.

Each matching sheet is analyzed:
- Header row is first analyzed and checked.  
  If a `MANDATORY` column is missing, an issue is generated and import of
  that sheet is aborted.  
  `MANDATORY_ON_CREATE` columns can be missing. In that case, `CREATE`
  actions will be impossible.
- Each data row is analyzed.
    - Column Action is searched. If absent, action is set to `IGNORE`.
    - Cells are converted. In case of conversion failure, an issue is
      generated and associated value is set to null.  
      If a cell contains `ERASE`, this is stored an no conversion happens.
      This is only possible  when action is `UPDATE`.  
      If this is not the case, an issue is generated.
    - If a cell is successfully converted and a checker is defined, check
      is applied. If it fails, an issue is generated.
    - Presence or absence of cells is confronted to column usage and
      action. If something is missing, an issue is generated.
    - All rows are passed to the `WorkbookImporter`, whether they are valid
      or not.

### Template matching
To match a template, a sheet name must be
*'sheet template name'[# any string]*.

For example, if a sheet template named 'foo' is authorized, sheets named
'foo', 'foo#1' or 'foo#2' will be analyzed and converted using 'foo'
template.  
Matching is case sensitive.

### WorkbookImporter and SheetImporter
`WorkbookImporter` and `SheetImporter` are SPI that must be implemented by
the using application.  
One WorkbookImporter must be passed to an `Importer`.

As it is simpler and better to create `SheetImporters` dedicated to one
type of sheet, one can use `fromDelegates()` to create a composite
`WorkbookImporter` that will delegate work to appropriate `SheetImporter`.  
One can also use `ImpExCatalogs` to create composite `WokbookImporters`.

### Import setup
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/import-setup-seq.svg)

### Import execution
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/import-exec-seq.svg)


## Passive exports
During exports, it is the application responsibility to fill valid and
compliant data.  
Conversion of data to string is handled by `ExportRow` implementations.  
If the application produces non compliant data, issues are generated.

`ImpExFactory` must be used to create an `Exporter` that is compliant with
the file to generate.

A `WorkbookExporter` must be passed to the created `Exporter`.
It must contains one `SheetExporter` for each passed `SheetTemplate` to generate.

### WorkbookExporter and SheetExporter
`WorkbookExporter` and `SheetExporter` are SPI that must be implemented by
the using application.  
One WorkbookExporter must be passed to an `Exporter`.

As it is simpler and better to create `SheetExporters` dedicated to one
type of sheet, one can use `fromDelegates()` to create a composite
`WorkbookExporter` that will delegate work to appropriate `SheetExporter`.  
One can also use `ImpExCatalogs` to create composite `WokbookExporters`.

### Passive export setup
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/passive-export-setup-seq.svg)

### Passive export execution
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/passive-export-exec-seq.svg)

## Active exports
`ImpExFactory` must be used to create an `ActiveExporter` that is compliant with
the file to generate.

### Active export setup
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/active-export-setup-seq.svg)

### Active export execution
![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/active-export-exec-seq.svg)

## Factory
The `ImpExFactory` class must be used to create:
- An `Importer` that matches the format of the file to import.
- An `Exporter` that matches the format of the file to export.
- A `TemplateGenerator` that matches the format of the template file to
  create.
- ...

## Catalogs
An `ImpExCatalog` can be used to register `SheetTemplates`,
`SheetImporters` and `SheetExporters`.  
Some utility functions are available to create composite `WorkbookImporter`
from `SheetImporters`, or composite `WorkbookExporter` from `SheetExporters`.


## Known limitations and possible improvements
- It is not possible to define alternate sets of identification columns.
- JSON import/export is incomplete and experimental.
- XML import/export is incomplete and experimental.

## Examples

### Create ColumnTemplates 
```java
    // MANDATORY Id String
    // Use default toString method to convert String to String
    public static final ColumnTemplate<String> ID =
            ColumnTemplate.builder(String.class)
                          .name("Id")
                          .description("Identifier of ...")
                          .usage(Usage.MANDATORY)
                          .importConverter(Function.identity())
                          .build();

    // OPTIONAL_RW_ATT Length int (fixed name)
    // Use default toString method to convert int to String
    // Checks that value is in range [0,10] and emit a WARNING if it is not the case
    public static final ColumnTemplate<Integer> LENGTH =
            ColumnTemplate.builder(int.class)
                          .name("Length")
                          .checker(IsInRange.from(1, 10))
                          .checkFailureSeverity(IssueLevel.WARNING)
                          .description("Length of ...")
                          .usage(Usage.OPTIONAL_RW_ATT)
                          .importConverter(Integer::valueOf)
                          .build();

    // MANDATORY_RW_ATT Birthday LocalDate (fixed name)
    public static final ColumnTemplate<LocalDate> BIRTHDAY =
            ColumnTemplate.builder(LocalDate.class)
                          .name("Birthday")
                          .description("Birthday of ...")
                          .usage(Usage.MANDATORY_RW_ATT)
                          .importConverter(...)
                          .exportConverter(...)
                          .build();

    // OPTIONAL_RW_ATT Meta String (pattern)
    // There can be any number of actual columns that match this template
    public static final ColumnTemplate<String> META =
            ColumnTemplate.builder(String.class)
                          .pattern("Meta:.+")
                          .description("Meta data columns")
                          .usage(Usage.OPTIONAL_RW_ATT)
                          .build();
```

### Create SheetTemplates 
```java
    // TEMPLATE1 contains 3 columns: Id, Length and Action (that is automatically inserted with its default name)
    public static final SheetTemplate TEMPLATE1 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template1")
                         .column(ID)
                         .column(LENGTH)
                         .build();

    // TEMPLATE2 contains 3 columns: Id, Birthday and Action (that is automatically inserted with its default name)
    public static final SheetTemplate TEMPLATE2 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template2")
                         .column(ID)
                         .column(BIRTHDAY)
                         .build();
```

### Implement SheetImporter
```java
    /**
     * Implementation of SheetImporter dedicated to Template1.
     */
    public class Template1Importer implements SheetImporter {
        public Template1Importer() {
            super();
        }

        public void beginSheetImport(String systemId,
                                     String sheetName,
                                     SheetTemplate template,
                                     IssuesHandler<ImportIssueType> issuesHandler) {
             // Check that template is TEMPLATE1
             // Actual header is not yet known
             // ... 
        }
        
        public void importHeader(SheetTemplateInstance templateInstance,
                                 IssuesHandler<Issue> issuesHandler) {
             // Here actual header is known
             // ... 
        }

        public void importRow(ImportRow row,
                              IssuesHandler<ImportIssueType> issuesHandler) {
            if (row.canBeProcessed()) {
                // Retrieve id using ID ColumnTemplate
                final String id = row.getDataOrNull(ID);
                
                // Retrieve length using LENGTH ColumnTemplate
                final Integer length = row.getDataOrNull(LENGTH);

                // 
                switch (row.getAction()) {
                case CREATE:
                    try {
                        // Create the entity designated by row
                        // ...
                    } catch (... e) {
                        // Failure
                        // Use issuesHandler to generate an application issue
                        issuesHanlder.issue(...);
                    }
                    break;
                case UPDATE:
                    try {
                        // Update the entity designated by row
                        // ...
                        // We may need to do special processing if ERASE was asked for an OPTIONAL cell
                        // Use row.isErase(...)
                        
                    } catch (... e) {
                        // Failure
                        // Use issuesHandler to generate an application issue
                        issuesHanlder.issue(...);
                    }
                    break;
                case DELETE:
                    try {
                        // Delete the entity designated by row
                        // ...
                    } catch (... e) {
                        // Failure
                        // Use issuesHandler to generate an application issue
                        issuesHanlder.issue(...);
                    }
                    break;
                case IGNORE:
                default:
                    // Ignore 
                    break;
                }
            } else {
              // If row can not be processed, issues are already generated by importer,
              // so we don't need to do nothing here.
              // However, we can retrieve all issues that we found in analysis of row
              // using row.getIssues(), and do whatever we want.
            }
         }

        public void endSheetImport(String systemId,
                                   String sheetName,
                                   SheetTemplateInstance templateInstance,
                                   IssuesHandler<ImportIssueType> issuesHandler) {
            // We may close a connection here, ...
        }
    }

    /**
     * Implementation of SheetImporter dedicated to Template2.
     */
    public class Template2Importer implements SheetImporter {
        // ...
    }
```

### Implement SheetExporter
```java
    public class Template1Exporter extends CheckedSheetExporter {
        public Template1Exporter() {
            // ...
        }

        @Override
        public void beginSheetExport(SheetTemplateInstance templateInstance,
                                     IssuesHandler<? super ExportIssue> issuesHandler) {
            super.beginSheetExtraction(templateInstance, issuesHandler);
            // ...
        }

        @Override
        public int getNumberOfRemainingRows() {
            checkStatus(ImpExStatus.SHEET);
            return ...;
        }

        @Override
        public void exportRow(ExportRow row,
                              IssuesHandler<? super ExportIssue> issuesHandler) {
            checkStatus(ImpExStatus.SHEET);

            row.setData(..., ...);
            row.setData(..., ...);
            ....
            }
        }
        
        @Override
        public void endSheetExport(SheetTemplateInstance templateInstance,
                                   IssuesHandler<? super ExportIssue> issuesHandler) {
            super.endSheetExtraction(templateInstance, issuesHandler);
            // ...
        }
    }
    
    public class Template2Exporter extends CheckedSheetExporter {
        // ...
    }
```

### Create and use ImpExCatalog
```java
    public static final ImpExCatalog CATALOG = new ImpExCatalog();

    // Register TEMPLATE1 and associate it to an instance of Template1Importer 
    CATALOG.register(TEMPLATE1, new Template1Importer());
    // Register TEMPLATE2 and associate it to an instance of Template2Importer 
    CATALOG.register(TEMPLATE2, new Template2Importer());
    // Register TEMPLATE1 and associate it to an instance of Template1Exporter 
    CATALOG.register(TEMPLATE1, new Template1Exporter());
    // Register TEMPLATE2 and associate it to an instance of Template2Exporter 
    CATALOG.register(TEMPLATE2, new Template2Exporter());

    // We could also do things in more steps 
    CATALOG.register(TEMPLATE1);
    CATALOG.register(TEMPLATE2);
    CATALOG.register(TEMPLATE1.getName(), new Template1Importer());
    CATALOG.register(TEMPLATE2.getName(), new Template2Importer());
    CATALOG.register(TEMPLATE1.getName(), new Template1Exporter());
    CATALOG.register(TEMPLATE2.getName(), new Template2Exporter());
```

### Generate template files
```java
    // File that should contain description of selected templates
    // File name should match the extension of a supported template format
    final File file = ...

    // Create a TemplateGenerator for target file
    final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
    final TemplateGenerator generator = factory.createTemplateGenerator(file);

    // Generate the template file for selected templates
    generator.generate(file,
                       TEMPLATE1,
                       TEMPLATE2);
```

### Import data
```java
    // File to import
    // File name should match the extension of a supported import format
    final File file = ...

    // We want to collect issues generated by import, so we create an IssueCollector
    final IssuesCollector<ImportIssue> issuesHandler =
                new IssuesCollector<>(DefaultIssuesHandler.get(false));

    // The progress controller to use
    final ProgressController controller = ...

    // Create an importer for file
    final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
    final Importer importer = factory.createImporter(file);

    // Import file
    // We only want to load data related to Template1 and Template2
    // We use CATALOG to create a composite WorkbookImporter for Template1 and Template2,
    importer.importData(file,
                        CATALOG.getTemplatesAsSet("Template1", "Template2"),
                        CATALOG.createWorkbookImporterFor("Template1", "Template2"),
                        issuesHandler,
                        controller);

    // Save issues into a file
    // File name should match the extension of a supported format
    // Timestamps of issues will be 
    IssuesWriter.save(issuesHandler.getIssues(),
                      new File("..."),
                      IssuesFactoryFeatures.UTC_FASTEST);
```

### Add batch actions during imports
During imports it may be necessary to execute some actions regularly.  
For example, one may want to commit data every 100 rows.  

`BatchSheetImporter` can be used for that. It is a filter that delegate
most work and calls a `batch` method when necessary.

### Export data (passive)
```java
    // File to generate
    // File name should match the extension of a supported export format
    final File file = ...
    
    // We want to collect issues generated by export, so we create an IssueCollector
    final IssuesCollector<ExportIssue> issuesHandler =
                new IssuesCollector<>(DefaultIssuesHandler.get(false));
    
    // The progress controller to use
    final ProgressController controller = ...
    
    // Create an exporter for file
    // An exporter is not dedicated to any template, just to a file format
    final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
    final Exporter exporter = factory.createExporter(file);
    
    // Export to file
    // We only want to export data related to Template1 and Template2
    // We use CATALOG to create a composite WorkbookExporter for Template1 and Template2
    // The order of templates matters
    exporter.exportData(file,
                        CATALOG.getTemplatesAsList("Template1", "Template2"),
                        CATALOG.createWorkbookExporterFor("Template1", "Template2"),
                        issuesHandler,
                        controller);

    // Save issues into a file
    // File name should match the extension of a supported format
    // Timestamps of issues will be 
    IssuesWriter.save(issuesHandler.getIssues(),
                      new File("..."),
                      IssuesFactoryFeatures.UTC_FASTEST);
```

### Export data (active)
```java
    // File to generate
    // File name should match the extension of a supported export format
    final File file = ...
    
    // We want to collect issues generated by export, so we create an IssueCollector
    final IssuesCollector<ExportIssue> issuesHandler =
                new IssuesCollector<>(DefaultIssuesHandler.get(false));
    
    // The progress controller to use
    final ProgressController controller = ...
    
    // Create an exporter for file
    // An exporter is not dedicated to any template, just to a file format
    final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
    final ActiveExporter exporter = factory.createActiveExporter(file);
    
    exporter.beginExport(file, issuesHandler, controller);
    // Export sheet 1
    exporter.beginSheet(TEMPLATE1, rows);
    for (int index = 0; index < rows; index++) {
        final ExportRow row = exporter.nextRow();
        // Fill row
        exporter.addRow();
    }
    exporter.endSheet();
    
    // Export sheet 2
    exporter.beginSheet(TEMPLATE2, rows);
    for (int index = 0; index < rows; index++) {
        final ExportRow row = exporter.nextRow();
        // Fill row
        exporter.addRow();
    }
    exporter.endSheet();
    
    // Export other sheets
    
    exporter.endExport();

    // Save issues into a file
    // File name should match the extension of a supported format
    // Timestamps of issues will be 
    IssuesWriter.save(issuesHandler.getIssues(),
                      new File("..."),
                      IssuesFactoryFeatures.UTC_FASTEST);
```

## JDBC SPI implementation
TODO  
See **DbExportDemo**.

## Toolbox
**ImpExToolbox** is a command line and graphical tool that can be used to:
- check import files
- generate template files 

![Image](cdc-impex-api/src/main/javadoc/cdc/impex/doc-files/toolbox.png)

### Options
```
USAGE
ImpExToolbox [--all-templates] [--args-file <arg>] [--args-file-charset <arg>] [--check-file <arg>] [--cli | --gui]
                   [--generate-template <arg>]  [-h | -v] [--help-width <arg>] [--issues-file <arg>] [--output-dir <arg>] [--prefix
                   <arg>] [--preload <arg>] [--template <arg>]  [--verbose]

OPTIONS
    --all-templates             If enabled, use or select all templates.
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option or value).
                                A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file encoding.
    --check-file <arg>          Name(s) of the file(s) to check. CLI mode.
    --cli                       If enabled, run as CLI.
    --generate-template <arg>   Name of the template file to generate. CLI mode.
    --gui                       If enabled, run as GUI (default).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --issues-file <arg>         Name of the issues file. CLI mode.
    --output-dir <arg>          Name of the output directory.
    --prefix <arg>              Prefix to use for generated files. GUI mode.
    --preload <arg>             Names of class(es) to preload.
                                Those classes should statically initialize Settings.CATALOG to declare templates that should be
                                used.
    --template <arg>            Name(s) of template(s) to use or select.
 -v,--version                   Prints version and exits.
    --verbose                   Print progress messages.
```