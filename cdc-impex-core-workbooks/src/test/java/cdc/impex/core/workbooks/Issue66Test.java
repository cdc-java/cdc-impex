package cdc.impex.core.workbooks;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.impex.ImpExCatalog;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.Importer;
import cdc.impex.imports.SheetImporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.Usage;
import cdc.issues.IssuesHandler;
import cdc.util.events.ProgressController;

class Issue66Test {
    private static final ColumnTemplate<String> ID =
            ColumnTemplate.builder(String.class)
                          .name("ID")
                          .usage(Usage.MANDATORY_RO_ID)
                          .build();
    private static final SheetTemplate ST =
            SheetTemplate.builder()
                         .domain("Domain")
                         .name("Sheet")
                         .column(ID)
                         .build();
    static final ImpExCatalog CATALOG =
            new ImpExCatalog().register(ST, SheetImporter.QUIET_VOID);

    void check(String filename) throws IOException {
        Importer.importData(new File(filename),
                            Set.of("Sheet"),
                            CATALOG,
                            IssuesHandler.VOID,
                            ProgressController.VOID,
                            ImpExFactoryFeatures.FASTEST);
    }

    @Test
    void testXlsx() throws IOException {
        check("src/test/resources/issue66.xlsx");
    }

    @Test
    void testCsv() throws IOException {
        check("src/test/resources/issue66.csv");
    }
}