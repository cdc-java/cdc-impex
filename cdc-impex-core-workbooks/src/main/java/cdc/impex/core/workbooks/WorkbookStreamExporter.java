package cdc.impex.core.workbooks;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExFormat;
import cdc.impex.ImpExStatus;
import cdc.impex.core.ExportRowImpl;
import cdc.impex.exports.ExportRow;
import cdc.impex.exports.StreamExporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.office.ss.ContentValidation;
import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.encoding.Encoders;
import cdc.util.encoding.ExtensionEncoder;
import cdc.util.lang.Checks;

/**
 * Implementation of Exporter that export data to workbooks.
 *
 * @author Damien Carbonne
 */
public class WorkbookStreamExporter implements StreamExporter {
    private final IssuesHandler<Issue> issuesHandler;

    private final ImpExFactoryFeatures features;
    private WorkbookWriter<?> writer = null;
    private ImpExStatus status = ImpExStatus.INIT;

    public WorkbookStreamExporter(IssuesHandler<Issue> issuesHandler,
                                  ImpExFactoryFeatures features) {
        Checks.isNotNull(issuesHandler, "issuesHandler");
        Checks.isNotNull(features, "features");

        this.issuesHandler = issuesHandler;
        this.features = features;
    }

    public WorkbookStreamExporter(IssuesHandler<Issue> issuesHandler,
                                  ImpExFactory factory) {
        this(issuesHandler, factory.getFeatures());
    }

    private void checkStatus(ImpExStatus expected) {
        Checks.isTrue(this.status == expected, "Invalid status " + status + ", expected " + expected);
    }

    private WorkbookWriterFactory beginExport() {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;

        final WorkbookWriterFactory factory = new WorkbookWriterFactory();

        // Pass matching hints to factory
        final ExtensionEncoder<ImpExFactoryFeatures.Hint, WorkbookWriterFactory.Hint> hintEncoder =
                Encoders.sameNameEncoder(ImpExFactoryFeatures.Hint.class,
                                         WorkbookWriterFactory.Hint.class);
        for (final ImpExFactoryFeatures.Hint sourceHint : hintEncoder.getSourceValues()) {
            final WorkbookWriterFactory.Hint targetHint = hintEncoder.encode(sourceHint);
            factory.setEnabled(targetHint, features.isEnabled(sourceHint));
        }
        return factory;
    }

    @Override
    public void beginExport(File file) throws IOException {
        Checks.isNotNull(file, "file");

        final WorkbookWriterFactory factory = beginExport();
        this.writer = factory.create(file, features.getWorkbookWriterFeatures());
    }

    @Override
    public void beginExport(OutputStream out,
                            ImpExFormat format) throws IOException {
        Checks.isNotNull(out, "out");
        Checks.isNotNull(format, "format");
        // This will returns a valid kind
        final WorkbookKind kind = format.getWorkbookKind();

        final WorkbookWriterFactory factory = beginExport();
        this.writer = factory.create(out, kind, features.getWorkbookWriterFeatures());

    }

    @Override
    public void addReadme(List<SheetTemplateInstance> templateInstances) throws IOException {
        WorkbookSupport.generateReadme(writer,
                                       templateInstances.stream().map(SheetTemplateInstance::getTemplate).toList(),
                                       features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN));
    }

    @Override
    public void beginSheet(SheetTemplateInstance templateInstance,
                           String sheetName,
                           long numberOfRows) throws IOException {
        Checks.isNotNull(templateInstance, "templateInstance");
        Checks.isNotNull(sheetName, "sheetName");

        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;

        writer.beginSheet(sheetName);
        writer.beginRow(TableSection.HEADER);
        int columnIndex = 0;
        for (final String columnName : templateInstance.getHeader().getSortedNames()) {
            final ColumnTemplate<?> column = templateInstance.getTemplate().getMatchingColumn(columnName);
            if (!templateInstance.getTemplate().isActionColumn(column)
                    || !features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN)) {
                writer.addCell(columnName);
                if (features.isEnabled(ImpExFactoryFeatures.Hint.ADD_HEADER_COMMENTS)
                        && writer.isSupported(WorkbookWriterFeatures.Feature.COMMENTS)) {
                    writer.addCellComment(column.getComment());
                }
                if (features.isEnabled(ImpExFactoryFeatures.Hint.ADD_CONTENT_VALIDATION)
                        && writer.isSupported(WorkbookWriterFeatures.Feature.CONTENT_VALIDATION)) {
                    final ContentValidation cv =
                            WorkbookTemplateGenerator.createContentValidation(writer.getKind(), column, columnName, columnIndex);
                    if (cv != null) {
                        writer.addContentValidation(cv);
                    }
                }

                columnIndex++;
            }
        }
    }

    @Override
    public void addRow(ExportRow row) throws IOException {
        checkStatus(ImpExStatus.SHEET);

        final List<Issue> issues = row.getIssues();
        issuesHandler.issues(issues);

        writer.beginRow(TableSection.DATA);
        for (final String name : row.getTemplateInstance().getHeader().getSortedNames()) {
            final ColumnTemplate<?> column = row.getTemplateInstance().getTemplate().getMatchingColumn(name);
            if (!row.getTemplate().isActionColumn(column) || !features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN)) {
                if (row.containsKey(name)) {
                    final String s = row.getValue(name);
                    writer.addCell(s);
                    if (features.isEnabled(ImpExFactoryFeatures.Hint.ADD_DATA_COMMENTS)) {
                        final String comment = row.getComment(name);
                        if (comment != null) {
                            writer.addCellComment(comment);
                        }
                    }
                } else if (column.getUsage().isMandatoryForFutureImport()) {
                    writer.addCell(ExportRowImpl.MISSING_DATA);
                } else {
                    writer.addEmptyCell();
                }
            }
        }
    }

    @Override
    public void endSheet() throws IOException {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.WORKBOOK;
    }

    @Override
    public void endExport() throws IOException {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.INIT;

        this.writer.flush();
    }

    @Override
    public void flush() throws IOException {
        this.writer.flush();
    }

    @Override
    public void close() throws IOException {
        this.writer.close();
    }
}