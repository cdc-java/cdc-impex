package cdc.impex.core.workbooks;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.ImportAction;
import cdc.impex.templates.SheetTemplate;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.TableSection;

final class WorkbookSupport {
    private WorkbookSupport() {
    }

    /**
     * Generates README sheets.
     *
     * @param writer The writer.
     * @param sheets The sheets.
     * @param skipActionColumn If {@code true}, action column is skipped.
     * @throws IOException When an IO error occurs.
     */
    static void generateReadme(WorkbookWriter<?> writer,
                               List<SheetTemplate> sheets,
                               boolean skipActionColumn) throws IOException {
        writer.beginSheet("README");
        writer.beginRow(TableSection.HEADER);
        writer.addCellAndComment("Kind", "Line Kind (SHEET or COLUMN).");
        writer.addCellAndComment("Domain", "Name of the Functional Domain of the Sheet Template.");
        writer.addCellAndComment("Sheet", "Name of the Sheet Template.");
        writer.addCellAndComment("Column", "Name of the Column in the Sheet Template.");
        writer.addCellAndComment("Description", "Description of the Sheet or Column.");
        writer.addCellAndComment("Usage", "Usage of the Column.");
        writer.addCellAndComment("Create", "Expectations on the Column content when action is CREATE.");
        writer.addCellAndComment("Update", "Expectations on the Column content when action is UPDATE.");
        writer.addCellAndComment("Delete", "Expectations on the Column content when action is DELETE.");
        writer.addCellAndComment("Erasable", "Can this column be erased (using ERASE keyword) when the action is UPDATE.");
        writer.addCellAndComment("Data Type", "Data Type of the Column.");
        writer.addCellAndComment("Check", "Explanations on additional checks, if any.");
        for (final SheetTemplate sheet : sheets) {
            writer.beginRow(TableSection.DATA);
            writer.addCell("SHEET");
            writer.addCell(sheet.getDomain());
            writer.addCell(sheet.getName());
            writer.addEmptyCell();
            writer.addCell(sheet.getDescription());

            for (final ColumnTemplate<?> column : sheet.getColumns()) {
                if (!sheet.isActionColumn(column) || !skipActionColumn) {
                    writer.beginRow(TableSection.DATA);
                    writer.addCell("COLUMN");
                    writer.addCell(sheet.getDomain());
                    writer.addCell(sheet.getName());
                    if (column.getHeader().isName()) {
                        writer.addCell(column.getName());
                    } else {
                        writer.addCell(column.getPattern().pattern() + " (pattern)");
                    }
                    writer.addCell(column.getDescription());
                    writer.addCell(column.getUsage().getName());
                    if (column == sheet.getActionColumn()) {
                        writer.addEmptyCells(3);
                    } else {
                        writer.addCell(column.getUsage().getPresence(ImportAction.CREATE));
                        writer.addCell(column.getUsage().getPresence(ImportAction.UPDATE));
                        writer.addCell(column.getUsage().getPresence(ImportAction.DELETE));
                    }
                    writer.addCell(column.getUsage().isErasable() ? "Yes" : "No");
                    final Class<?> dataType = column.getDataType();
                    if (dataType.isEnum()) {
                        writer.addCell(dataType.getSimpleName() + " " + Arrays.toString(dataType.getEnumConstants()));
                    } else {
                        writer.addCell(dataType.getSimpleName());
                    }
                    if (column.getCheckerOrNull() == null) {
                        writer.addEmptyCell();
                    } else {
                        writer.addCell(column.getCheckerOrNull().explain());
                    }
                }
            }
        }
    }
}