package cdc.impex.core.workbooks;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.impex.templates.TemplateGenerator;
import cdc.office.ss.CellAddressRange;
import cdc.office.ss.ContentValidation;
import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.tables.TableSection;

/**
 * Class used to generate an empty template file.
 *
 * @author Damien Carbonne
 */
public class WorkbookTemplateGenerator implements TemplateGenerator {
    private static final Logger LOGGER = LogManager.getLogger(WorkbookTemplateGenerator.class);
    private final ImpExFactoryFeatures features;

    public WorkbookTemplateGenerator(ImpExFactoryFeatures features) {
        this.features = features;
    }

    public WorkbookTemplateGenerator(ImpExFactory factory) {
        this(factory.getFeatures());
    }

    public static ContentValidation createContentValidation(WorkbookKind kind,
                                                            ColumnTemplate<?> column,
                                                            String columnName,
                                                            int columnIndex) {
        if (column.getContentValidationType() == ContentValidation.Type.ANY) {
            return null;
        } else {
            final ContentValidation cv =
                    ContentValidation.builder()
                                     .help(columnName + " value", column.getComment())
                                     .error("Invalid value for " + columnName, column.getComment())
                                     .errorReaction(ContentValidation.ErrorReaction.WARN)
                                     .allowsEmptyCell(true)
                                     .type(column.getContentValidationType())
                                     .operator(column.getContentValidationOperator())
                                     .values(column.getContentValidationValues())
                                     .addRange(new CellAddressRange(1, -1, columnIndex, columnIndex))
                                     .build();
            try {
                cv.check(kind);
                return cv;
            } catch (final IllegalArgumentException e) {
                LOGGER.warn("Disable content validation for column '{}'.\n{}: {}",
                            columnName,
                            e.getClass().getCanonicalName(),
                            e.getMessage());
                return null;
            }
        }
    }

    /**
     * Generates a sample template file.
     *
     * @param file The name of the file to generate.
     *            Its extension must be one of the supported workbook formats.
     * @param templateInstances The SheetTemplates for which sheets must be generated.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code file} extension is not recognized.
     */
    @Override
    public void generate(File file,
                         List<SheetTemplateInstance> templateInstances) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        try (final WorkbookWriter<?> writer = factory.create(file, features.getWorkbookWriterFeatures())) {
            final WorkbookKind kind = WorkbookKind.from(file);

            // README
            WorkbookSupport.generateReadme(writer,
                                           templateInstances.stream().map(SheetTemplateInstance::getTemplate).toList(),
                                           features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN));

            // Sheets
            for (final SheetTemplateInstance templateInstance : templateInstances) {
                writer.beginSheet(templateInstance.getTemplate().getName());
                writer.beginRow(TableSection.HEADER);
                int col = 0;
                for (final String columnName : templateInstance.getHeader().getSortedNames()) {
                    final ColumnTemplate<?> column = templateInstance.getTemplate().getMatchingColumn(columnName);
                    if (!templateInstance.getTemplate().isActionColumn(column)
                            || !features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN)) {
                        writer.addCell(columnName);
                        // Comments are not supported by CSV and will be ignored
                        // Other formats should support them, even if not yet implemented
                        writer.addCellComment(column.getComment());

                        // Content validation
                        final ContentValidation cv = createContentValidation(kind, column, columnName, col);
                        if (cv != null) {
                            writer.addContentValidation(cv);
                        }
                        col++;
                    }
                }
                if (kind == WorkbookKind.CSV) {
                    writer.beginRow(TableSection.HEADER);
                    for (final String columnName : templateInstance.getHeader().getSortedNames()) {
                        final ColumnTemplate<?> column = templateInstance.getTemplate().getMatchingColumn(columnName);
                        writer.addCell(column.getComment());
                    }
                } else {
                    // Do that to ensure auto filtering is handled
                    writer.beginRow(TableSection.DATA);
                }
            }
        }
    }
}