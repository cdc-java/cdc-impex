package cdc.impex.core.workbooks;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExMetas;
import cdc.impex.core.ImportRowImpl;
import cdc.impex.imports.ImportIssueType;
import cdc.impex.imports.ImportIssues;
import cdc.impex.imports.ImportRow;
import cdc.impex.imports.Importer;
import cdc.impex.imports.WorkbookImporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.ImportAction;
import cdc.impex.templates.Presence;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesHandler;
import cdc.issues.Metas;
import cdc.issues.locations.WorkbookLocation;
import cdc.office.ss.SheetParser;
import cdc.office.ss.SheetParserFactory;
import cdc.office.tables.BasicHeaderMapper;
import cdc.office.tables.Header;
import cdc.office.tables.HeaderCell;
import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.TablesHandler;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.function.Evaluation;
import cdc.util.lang.Checks;
import cdc.util.time.Chronometer;

/**
 * Implementation of Importer that can handle Workbooks.
 *
 * @author Damien Carbonne
 */
public class WorkbookImporterImpl implements Importer {
    private final ImpExFactoryFeatures features;

    public WorkbookImporterImpl(ImpExFactoryFeatures features) {
        this.features = features;
    }

    public WorkbookImporterImpl(ImpExFactory factory) {
        this(factory.getFeatures());
    }

    private static class Handler implements TablesHandler {
        private final String systemId;
        private final Map<String, SheetTemplate> templates = new HashMap<>();
        private final WorkbookImporter workbookImporter;
        private final IssuesHandler<Issue> issuesHandler;
        private final ProgressSupplier progress;
        private final ImpExFactoryFeatures features;

        /** Chronometer used to measure sheet loading time. */
        private final Chronometer sheetChrono = new Chronometer();
        /** Total number of sheets in workbook. */
        int totalSheets = 0;
        /** Number of loaded/recognized sheets in workbook. */
        int loadedSheets = 0;
        /** Number of loaded rows in current sheet. */
        int loadedRows = 0;

        private SheetTemplate currentTemplate = null;
        private SheetTemplateInstance currentTemplateInstance = null;
        private String currentSheetName = null;
        private BasicHeaderMapper currentHeaderMapper = null;
        private boolean cancelled = false;

        public Handler(String systemId,
                       Set<SheetTemplate> templates,
                       WorkbookImporter workbookImporter,
                       IssuesHandler<Issue> issuesHandler,
                       ProgressController controller,
                       ImpExFactoryFeatures features) {
            this.systemId = systemId;
            this.workbookImporter = workbookImporter;
            this.issuesHandler = issuesHandler;
            this.progress = new ProgressSupplier(controller);
            this.features = features;
            for (final SheetTemplate template : templates) {
                this.templates.put(template.getName(), template);
            }
            issue(ImportIssueType.LOAD_WORKBOOK,
                  "Load '" + systemId + "' workbook.");
        }

        private void applicationFailure(String description,
                                        ImportRow row) {
            Checks.isNotNull(description, "description");
            issuesHandler.issue(ImportIssues.builder()
                                            .name(ImportIssueType.APP_FAILURE)
                                            .severity(IssueSeverity.CRITICAL)
                                            .description(description)
                                            .metas(Metas.builder()
                                                        .meta(ImpExMetas.WORKBOOK, systemId)
                                                        .meta(ImpExMetas.SHEET, currentSheetName)
                                                        .build())
                                            .addLocation(WorkbookLocation.builder()
                                                                         .systemId(systemId)
                                                                         .sheetName(currentSheetName)
                                                                         .rowNumber(row.getNumber())
                                                                         .build())
                                            .build());
        }

        private void issue(ImportIssueType type,
                           String description) {
            issuesHandler.issue(ImportIssues.builder()
                                            .name(type)
                                            .severity(type.getSeverity())
                                            .description(description)
                                            .metas(Metas.builder()
                                                        .meta(ImpExMetas.WORKBOOK, systemId)
                                                        .metaIfNotNull(ImpExMetas.SHEET, currentSheetName)
                                                        .build())
                                            .addLocation(WorkbookLocation.builder()
                                                                         .systemId(systemId)
                                                                         .sheetName(currentSheetName)
                                                                         .build())
                                            .build());
        }

        /**
         * @return {@code true} if cancel was asked.<br>
         *         The first time the answer is {@code true}, an issue is emitted.
         */
        private boolean isCancelled() {
            if (cancelled) {
                return true;
            } else {
                if (progress.getController().isCancelled()) {
                    cancelled = true;
                    issue(ImportIssueType.IMPORT_CANCELLED,
                          "Import of '" + systemId + "' was cancelled.");
                    return true;
                } else {
                    return false;
                }
            }
        }

        /**
         * Returns the template that matches a sheet name.
         *
         * @param name The sheet name.
         * @return The matching template or {@code null}.
         */
        private SheetTemplate getMatchingTemplate(String name) {
            if (name == null) {
                if (systemId.toLowerCase().endsWith(".csv")) {
                    if (templates.size() == 1) {
                        return templates.values().iterator().next();
                    } else {
                        issue(ImportIssueType.TOO_MANY_TEMPLATES,
                              "Expected one template to import '" + systemId + "'.");
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                final SheetTemplate template = templates.get(name);
                if (template == null) {
                    // Try pattern matching
                    final int pos = name.indexOf('#');
                    if (pos >= 0) {
                        return templates.get(name.substring(0, pos));
                    } else {
                        return null;
                    }
                } else {
                    return template;
                }
            }
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            totalSheets++;
            loadedRows = 0;
            sheetChrono.start();
            currentTemplate = getMatchingTemplate(name);
            currentTemplateInstance = null;
            currentSheetName = name;
            if (currentTemplate != null && !isCancelled()) {
                loadedSheets++;
                progress.reset(numberOfRows, "Import sheet '" + name + "'");
                // Do this first, so that the message that is produced just after
                // is dispatched to the appropriate import handler
                workbookImporter.beginSheetImport(systemId,
                                                  currentSheetName,
                                                  currentTemplate,
                                                  issuesHandler);
                issue(ImportIssueType.LOAD_SHEET,
                      "Load '" + name + "' sheet using '" + currentTemplate.getQName()
                              + "' template.");
            } else {
                issue(ImportIssueType.IGNORED_SHEET,
                      "Ignored '" + name + "' sheet as there is no associated template.");
            }
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            if (currentTemplate != null && !isCancelled()) {
                // Found a matching template
                final Header expected = Header.builder()
                                              .cells(currentTemplate.getHeader().getSortedCells())
                                              .build();
                final Header actual = Header.builder()
                                            .names(header)
                                            .build();
                // The necessity of a column depends on the action and can not be defined here
                currentHeaderMapper = BasicHeaderMapper.builder()
                                                       .expected(expected)
                                                       .actual(actual)
                                                       .build();
                // If true, sheet loading must be abandoned
                boolean giveup = false;

                // Analyze columns that are declared in template and are absent from header
                for (final HeaderCell cell : currentHeaderMapper.getMissingExpectedCells()) {
                    // Retrieve the corresponding column.
                    // It MUST exist, as 'expected' was built from template columns
                    final ColumnTemplate<?> column = currentTemplate.getColumn(cell);
                    if (column.getUsage().getCount(Presence.MANDATORY) == 3) {
                        issue(ImportIssueType.MISSING_MANDATORY_COLUMN,
                              "Mandatory column " + cell + " is missing. Give up.");
                        giveup = true;
                    } else if (column.getUsage().getCount(Presence.MANDATORY) > 0) {
                        issue(ImportIssueType.MISSING_PARTIALLY_MANDATORY_COLUMN,
                              "Mandatory column '" + cell + "' is missing, "
                                      + Arrays.toString(column.getUsage().getImportActions(Presence.MANDATORY))
                                      + " actions are impossible.");
                    } else {
                        final String missing =
                                "Optional action column " + cell + " is missing.";
                        // The column is never mandatory.
                        if (currentTemplate.getActionColumnName().equals(cell.getLabel())) {
                            // Special processing for action column
                            if (features.isEnabled(ImpExFactoryFeatures.Hint.IGNORE_MISSING_ACTION_COLUMN)) {
                                issue(ImportIssueType.MISSING_ACTION_COLUMN, missing);
                            } else {
                                final ImportAction defaultAction = features.getDefaultAction().orElse(ImportAction.IGNORE);
                                if (defaultAction == ImportAction.IGNORE) {
                                    issue(ImportIssueType.MISSING_ACTION_COLUMN,
                                          missing + " Default action is " + defaultAction + ". Give up.");
                                    giveup = true;
                                } else {
                                    issue(ImportIssueType.MISSING_ACTION_COLUMN,
                                          missing + " Default action is " + defaultAction + ".");
                                }
                            }
                        } else {
                            // Normal optional column
                            issue(ImportIssueType.MISSING_OPTIONAL_COLUMN, missing);
                        }
                    }
                }

                // Analyze columns that are NOT declared in template
                for (final String name : currentHeaderMapper.getAdditionalNames()) {
                    issue(ImportIssueType.IGNORED_COLUMN,
                          "Column '" + name + "' is not recognized and will be ignored.");
                }

                if (giveup) {
                    // We can give up, no need to continue processing of current table
                    return Evaluation.PRUNE;
                } else {
                    currentTemplateInstance = SheetTemplateInstance.of(currentTemplate, actual); // FIXME remove useless columns?
                    progress.incrementValue();
                    return Evaluation.CONTINUE;
                }
            } else {
                // No associated template was found
                // We can give up, no need to continue processing of current table
                return Evaluation.PRUNE;
            }
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            loadedRows++;
            final ImportRowImpl.Builder builder = ImportRowImpl.builder()
                                                               .defaultAction(features.getDefaultAction())
                                                               .systemId(systemId)
                                                               .templateInstance(currentTemplateInstance)
                                                               .sheetName(currentSheetName)
                                                               .number(location.getGlobalNumber());

            // Do not retrieve cells that do not have a corresponding header
            final int maxIndex = Math.min(currentHeaderMapper.getActualHeader().size(), data.size());
            // If all cells are empty, do nothing
            boolean hasContent = false;
            for (int index = 0; index < maxIndex; index++) {
                final String name = currentHeaderMapper.getActualHeader().getNameAt(index);
                final String value = data.getValue(index);
                if (value != null) {
                    hasContent = true;
                }
                builder.put(name, value);
            }
            if (hasContent) {
                final ImportRow importRow = builder.build();
                for (final Issue issue : importRow.getIssues()) {
                    issuesHandler.issue(issue);
                }
                try {
                    workbookImporter.importRow(importRow, issuesHandler);
                } catch (final RuntimeException e) {
                    final String message = e.getMessage();
                    applicationFailure(message == null ? e.getClass().getSimpleName() : message, importRow);
                }
            }
            progress.incrementValue();
            if (isCancelled()) {
                return Evaluation.PRUNE;
            } else {
                return Evaluation.CONTINUE;
            }
        }

        @Override
        public void processEndTable(String name) {
            sheetChrono.suspend();
            if (currentTemplate != null) {
                issue(ImportIssueType.LOADED_SHEET,
                      "Loaded " + loadedRows + " data rows of '" + currentSheetName
                              + "' sheet in " + sheetChrono + ".");
                workbookImporter.endSheetImport(systemId,
                                                currentSheetName,
                                                currentTemplateInstance,
                                                issuesHandler);
            }
            currentSheetName = null;
            currentTemplate = null;
            currentHeaderMapper = null;
        }

        @Override
        public void processEndTables(String systemId) {
            progress.close();
        }

    }

    @Override
    public void importData(File file,
                           Set<SheetTemplate> templates,
                           WorkbookImporter workbookImporter,
                           IssuesHandler<Issue> issuesHandler,
                           ProgressController controller) throws IOException {
        Checks.isNotNull(file, "file");
        Checks.isNotNull(templates, "templates");
        Checks.isNotNull(workbookImporter, "workbookImporter");
        Checks.isNotNull(controller, "controller");

        final String systemId = file.getPath();

        // Chronometer used to measure workbook loading time.
        final Chronometer chrono = new Chronometer();

        try {
            chrono.start();
            workbookImporter.beginImport(systemId, issuesHandler);

            if (templates.isEmpty()) {
                // We can not do much
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.NO_TEMPLATES)
                                                .severity(ImportIssueType.NO_TEMPLATES.getSeverity())
                                                .description("No templates were passed, won't do anything.")
                                                .metas(Metas.builder()
                                                            .meta(ImpExMetas.WORKBOOK, systemId)
                                                            .build())
                                                .addLocation(WorkbookLocation.builder()
                                                                             .systemId(systemId)
                                                                             .build())
                                                .build());
            } else {
                final SheetParserFactory factory = new SheetParserFactory();
                final SheetParser parser = factory.create(file);
                final Handler handler = new Handler(systemId,
                                                    templates,
                                                    workbookImporter,
                                                    issuesHandler,
                                                    controller,
                                                    features);
                parser.parse(file, features.getPassword(), 1, handler);
                chrono.suspend();
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.LOADED_WORKBOOK)
                                                .severity(ImportIssueType.LOADED_WORKBOOK.getSeverity())
                                                .description("Loaded " + handler.loadedSheets + "/" + handler.totalSheets
                                                        + " sheets of '" + systemId + "' workbook in " + chrono + ".")
                                                .metas(Metas.builder()
                                                            .meta(ImpExMetas.WORKBOOK, systemId)
                                                            .build())
                                                .addLocation(WorkbookLocation.builder()
                                                                             .systemId(systemId)
                                                                             .build())
                                                .build());
            }
        } finally {
            workbookImporter.endImport(systemId,
                                       issuesHandler);
        }
    }
}