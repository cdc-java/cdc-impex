package cdc.impex.core.xml;

import cdc.util.strings.CaseConverter;
import cdc.util.strings.Splitter;

class XmlIo {
    protected XmlIo() {
        super();
    }

    public static final String AS = "as";
    public static final String DATA = "data";
    public static final String HEADER = "header";
    public static final String COLUMN = "column";
    public static final String NAME = "name";
    public static final String NOTE = "note";
    public static final String ROW = "row";
    public static final String SHEET = "sheet";
    public static final String VALUE = "value";
    public static final String WORKBOOK = "workbook";

    private static final Splitter SPLITTER = new Splitter(' ');
    private static final CaseConverter LOWER =
            CaseConverter.builder()
                         .style(CaseConverter.Style.LOWER_CASE)
                         .splitter(SPLITTER)
                         .build();

    public static String toXmlName(String s) {
        return LOWER.splitAndConvert(s.replaceAll("[_ ]", "-"));
    }
}