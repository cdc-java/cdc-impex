package cdc.impex.core.xml;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExFormat;
import cdc.impex.ImpExStatus;
import cdc.impex.exports.ExportRow;
import cdc.impex.exports.StreamExporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.io.xml.XmlWriter;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.lang.Checks;

/**
 * Implementation of Exporter that export data to XML.
 *
 * @author Damien Carbonne
 */
public class XmlStreamExporter implements StreamExporter {
    private final IssuesHandler<Issue> issuesHandler;

    private final ImpExFactoryFeatures features;
    private XmlWriter writer = null;
    private ImpExStatus status = ImpExStatus.INIT;
    /** (name, XML) map, for actual header names. */
    private final Map<String, String> toXml = new HashMap<>();

    public XmlStreamExporter(IssuesHandler<Issue> issuesHandler,
                             ImpExFactoryFeatures features) {
        Checks.isNotNull(issuesHandler, "issuesHandler");
        Checks.isNotNull(features, "features");

        this.issuesHandler = issuesHandler;
        this.features = features;
    }

    public XmlStreamExporter(IssuesHandler<Issue> issuesHandler,
                             ImpExFactory factory) {
        this(issuesHandler, factory.getFeatures());
    }

    private void checkStatus(ImpExStatus expected) {
        Checks.isTrue(this.status == expected, "Invalid status " + status + ", expected " + expected);
    }

    private void beginExport(XmlWriter writer) throws IOException {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;

        this.writer = writer;

        if (features.isEnabled(ImpExFactoryFeatures.Hint.PRETTY_PRINT)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        }

        this.writer.beginDocument();
        this.writer.beginElement(XmlIo.WORKBOOK);
    }

    @Override
    public void beginExport(File file) throws IOException {
        Checks.isNotNull(file, "file");

        final Charset charset = features.getWorkbookWriterFeatures().getCharset();
        if (charset == null) {
            beginExport(new XmlWriter(file));
        } else {
            beginExport(new XmlWriter(file, charset.name()));
        }
    }

    @Override
    public void addReadme(List<SheetTemplateInstance> templateInstances) throws IOException {
        // TODO
    }

    @Override
    public void beginExport(OutputStream out,
                            ImpExFormat format) throws IOException {
        Checks.isNotNull(out, "out");
        Checks.isTrue(format == ImpExFormat.XML, "Unexpected format {}", format);

        final Charset charset = features.getWorkbookWriterFeatures().getCharset();
        if (charset == null) {
            beginExport(new XmlWriter(out));
        } else {
            beginExport(new XmlWriter(out, charset.name()));
        }
    }

    private void writeHeader(SheetTemplateInstance templateInstance) throws IOException {
        writer.beginElement(XmlIo.HEADER);
        for (final String name : templateInstance.getHeader().getSortedNames()) {
            writer.beginElement(XmlIo.COLUMN);
            writer.addAttribute(XmlIo.NAME, name);
            writer.addAttribute(XmlIo.AS, toXml.get(name));
            writer.endElement();
        }
        writer.endElement();
    }

    @Override
    public void beginSheet(SheetTemplateInstance templateInstance,
                           String sheetName,
                           long numberOfRows) throws IOException {
        Checks.isNotNull(templateInstance, "templateInstance");
        Checks.isNotNull(sheetName, "sheetName");

        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;

        // Compute (name, XML) mapping
        toXml.clear();
        for (final String name : templateInstance.getHeader().getSortedNames()) {
            toXml.put(name, XmlIo.toXmlName(name));
        }

        writer.beginElement(XmlIo.SHEET);
        writer.addAttribute(XmlIo.NAME, sheetName);
        writeHeader(templateInstance);
        writer.beginElement(XmlIo.DATA);
    }

    @Override
    public void addRow(ExportRow row) throws IOException {
        checkStatus(ImpExStatus.SHEET);

        final List<Issue> issues = row.getIssues();
        issuesHandler.issues(issues);

        writer.beginElement(XmlIo.ROW);
        for (final String name : row.getTemplateInstance().getHeader().getSortedNames()) {
            final ColumnTemplate<?> column = row.getTemplate().getMatchingColumn(name);
            if (!row.getTemplate().isActionColumn(column) || !features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN)) {
                if (row.containsKey(name)) {
                    writer.beginElement(toXml.get(name));
                    final String value = row.getValue(name);
                    writer.addAttribute(XmlIo.VALUE, value);
                    if (features.isEnabled(ImpExFactoryFeatures.Hint.ADD_DATA_COMMENTS)) {
                        final String comment = row.getComment(name);
                        if (comment != null) {
                            writer.beginElement(XmlIo.NOTE);
                            writer.addElementContent(comment);
                            writer.endElement();
                        }
                    }
                    writer.endElement();
                }
            }
        }
        writer.endElement();
    }

    @Override
    public void endSheet() throws IOException {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.WORKBOOK;
        writer.endElement(); // DATA
        writer.endElement(); // SHEET
    }

    @Override
    public void endExport() throws IOException {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.INIT;

        writer.endElement();
        writer.endDocument();
        writer.flush();
    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}