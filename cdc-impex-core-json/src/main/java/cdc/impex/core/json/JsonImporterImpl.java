package cdc.impex.core.json;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExMetas;
import cdc.impex.core.ImportRowImpl;
import cdc.impex.imports.ImportIssueType;
import cdc.impex.imports.ImportIssues;
import cdc.impex.imports.ImportRow;
import cdc.impex.imports.Importer;
import cdc.impex.imports.WorkbookImporter;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.io.json.JsonEvent;
import cdc.io.json.JsonpParser;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.issues.Metas;
import cdc.issues.locations.WorkbookLocation;
import cdc.office.tables.Header;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.lang.Checks;
import cdc.util.time.Chronometer;

public class JsonImporterImpl implements Importer {
    private static final Logger LOGGER = LogManager.getLogger(JsonImporterImpl.class);
    private final ImpExFactoryFeatures features;

    public JsonImporterImpl(ImpExFactoryFeatures features) {
        this.features = features;
    }

    public JsonImporterImpl(ImpExFactory factory) {
        this(factory.getFeatures());
    }

    @Override
    public void importData(File file,
                           Set<SheetTemplate> templates,
                           WorkbookImporter workbookImporter,
                           IssuesHandler<Issue> issuesHandler,
                           ProgressController controller) throws IOException {
        Checks.isNotNull(file, "file");
        Checks.isNotNull(templates, "templates");
        Checks.isNotNull(workbookImporter, "workbookImporter");
        Checks.isNotNull(controller, "controller");

        final String systemId = file.getPath();

        // Chronometer used to measure workbook loading time.
        final Chronometer chrono = new Chronometer();

        try {
            chrono.start();
            workbookImporter.beginImport(systemId, issuesHandler);

            if (templates.isEmpty()) {
                // We can not do much
                issuesHandler.issue(ImportIssues.builder()
                                                .name(ImportIssueType.NO_TEMPLATES)
                                                .severity(ImportIssueType.NO_TEMPLATES.getSeverity())
                                                .description("No templates were passed, won't do anything.")
                                                .metas(Metas.builder()
                                                            .meta(ImpExMetas.WORKBOOK, systemId)
                                                            .build())
                                                .addLocation(WorkbookLocation.builder()
                                                                             .systemId(systemId)
                                                                             .build())
                                                .build());
            } else {
                final JsonParserFactory factory = Json.createParserFactory(null);
                try (final JsonParser parser = factory.createParser(new BufferedInputStream(new FileInputStream(file)))) {
                    final JsonpParser wrapper = new JsonpParser(parser);
                    final Handler handler = new Handler(wrapper,
                                                        systemId,
                                                        templates,
                                                        workbookImporter,
                                                        issuesHandler,
                                                        controller,
                                                        features);

                    handler.parseWorkbook();

                    chrono.suspend();
                    issuesHandler.issue(ImportIssues.builder()
                                                    .name(ImportIssueType.LOADED_WORKBOOK)
                                                    .severity(ImportIssueType.LOADED_WORKBOOK.getSeverity())
                                                    .description("Loaded " + handler.loadedSheets + "/" + handler.totalSheets
                                                            + " sheets of '" + systemId + "' workbook in " + chrono + ".")
                                                    .metas(Metas.builder()
                                                                .meta(ImpExMetas.WORKBOOK, systemId)
                                                                .build())
                                                    .addLocation(WorkbookLocation.builder()
                                                                                 .systemId(systemId)
                                                                                 .build())
                                                    .build());
                }
            }
        } catch (final RuntimeException e) {
            LOGGER.catching(e);
            throw e;
        } finally {
            workbookImporter.endImport(systemId,
                                       issuesHandler);
        }
    }

    private static record HeaderColumn(String name,
                                       String json) {
    }

    private static class Handler {
        private final JsonpParser wrapper;
        private final String systemId;
        private final Map<String, SheetTemplate> templates = new HashMap<>();
        private final WorkbookImporter workbookImporter;
        private final IssuesHandler<Issue> issuesHandler;
        private SheetTemplate currentTemplate;
        private SheetTemplateInstance currentTemplateInstance;
        private final ProgressSupplier progress;
        private final ImpExFactoryFeatures features;
        /** Total number of sheets in workbook. */
        int totalSheets = 0;
        /** Number of loaded/recognized sheets in workbook. */
        int loadedSheets = 0;
        /** Number of loaded rows in current sheet. */
        int loadedRows = 0;

        private String currentSheetName = null;
        private boolean cancelled = false;

        private final Map<String, String> jsonToTemplateName = new HashMap<>();

        public Handler(JsonpParser wrapper,
                       String systemId,
                       Set<SheetTemplate> templates,
                       WorkbookImporter workbookImporter,
                       IssuesHandler<Issue> issuesHandler,
                       ProgressController controller,
                       ImpExFactoryFeatures features) {
            this.wrapper = wrapper;
            this.systemId = systemId;
            this.workbookImporter = workbookImporter;
            this.issuesHandler = issuesHandler;
            this.progress = new ProgressSupplier(controller);
            this.features = features;
            for (final SheetTemplate template : templates) {
                this.templates.put(template.getName(), template);
            }
            issue(ImportIssueType.LOAD_WORKBOOK,
                  "Load '" + systemId + "' workbook.");
        }

        private void issue(ImportIssueType type,
                           String description) {
            issuesHandler.issue(ImportIssues.builder()
                                            .name(type)
                                            .severity(type.getSeverity())
                                            .description(description)
                                            .metas(Metas.builder()
                                                        .meta(ImpExMetas.WORKBOOK, systemId)
                                                        .meta(ImpExMetas.SHEET, currentSheetName)
                                                        .build())
                                            .addLocation(WorkbookLocation.builder()
                                                                         .systemId(systemId)
                                                                         .sheetName(currentSheetName)
                                                                         .build())
                                            .build());
        }

        private boolean isCancelled() {
            if (cancelled) {
                return true;
            } else {
                if (progress.getController().isCancelled()) {
                    cancelled = true;
                    issue(ImportIssueType.IMPORT_CANCELLED,
                          "Import of '" + systemId + "' was cancelled.");
                    return true;
                } else {
                    return false;
                }
            }
        }

        public void parseWorkbook() {
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.START_OBJECT)) {
                    parseSheet();
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }
            progress.close();
        }

        private void parseSheet() {
            wrapper.next().expectKeyName(JsonIo.SHEET);
            final String sheetName = wrapper.next().getStringValue();
            totalSheets++;
            loadedRows = 0;
            // Chronometer used to measure sheet loading time.
            final Chronometer sheetChrono = new Chronometer();
            sheetChrono.start();
            currentTemplate = getMatchingTemplate(sheetName);
            currentTemplateInstance = null;
            currentSheetName = sheetName;
            if (currentTemplate != null && !isCancelled()) {
                loadedSheets++;
                progress.reset(-1, "Import sheet " + sheetName);

                // Do this first, so that the message that is produced just after
                // is dispatched to the appropriate import handler
                workbookImporter.beginSheetImport(systemId,
                                                  currentSheetName,
                                                  currentTemplate,
                                                  issuesHandler);
                issue(ImportIssueType.LOAD_SHEET,
                      "Load '" + sheetName + "' sheet using '" + currentTemplate.getQName()
                              + "' template.");

                wrapper.next().expectKeyName(JsonIo.HEADER);
                parseHeader();

                workbookImporter.importHeader(currentTemplateInstance,
                                              issuesHandler);

                wrapper.expectKeyName(JsonIo.DATA);
                parseRows();

                sheetChrono.suspend();
                issue(ImportIssueType.LOADED_SHEET,
                      "Loaded " + loadedRows + " data rows of '" + currentSheetName
                              + "' sheet in " + sheetChrono + ".");
                workbookImporter.endSheetImport(systemId,
                                                currentSheetName,
                                                currentTemplateInstance,
                                                issuesHandler);
                currentSheetName = null;
            } else {
                skipToEnd();
                issue(ImportIssueType.IGNORED_SHEET,
                      "Ignored '" + sheetName + "' sheet as there is no associated template.");
            }
            currentTemplate = null;
            currentSheetName = null;
        }

        private void parseHeader() {
            final Header.Builder hb = Header.builder();
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.START_OBJECT)) {
                    final HeaderColumn col = parseHeaderColumn();
                    jsonToTemplateName.put(col.json, col.name);
                    hb.name(col.name);
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }
            wrapper.next();
            this.currentTemplateInstance = SheetTemplateInstance.of(currentTemplate, hb.build());
        }

        private HeaderColumn parseHeaderColumn() {
            String colName = null;
            String colAs = null;
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.KEY_NAME)) {
                    final String name = wrapper.getKeyName();
                    final String value = wrapper.next().getStringValue();
                    switch (name) {
                    case JsonIo.NAME:
                        colName = value;
                        break;
                    case JsonIo.AS:
                        colAs = value;
                        break;
                    default:
                        break;
                    }
                } else {
                    wrapper.expect(JsonEvent.END_OBJECT);
                    break;
                }
            }
            return new HeaderColumn(colName, colAs);
        }

        private void parseRows() {
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.START_OBJECT)) {
                    parseRow();
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }
            wrapper.next();
        }

        private void parseRow() {
            loadedRows++;
            final ImportRowImpl.Builder builder = ImportRowImpl.builder()
                                                               .defaultAction(features.getDefaultAction())
                                                               .systemId(systemId)
                                                               .templateInstance(currentTemplateInstance)
                                                               .sheetName(currentSheetName)
                                                               .number(loadedRows);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.KEY_NAME)) {
                    final String name = wrapper.getKeyName();
                    final String value = wrapper.next().getStringValue();
                    builder.put(jsonToTemplateName.get(name), value);
                } else {
                    wrapper.expect(JsonEvent.END_OBJECT);
                    break;
                }
            }

            final ImportRow importRow = builder.build();
            for (final Issue issue : importRow.getIssues()) {
                issuesHandler.issue(issue);
            }
            workbookImporter.importRow(importRow, issuesHandler);
            progress.incrementValue();
        }

        private SheetTemplate getMatchingTemplate(String name) {
            final SheetTemplate template = templates.get(name);
            if (template == null) {
                // Try pattern matching
                final int pos = name.indexOf('#');
                if (pos >= 0) {
                    return templates.get(name.substring(0, pos));
                } else {
                    return null;
                }
            } else {
                return template;
            }
        }

        private void skipToEnd() {
            int depth = 0;
            while (wrapper.hasNext()) {
                wrapper.next();
                switch (wrapper.getEvent()) {
                case END_ARRAY, END_OBJECT:
                    if (depth == 0) {
                        return;
                    } else {
                        depth--;
                    }
                    break;
                case START_ARRAY, START_OBJECT:
                    depth++;
                    break;
                default:
                    break;
                }
            }
        }
    }
}