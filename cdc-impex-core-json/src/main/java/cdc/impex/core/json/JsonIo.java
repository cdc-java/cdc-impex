package cdc.impex.core.json;

class JsonIo {
    protected JsonIo() {
        super();
    }

    public static final String AS = "as";
    public static final String DATA = "data";
    public static final String HEADER = "header";
    public static final String NAME = "name";
    public static final String SHEET = "sheet";
    public static final String WORKBOOK = "workbook";
}