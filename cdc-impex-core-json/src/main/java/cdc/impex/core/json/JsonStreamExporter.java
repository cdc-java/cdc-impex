package cdc.impex.core.json;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExFormat;
import cdc.impex.ImpExStatus;
import cdc.impex.core.ExportRowImpl;
import cdc.impex.exports.ExportRow;
import cdc.impex.exports.StreamExporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.Primitive;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.lang.Checks;

/**
 * Implementation of Exporter that export data to JSON.
 *
 * @author Damien Carbonne
 */
public class JsonStreamExporter implements StreamExporter {
    private final IssuesHandler<Issue> issuesHandler;

    private final ImpExFactoryFeatures features;
    private JsonGenerator generator = null;
    private ImpExStatus status = ImpExStatus.INIT;
    /** (name, JSON) map, for actual header names. */
    private final Map<String, String> toJson = new HashMap<>();

    public JsonStreamExporter(IssuesHandler<Issue> issuesHandler,
                              ImpExFactoryFeatures features) {
        Checks.isNotNull(issuesHandler, "issuesHandler");
        Checks.isNotNull(features, "features");

        this.issuesHandler = issuesHandler;
        this.features = features;
    }

    public JsonStreamExporter(IssuesHandler<Issue> issuesHandler,
                              ImpExFactory factory) {
        this(issuesHandler, factory.getFeatures());
    }

    private void checkStatus(ImpExStatus expected) {
        Checks.isTrue(this.status == expected, "Invalid status " + status + ", expected " + expected);
    }

    private void beginExport(Writer writer) {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;

        final Map<String, Object> config = new HashMap<>();
        if (features.isEnabled(ImpExFactoryFeatures.Hint.PRETTY_PRINT)) {
            config.put(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE);
        }

        final JsonGeneratorFactory factory = Json.createGeneratorFactory(config);
        this.generator = factory.createGenerator(writer);

        generator.writeStartArray();
    }

    @Override
    public void beginExport(File file) throws IOException {
        Checks.isNotNull(file, "file");

        final Charset charset = features.getWorkbookWriterFeatures().getCharset();
        if (charset == null) {
            beginExport(new BufferedWriter(new FileWriter(file)));
        } else {
            beginExport(new BufferedWriter(new FileWriter(file, charset)));
        }
    }

    @Override
    public void addReadme(List<SheetTemplateInstance> templateInstance) throws IOException {
        // TODO
    }

    @Override
    public void beginExport(OutputStream out,
                            ImpExFormat format) {
        Checks.isNotNull(out, "out");
        Checks.isTrue(format == ImpExFormat.JSON, "Unexpected format {}", format);

        final Charset charset = features.getWorkbookWriterFeatures().getCharset();
        if (charset == null) {
            beginExport(new BufferedWriter(new OutputStreamWriter(out)));
        } else {
            beginExport(new BufferedWriter(new OutputStreamWriter(out, charset)));
        }
    }

    private void writeHeader(SheetTemplateInstance templateInstance) {
        generator.writeStartArray(JsonIo.HEADER);
        for (final String name : templateInstance.getHeader().getSortedNames()) {
            generator.writeStartObject();
            generator.write(JsonIo.NAME, name);
            generator.write(JsonIo.AS, toJson.get(name));
            generator.writeEnd();
        }
        generator.writeEnd();
    }

    @Override
    public void beginSheet(SheetTemplateInstance templateInstance,
                           String sheetName,
                           long numberOfRows) throws IOException {
        Checks.isNotNull(templateInstance, "templateInstance");
        Checks.isNotNull(sheetName, "sheetName");

        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;

        // Compute (name, JSON) mapping
        toJson.clear();
        for (final String name : templateInstance.getHeader().getSortedNames()) {
            toJson.put(name, JsonSchema.toJsonName(name));
        }

        generator.writeStartObject();
        generator.write(JsonIo.SHEET, sheetName);
        writeHeader(templateInstance);
        generator.writeStartArray(JsonIo.DATA);
    }

    @Override
    public void addRow(ExportRow row) throws IOException {
        checkStatus(ImpExStatus.SHEET);

        final List<Issue> issues = row.getIssues();
        issuesHandler.issues(issues);

        generator.writeStartObject();
        for (final String name : row.getTemplateInstance().getHeader().getSortedNames()) {
            // The JSON name
            final String key = toJson.get(name);
            final ColumnTemplate<?> column = row.getTemplateInstance().getTemplate().getMatchingColumn(name);
            if (!row.getTemplate().isActionColumn(column) || !features.isEnabled(ImpExFactoryFeatures.Hint.SKIP_ACTION_COLUMN)) {
                if (row.containsKey(name)) {
                    final String s = row.getValue(name);
                    if (s == null) {
                        generator.write(key, "");
                    } else if (row.hasValidValue(name)) {
                        final Primitive primitive = column.getPrimitive();
                        if (primitive == Primitive.BOOL) {
                            generator.write(key, row.getValueAsBoolean(name));
                        } else if (primitive == Primitive.INT) {
                            generator.write(key, row.getValueAsLong(name));
                        } else if (primitive == Primitive.REAL) {
                            generator.write(key, row.getValueAsDouble(name));
                        } else if (primitive == Primitive.BIG_DEC) {
                            generator.write(key, row.getValueAsBigDecimal(name));
                        } else if (primitive == Primitive.BIG_INT) {
                            generator.write(key, row.getValueAsBigInteger(name));
                        } else {
                            generator.write(key, s);
                        }
                    } else {
                        generator.write(key, s);
                    }
                    // generator.addCell(s);
                    // if (features.isEnabled(ImpExFactoryFeatures.Hint.ADD_DATA_COMMENTS)) {
                    // final String comment = row.getComment(column.getName());
                    // if (comment != null) {
                    // generator.addCellComment(comment);
                    // }
                    // }
                } else if (column.getUsage().isMandatoryForFutureImport()) {
                    generator.write(key, ExportRowImpl.MISSING_DATA);
                } else {
                    generator.write(key, "");
                }
            }
        }
        generator.writeEnd();
    }

    @Override
    public void endSheet() throws IOException {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.WORKBOOK;
        generator.writeEnd(); // DATA
        generator.writeEnd(); // SHEET
    }

    @Override
    public void endExport() throws IOException {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.INIT;

        generator.writeEnd();
        generator.flush();
    }

    @Override
    public void flush() throws IOException {
        generator.flush();
    }

    @Override
    public void close() throws IOException {
        generator.close();
    }
}