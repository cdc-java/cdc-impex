package cdc.impex.core.json;

import java.util.HashMap;
import java.util.Map;

import cdc.util.strings.CaseConverter;
import cdc.util.strings.Splitter;

public final class JsonSchema {
    private JsonSchema() {
    }

    public static final String JSON_SCHEMA = "https://json-schema.org/draft/2020-12/schema";
    public static final String SCHEMA = "$schema";
    public static final String ID = "$id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String COMMENT = "$comment";
    public static final String PATTERN = "pattern";
    public static final String PROPERTY_NAMES = "propertyNames";
    public static final String MIN_PROPERTIES = "minProperties";
    public static final String MAX_PROPERTIES = "maxProperties";
    public static final String DEPENDENCIES = "dependencies";
    public static final String ADDITIONAL_PROPERTIES = "additionalProperties";

    public static final String ALL_OF = "allOf";
    public static final String ANY_OF = "anyOf";
    public static final String ONE_OF = "oneOf";
    public static final String NOT = "not";

    public static final String MULTIPLE_OF = "multipleOf";

    public static final String TYPE = "type";
    public static final String OBJECT = "object";
    public static final String ARRAY = "array";
    public static final String NUMBER = "number";
    public static final String STRING = "string";
    public static final String INTEGER = "integer";
    public static final String BOOLEAN = "boolean";

    public static final String DEFINITIONS = "definitions";

    public static final String ENUM = "enum";
    public static final String CONST = "const";
    public static final String DEFAULT = "default";

    public static final String ITEMS = "items";
    public static final String MIN_ITEMS = "minItems";
    public static final String UNIQUE_ITEMS = "uniqueItems";
    public static final String PROPERTIES = "properties";
    public static final String REQUIRED = "required";
    public static final String EXLUSIVE_MAXIMUM = "exclusiveMaximum";
    public static final String EXLUSIVE_MINIMUM = "exclusiveMinimum";
    public static final String MINIMUM = "minimum";
    public static final String MAXIMUM = "maximum";

    private static final Map<Class<?>, String> TYPES = new HashMap<>();

    static {
        TYPES.put(boolean.class, BOOLEAN);
        TYPES.put(Boolean.class, BOOLEAN);

        TYPES.put(String.class, STRING);
        TYPES.put(Character.class, STRING);
        TYPES.put(char.class, STRING);

        TYPES.put(byte.class, INTEGER);
        TYPES.put(Byte.class, INTEGER);
        TYPES.put(short.class, INTEGER);
        TYPES.put(Short.class, INTEGER);
        TYPES.put(int.class, INTEGER);
        TYPES.put(Integer.class, INTEGER);
        TYPES.put(long.class, INTEGER);
        TYPES.put(Long.class, INTEGER);

        TYPES.put(float.class, NUMBER);
        TYPES.put(Float.class, NUMBER);
        TYPES.put(double.class, NUMBER);
        TYPES.put(Double.class, NUMBER);
    }

    private static final Splitter SPLITTER = new Splitter(' ');
    private static final CaseConverter CAMEL =
            CaseConverter.builder()
                         .style(CaseConverter.Style.CAMEL_CASE)
                         .splitter(SPLITTER)
                         .build();

    public static String toJsonType(Class<?> cls) {
        if (Enum.class.isAssignableFrom(cls)) {
            return STRING;
        } else {
            return TYPES.get(cls);
        }
    }

    public static String toJsonName(String s) {
        return CAMEL.splitAndConvert(s.replaceAll("[_-]", " "));
    }
}