package cdc.impex.core.json;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.Presence;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.impex.templates.TemplateGenerator;
import cdc.util.strings.StringUtils;

/**
 * Class used to generate an empty template file.
 *
 * @author Damien Carbonne
 */
public class JsonTemplateGenerator extends JsonIo implements TemplateGenerator {
    private final ImpExFactoryFeatures features;

    public JsonTemplateGenerator(ImpExFactoryFeatures features) {
        this.features = features;
    }

    public JsonTemplateGenerator(ImpExFactory factory) {
        this(factory.getFeatures());
    }

    private static String toJsonType(Class<?> cls) {
        final String s = JsonSchema.toJsonType(cls);
        return s == null
                ? JsonSchema.STRING
                : s;
    }

    @Override
    public void generate(File file,
                         List<SheetTemplateInstance> templateInstances) throws IOException {
        final Map<String, Object> config = new HashMap<>();
        if (features.isEnabled(ImpExFactoryFeatures.Hint.PRETTY_PRINT)) {
            config.put(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE);
        }

        final JsonGeneratorFactory factory = Json.createGeneratorFactory(config);
        try (final Writer writer = new BufferedWriter(new FileWriter(file));
                final JsonGenerator generator = factory.createGenerator(writer)) {
            generator.writeStartObject();
            generator.write(JsonSchema.SCHEMA, JsonSchema.JSON_SCHEMA);
            generator.write(JsonSchema.ID, "TODO");
            generator.write(JsonSchema.TITLE, "JSON ImpEx");
            generator.write(JsonSchema.DESCRIPTION, "JSON schema for import/export of ...");

            // FIXME Invalid schema. header, data, ...
            generator.writeStartObject(JsonSchema.DEFINITIONS);
            for (final SheetTemplateInstance templateInstance : templateInstances) {
                final SheetTemplate sheet = templateInstance.getTemplate();
                generator.writeStartObject(JsonSchema.toJsonName(sheet.getName()));
                generator.write(JsonSchema.TYPE, JsonSchema.OBJECT);
                if (!StringUtils.isNullOrEmpty(sheet.getDescription())) {
                    generator.write(JsonSchema.DESCRIPTION, sheet.getDescription());
                }
                generator.writeStartObject(JsonSchema.PROPERTIES);

                for (final String columnName : templateInstance.getHeader().getSortedNames()) {
                    final ColumnTemplate<?> column = sheet.getMatchingColumn(columnName);
                    generator.writeStartObject(JsonSchema.toJsonName(columnName));
                    final Class<?> dataType = column.getDataType();
                    generator.write(JsonSchema.TYPE, toJsonType(dataType));
                    if (Enum.class.isAssignableFrom(dataType)) {
                        generator.writeStartArray(JsonSchema.ENUM);
                        for (final Object x : dataType.getEnumConstants()) {
                            generator.write(x.toString());
                        }
                        generator.writeEnd();
                    }
                    if (!StringUtils.isNullOrEmpty(column.getComment())) {
                        generator.write(JsonSchema.DESCRIPTION, column.getComment());
                    }
                    generator.writeEnd();
                }
                generator.writeEnd();

                final List<ColumnTemplate<?>> required = new ArrayList<>();
                for (final ColumnTemplate<?> col : sheet.getColumns()) {
                    if (col.getUsage().getCount(Presence.MANDATORY) > 0) {
                        required.add(col);
                    }
                }
                if (!required.isEmpty()) {
                    generator.writeStartArray(JsonSchema.REQUIRED);
                    for (final ColumnTemplate<?> column : required) {
                        generator.write(JsonSchema.toJsonName(column.getName()));
                    }
                    generator.writeEnd();
                }

                generator.writeEnd();
            }
            generator.writeEnd();
            generator.writeEnd();
        }
    }
}