package cdc.impex.imports;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.impex.templates.ImportAction;
import cdc.impex.templates.Presence;
import cdc.impex.templates.Usage;

class UsageTest {
    @Test
    void testAction() {
        assertSame(Presence.OPTIONAL, Usage.ACTION.getPresence(ImportAction.CREATE));
        assertSame(Presence.OPTIONAL, Usage.ACTION.getPresence(ImportAction.UPDATE));
        assertSame(Presence.OPTIONAL, Usage.ACTION.getPresence(ImportAction.DELETE));
        assertSame(Presence.OPTIONAL, Usage.ACTION.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testMandatoryRoId() {
        assertSame(Presence.MANDATORY, Usage.MANDATORY_RO_ID.getPresence(ImportAction.CREATE));
        assertSame(Presence.MANDATORY, Usage.MANDATORY_RO_ID.getPresence(ImportAction.UPDATE));
        assertSame(Presence.MANDATORY, Usage.MANDATORY_RO_ID.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RO_ID.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testOptionalRoId() {
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RO_ID.getPresence(ImportAction.CREATE));
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RO_ID.getPresence(ImportAction.UPDATE));
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RO_ID.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RO_ID.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testMandatoryRoAtt() {
        assertSame(Presence.MANDATORY, Usage.MANDATORY_RO_ATT.getPresence(ImportAction.CREATE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RO_ATT.getPresence(ImportAction.UPDATE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RO_ATT.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RO_ATT.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testOptionalRoAtt() {
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RO_ATT.getPresence(ImportAction.CREATE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RO_ATT.getPresence(ImportAction.UPDATE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RO_ATT.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RO_ATT.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testOptionalRwAtt() {
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RW_ATT.getPresence(ImportAction.CREATE));
        assertSame(Presence.OPTIONAL, Usage.OPTIONAL_RW_ATT.getPresence(ImportAction.UPDATE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RW_ATT.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.OPTIONAL_RW_ATT.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testMandatoryRwAtt() {
        assertSame(Presence.MANDATORY, Usage.MANDATORY_RW_ATT.getPresence(ImportAction.CREATE));
        assertSame(Presence.OPTIONAL, Usage.MANDATORY_RW_ATT.getPresence(ImportAction.UPDATE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RW_ATT.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RW_ATT.getPresence(ImportAction.IGNORE));
    }

    @Test
    void testDerivedRoAtt() {
        assertSame(Presence.IGNORED, Usage.DERIVED_RO_ATT.getPresence(ImportAction.CREATE));
        assertSame(Presence.IGNORED, Usage.DERIVED_RO_ATT.getPresence(ImportAction.UPDATE));
        assertSame(Presence.IGNORED, Usage.DERIVED_RO_ATT.getPresence(ImportAction.DELETE));
        assertSame(Presence.IGNORED, Usage.MANDATORY_RW_ATT.getPresence(ImportAction.IGNORE));
    }
}