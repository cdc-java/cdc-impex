package cdc.impex;

import java.io.File;

import cdc.office.ss.WorkbookKind;
import cdc.util.files.Files;
import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of supported import formats.
 *
 * @author Damien Carbonne
 */
public enum ImpExFormat {
    CSV,
    ODS,
    XLS,
    XLSM,
    XLSX,
    JSON,
    XML;

    public int getMaxRows() {
        switch (this) {
        case CSV:
            return WorkbookKind.CSV.getMaxRows();
        case ODS:
            return WorkbookKind.ODS.getMaxRows();
        case XLS:
            return WorkbookKind.XLS.getMaxRows();
        case XLSM:
            return WorkbookKind.XLSM.getMaxRows();
        case XLSX:
            return WorkbookKind.XLSX.getMaxRows();
        case JSON:
        case XML:
        default:
            return -1;
        }
    }

    @SuppressWarnings("static-method")
    public boolean isExportFormat() {
        return true;
    }

    public boolean isImportFormat() {
        switch (this) {
        case CSV:
        case ODS:
        case XLS:
        case XLSM:
        case XLSX:
        case JSON:
            return true;
        default:
            return false;
        }
    }

    public static ImpExFormat from(File file) {
        final String ext = Files.getExtension(file);
        for (final ImpExFormat format : values()) {
            if (format.name().equalsIgnoreCase(ext)) {
                return format;
            }
        }
        return null;
    }

    public WorkbookKind getWorkbookKind() {
        switch (this) {
        case CSV:
            return WorkbookKind.CSV;
        case ODS:
            return WorkbookKind.ODS;
        case XLS:
            return WorkbookKind.XLS;
        case XLSX:
            return WorkbookKind.XLSX;
        case XLSM:
            return WorkbookKind.XLSM;
        default:
            throw new UnexpectedValueException(this);
        }
    }
}