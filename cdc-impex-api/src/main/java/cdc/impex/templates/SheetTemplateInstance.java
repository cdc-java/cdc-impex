package cdc.impex.templates;

import java.util.List;

import cdc.office.tables.Header;
import cdc.util.lang.Checks;

/**
 * (SheetTemplate, Header) association.
 *
 * @author Damien Carbonne
 */
public final class SheetTemplateInstance {
    private final SheetTemplate template;
    private final Header header;

    private SheetTemplateInstance(SheetTemplate template,
                                  Header header) {
        this.template = Checks.isNotNull(template, "template");
        this.header = Checks.isNotNull(header, "header");
        Checks.isTrue(header.hasOnlyNames(), "Header must only contain names.");
        // TODO compliance
    }

    /**
     * @return The sheet template.
     */
    public SheetTemplate getTemplate() {
        return template;
    }

    /**
     * @return The actual header.
     *         It is compliant with {@code template} and contains only names.
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Creates a SheetTemplateInstance from a template and a header.
     *
     * @param template The template.
     * @param header The header;
     * @return A new SheetTemplateInstance instance.
     */
    public static SheetTemplateInstance of(SheetTemplate template,
                                           Header header) {
        return new SheetTemplateInstance(template, header);
    }

    /**
     * Creates a SheetTemplateInstance from a template whose header must only contain names.
     *
     * @param template The template.
     * @return A new SheetTemplateInstance instance.
     */
    public static SheetTemplateInstance of(SheetTemplate template) {
        return new SheetTemplateInstance(template,
                                         template.getHeader());
    }

    public static SheetTemplateInstance replace(SheetTemplate template,
                                                List<String> patternReplacement) {
        return new SheetTemplateInstance(template,
                                         Header.builder()
                                               .replacePatterns(template.getHeader(), patternReplacement)
                                               .build());
    }

    public static SheetTemplateInstance replace(SheetTemplate template,
                                                String... patternReplacement) {
        return new SheetTemplateInstance(template,
                                         Header.builder()
                                               .replacePatterns(template.getHeader(), patternReplacement)
                                               .build());
    }
}