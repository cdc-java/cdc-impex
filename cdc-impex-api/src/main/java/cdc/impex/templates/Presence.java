package cdc.impex.templates;

/**
 * Enumeration of possible presences of data.
 *
 * @author Damien Carbonne
 */
public enum Presence {
    /**
     * Data is useless.
     */
    IGNORED,

    /**
     * Data is either defined with a valid value, undefined (empty cell), or set with the ERASE tag (if it is erasable).
     */
    OPTIONAL,

    /**
     * Data must be defined with a valid value.
     */
    MANDATORY
}