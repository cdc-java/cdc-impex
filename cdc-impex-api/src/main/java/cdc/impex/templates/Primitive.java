package cdc.impex.templates;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of <em>primitive</em> types.
 * <p>
 * Typically used for JSON serialization.
 *
 * @author Damien Carbonne
 */
public enum Primitive {
    BOOL,
    INT,
    REAL,
    BIG_INT,
    BIG_DEC,
    CHAR,
    STRING,
    ENUM;

    private static final Map<Class<?>, Primitive> TYPES = new HashMap<>();

    static {
        TYPES.put(boolean.class, BOOL);
        TYPES.put(Boolean.class, BOOL);

        TYPES.put(String.class, STRING);

        TYPES.put(Character.class, CHAR);
        TYPES.put(char.class, CHAR);

        TYPES.put(byte.class, INT);
        TYPES.put(Byte.class, INT);
        TYPES.put(short.class, INT);
        TYPES.put(Short.class, INT);
        TYPES.put(int.class, INT);
        TYPES.put(Integer.class, INT);
        TYPES.put(long.class, INT);
        TYPES.put(Long.class, INT);

        TYPES.put(float.class, REAL);
        TYPES.put(Float.class, REAL);
        TYPES.put(double.class, REAL);
        TYPES.put(Double.class, REAL);

        TYPES.put(BigDecimal.class, BIG_DEC);
        TYPES.put(BigInteger.class, BIG_INT);
    }

    /**
     * Returns the Primitive corresponding to a class.
     *
     * @param cls The class.
     * @return The Primitive corresponding to {@code cls} or {@code null}.
     */
    public static Primitive getPrimitive(Class<?> cls) {
        if (Enum.class.isAssignableFrom(cls)) {
            return ENUM;
        } else {
            return TYPES.get(cls);
        }
    }
}