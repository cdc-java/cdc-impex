package cdc.impex.templates;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import cdc.impex.ImpExCatalog;
import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;

/**
 * Interface of classes that can generate template files from {@link SheetTemplate}s.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface TemplateGenerator {
    /**
     * Generate a template file.
     *
     * @param file The file that must contain templates.
     * @param templateInstances The {@link SheetTemplateInstance}s for which something must be generated.
     * @throws IOException When an IO error occurs.
     */
    public void generate(File file,
                         List<SheetTemplateInstance> templateInstances) throws IOException;

    /**
     * Generate a template file.
     *
     * @param file The file that must contain templates.
     * @param templateInstances The {@link SheetTemplate}s for which something must be generated.
     * @throws IOException When an IO error occurs.
     */
    public default void generate(File file,
                                 SheetTemplateInstance... templateInstances) throws IOException {
        generate(file, Arrays.asList(templateInstances));
    }

    public static void generate(File file,
                                List<String> templateNames,
                                ImpExCatalog catalog,
                                Function<SheetTemplate, SheetTemplateInstance> instancier,
                                ImpExFactoryFeatures features) throws IOException {
        final ImpExFactory factory = new ImpExFactory(features);
        final TemplateGenerator generator = factory.createTemplateGenerator(file);

        generator.generate(file,
                           catalog.getTemplatesAsList(templateNames).stream().map(instancier::apply).toList());
    }
}