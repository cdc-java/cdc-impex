package cdc.impex.templates;

import cdc.impex.imports.ImportRow;
import cdc.util.lang.Checks;

/**
 * Definition for each {@link ImportAction} of the expected presence of data or column.
 * <p>
 * When a column is missing, actual data is necessarily undefined.<br>
 * When a column is present, actual data can be defined, undefined, or declared as {@link ImportRow#ERASE ERASE}.
 *
 * @author Damien Carbonne
 */
public final class Usage {
    private final String name;
    private final Presence[] presence = new Presence[ImportAction.values().length];
    private final boolean erasable;
    private final int[] count = new int[Presence.values().length];

    private Usage(String name,
                  Presence create,
                  Presence update,
                  boolean erasable,
                  Presence delete,
                  Presence ignore) {
        this.name = Checks.isNotNull(name, "name");
        this.presence[ImportAction.CREATE.ordinal()] = Checks.isNotNull(create, "create");
        this.presence[ImportAction.UPDATE.ordinal()] = Checks.isNotNull(update, "update");
        this.erasable = erasable;
        Checks.isTrue(!erasable || update != Presence.MANDATORY, "Can not erase a " + update + " column.");
        this.presence[ImportAction.DELETE.ordinal()] = Checks.isNotNull(delete, "delete");
        this.presence[ImportAction.IGNORE.ordinal()] = ignore;

        this.count[create.ordinal()]++;
        this.count[update.ordinal()]++;
        this.count[delete.ordinal()]++;
        this.count[ignore.ordinal()]++;
    }

    private Usage(String name,
                  Presence create,
                  Presence update,
                  boolean erasable,
                  Presence delete) {
        this(name, create, update, erasable, delete, Presence.IGNORED);
    }

    /**
     * Usage dedicated to the action column.
     * <ul>
     * <li>CREATE: {@link Presence#OPTIONAL}
     * <li>UPDATE: {@link Presence#OPTIONAL}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#OPTIONAL}
     * </ul>
     */
    public static final Usage ACTION =
            new Usage("Optional Action (OOO)",
                      Presence.OPTIONAL,
                      Presence.OPTIONAL,
                      false,
                      Presence.OPTIONAL,
                      Presence.OPTIONAL);

    /**
     * The column is a <em>mandatory</em> <em>read-only</em> part of the entity <em>id</em>.
     * <ul>
     * <li>CREATE: {@link Presence#MANDATORY}
     * <li>UPDATE: {@link Presence#MANDATORY}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#MANDATORY}
     * </ul>
     * It is used to designate an entity.
     * It <em>can not</em> be changed.
     */
    public static final Usage MANDATORY_RO_ID =
            builder().name("Mandatory RO identifier (MMM)")
                     .create(Presence.MANDATORY)
                     .update(Presence.MANDATORY, false)
                     .delete(Presence.MANDATORY)
                     .build();

    /**
     * The column is an <em>optional</em> <em>read-only</em> part of the entity <em>id</em>.
     * <ul>
     * <li>CREATE: {@link Presence#OPTIONAL}
     * <li>UPDATE: {@link Presence#OPTIONAL}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#OPTIONAL}
     * </ul>
     * It is used to designate an entity.
     * It <em>can not</em> be changed.
     */
    public static final Usage OPTIONAL_RO_ID =
            builder().name("Optional RO identifier (OOO)")
                     .create(Presence.OPTIONAL)
                     .update(Presence.OPTIONAL, false)
                     .delete(Presence.OPTIONAL)
                     .build();

    /**
     * The column is a <em>mandatory</em> <em>read-only</em> entity <em>attribute</em>.
     * <ul>
     * <li>CREATE: {@link Presence#MANDATORY}
     * <li>UPDATE: {@link Presence#IGNORED}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#IGNORED}
     * </ul>
     * It <em>must</em> be set at <em>creation time</em>.<br>
     * It <em>can not</em> be changed.
     */
    public static final Usage MANDATORY_RO_ATT =
            builder().name("Mandatory RO attribute (MII)")
                     .create(Presence.MANDATORY)
                     .update(Presence.IGNORED, false)
                     .delete(Presence.IGNORED)
                     .build();

    /**
     * The column is a <em>optional</em> <em>read-only</em> entity <em>attribute</em>.
     * <ul>
     * <li>CREATE: {@link Presence#OPTIONAL}
     * <li>UPDATE: {@link Presence#IGNORED}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#IGNORED}
     * </ul>
     * It <em>can</em> be set at <em>creation time</em>.<br>
     * It <em>can not</em> be changed.
     */
    public static final Usage OPTIONAL_RO_ATT =
            builder().name("Optional RO attribute (OII)")
                     .create(Presence.OPTIONAL)
                     .update(Presence.IGNORED, false)
                     .delete(Presence.IGNORED)
                     .build();

    /**
     * The column is a <em>mandatory</em> <em>read-write</em> entity <em>attribute</em>.
     * <ul>
     * <li>CREATE: {@link Presence#MANDATORY}
     * <li>UPDATE: {@link Presence#OPTIONAL}, <em>CAN NOT</em> be erased
     * <li>DELETE: {@link Presence#IGNORED}
     * </ul>
     * It <em>can not</em> be {@code null}.<br>
     * It <em>must</em> be set at <em>creation time</em>.<br>
     * It <em>can</em> be changed.
     */
    public static final Usage MANDATORY_RW_ATT =
            builder().name("Mandatory RW attribute (MOI)")
                     .create(Presence.MANDATORY)
                     .update(Presence.OPTIONAL, false)
                     .delete(Presence.IGNORED)
                     .build();

    /**
     * The column is a <em>optional</em> <em>read-write</em> entity <em>attribute</em>.
     * <ul>
     * <li>CREATE: {@link Presence#OPTIONAL}
     * <li>UPDATE: {@link Presence#OPTIONAL}, <em>CAN</em> be erased
     * <li>DELETE: {@link Presence#IGNORED}
     * </ul>
     * It <em>can</em> be {@code null}.<br>
     * It <em>can</em> be set at <em>creation time</em>.<br>
     * It <em>can</em> be changed.
     */
    public static final Usage OPTIONAL_RW_ATT =
            builder().name("Optional RW attribute (OOI)")
                     .create(Presence.OPTIONAL)
                     .update(Presence.OPTIONAL, true)
                     .delete(Presence.IGNORED)
                     .build();

    /**
     * The column is a <em>read-only</em> <em>derived (computed)</em> entity <em>attribute</em>.
     * <ul>
     * <li>CREATE: {@link Presence#IGNORED}
     * <li>UPDATE: {@link Presence#IGNORED}, can not be erased
     * <li>DELETE: {@link Presence#IGNORED}
     * </ul>
     * It is useless for imports but can be useful in exports.
     */
    public static final Usage DERIVED_RO_ATT =
            builder().name("Derived RO attribute (III)")
                     .create(Presence.IGNORED)
                     .update(Presence.IGNORED, false)
                     .delete(Presence.IGNORED)
                     .build();

    public String getName() {
        return name;
    }

    public Presence getPresence(ImportAction action) {
        return presence[action.ordinal()];
    }

    /**
     * @return {@code true} if the column can be erased during update.
     */
    public boolean isErasable() {
        return erasable;
    }

    public int getCount(Presence presence) {
        return count[presence.ordinal()];
    }

    public ImportAction[] getImportActions(Presence presence) {
        final ImportAction[] array = new ImportAction[getCount(presence)];
        int index = 0;
        for (final ImportAction action : ImportAction.values()) {
            if (getPresence(action) == presence) {
                array[index] = action;
                index++;
            }
        }
        return array;
    }

    /**
     * @param action The action.
     * @return {@code true} if this usage is mandatory for {@code action}.
     */
    public boolean isMandatoryFor(ImportAction action) {
        return getPresence(action) == Presence.MANDATORY;
    }

    /**
     * @param action The action.
     * @return {@code true} if this usage is optional for {@code action}.
     */
    public boolean isOptionalFor(ImportAction action) {
        return getPresence(action) == Presence.OPTIONAL;
    }

    /**
     * @param action The action.
     * @return {@code true} if this usage is ignored for {@code action}.
     */
    public boolean isIgnoredFor(ImportAction action) {
        return getPresence(action) == Presence.IGNORED;
    }

    /**
     * @return {@code true} if this usage is mandatory for a future import.
     */
    public boolean isMandatoryForFutureImport() {
        return isMandatoryFor(ImportAction.CREATE);
    }

    @Override
    public String toString() {
        return getName();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String name;
        private Presence create = Presence.OPTIONAL;
        private Presence update = Presence.OPTIONAL;
        private boolean erasable = true;
        private Presence delete = Presence.OPTIONAL;

        private Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder create(Presence presence) {
            this.create = presence;
            return this;
        }

        public Builder update(Presence presence,
                              boolean erasable) {
            this.update = presence;
            this.erasable = erasable;
            return this;
        }

        public Builder delete(Presence presence) {
            this.delete = presence;
            return this;
        }

        public Usage build() {
            return new Usage(name,
                             create,
                             update,
                             erasable,
                             delete);
        }
    }
}