package cdc.impex.templates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.issues.IssueSeverity;
import cdc.office.ss.ContentValidation;
import cdc.office.tables.HeaderCell;
import cdc.office.tables.HeaderCell.NameCell;
import cdc.office.tables.HeaderCell.PatternCell;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;
import cdc.util.strings.StringConversion;
import cdc.validation.checkers.Checker;

/**
 * Description of a column template.
 * <p>
 * A column is described by:
 * <ul>
 * <li>A header (name or pattern).<br>
 * A name column represents a single actual column.<br>
 * A pattern column represents a set of actual columns.
 * <li>A {@link Usage}.
 * <li>A data type.
 * <li>An optional default value.
 * <li>An optional description.<br>
 * It is used when sample files are generated.
 * <li>An optional {@link Checker}.<br>
 * It can be used to add more checks to data type, for example, to check the length of string, the domain of a number, ...
 * <li>The severity of a failed check.
 * <li>A string to data type converter.
 * <li>A data type to string converter.
 * <li>An optional Content Validation type.
 * <li>An optional Content Validation operator.
 * <li>An optional list of Content Validation values.
 * </ul>
 * <b>WARNING:</b> content validation should be compliant with data type and checker.
 * <p>
 * If data type is a primitive or enum data type, default content validation data are automatically set.
 *
 * @author Damien Carbonne
 *
 * @param <T> The column data type.
 */
public final class ColumnTemplate<T> {
    private final HeaderCell header;
    private final Usage usage;
    private final Class<T> dataType;
    private final T def;
    private final Primitive primitive;
    private final String description;
    private final Checker<T> checker;
    private final IssueSeverity checkFailureSeverity;
    private final Function<String, T> importConverter;
    private final Function<T, String> exportConverter;
    private final ContentValidation.Type cvType;
    private final ContentValidation.Operator cvOperator;
    private final List<String> cvValues;

    protected ColumnTemplate(BaseBuilder<?, T> builder) {
        Checks.isTrue(builder.cvType.accepts(builder.cvOperator),
                      builder.cvType + " is not compliant with " + builder.cvOperator + " operator.");
        Checks.isTrue(builder.cvOperator.isCompliantWithValues(builder.cvValues.size()),
                      builder.cvOperator + " is not compliant with " + builder.cvValues.size() + " values.");

        this.header = Checks.isNotNull(builder.header, "header");
        this.usage = Checks.isNotNull(builder.usage, "usage");
        this.dataType = Checks.isNotNull(builder.dataType, "dataType");
        this.def = builder.def;
        this.primitive = Primitive.getPrimitive(builder.dataType);
        this.description = builder.description;
        this.checker = builder.checker;
        this.checkFailureSeverity = Checks.isNotNull(builder.checkFailureSeverity, "checkFailureSeverity");
        this.importConverter = Checks.isNotNull(builder.importConverter, "importConverter");
        this.exportConverter = builder.exportConverter == null ? Object::toString : builder.exportConverter;
        this.cvType = builder.cvType;
        this.cvOperator = builder.cvOperator;
        this.cvValues = Collections.unmodifiableList(builder.cvValues);
    }

    public HeaderCell getHeader() {
        return header;
    }

    /**
     * @return {@code true} if the header of the column is a name.
     */
    public boolean isName() {
        return header instanceof HeaderCell.NameCell;
    }

    public void checkIsName() {
        Checks.isTrue(isName(), "{} is not a name.", this);
    }

    /**
     * @return {@code true} if the header of the column is a pattern.
     */
    public boolean isPattern() {
        return header instanceof HeaderCell.PatternCell;
    }

    public void checkIsPattern() {
        Checks.isTrue(isPattern(), "{} is not a pattern.", this);
    }

    /**
     * @return The column label (its name or pattern).
     */
    public String getLabel() {
        return header.getLabel();
    }

    /**
     * Returns the column name.
     * <p>
     * <b>WARNING:</b> column must have a {@link NameCell name header}.
     *
     * @return The column name.
     * @throws AssertionError When the header of this template is not a name.
     */
    public String getName() {
        Checks.assertTrue(isName(), "'{}' is not a name column.", this);
        return ((HeaderCell.NameCell) header).getName();
    }

    /**
     * Returns the column pattern.
     * <p>
     * <b>WARNING:</b> column must have a {@link PatternCell pattern header}.
     *
     * @return The column pattern.
     * @throws AssertionError When the header of this template is not a pattern.
     */
    public Pattern getPattern() {
        Checks.assertTrue(isPattern(), "'{}' is not a pattern column.", this);
        return ((HeaderCell.PatternCell) header).getPattern();
    }

    /**
     * @return The column usage.
     */
    public Usage getUsage() {
        return usage;
    }

    /**
     * @return The column data type.
     */
    public Class<T> getDataType() {
        return dataType;
    }

    /**
     * @return The column default value.
     */
    public T getDef() {
        return def;
    }

    /**
     * @return The {@link Primitive} type of this column, or {@code null}.
     */
    public Primitive getPrimitive() {
        return primitive;
    }

    /**
     * @return The column description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return The checker or {@code null}.
     */
    public Checker<T> getCheckerOrNull() {
        return checker;
    }

    public boolean hasChecker() {
        return checker != null;
    }

    /**
     * @return The severity of a check failure.
     */
    public IssueSeverity getCheckFailureSeverity() {
        return checkFailureSeverity;
    }

    /**
     * @return the converter used to convert a string to data type.
     */
    public Function<String, T> getImportConverter() {
        return importConverter;
    }

    /**
     * @return the converter used to convert a data type to a string.
     */
    public Function<T, String> getExportConverter() {
        return exportConverter;
    }

    /**
     * @return The comment that should be used to describe this column.
     */
    public String getComment() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Usage: ")
               .append(getUsage().getName())
               .append('\n')
               .append("Erasable: ")
               .append(getUsage().isErasable() ? "Yes" : "No")
               .append('\n')
               .append("Type: ")
               .append(getDataType().getSimpleName());

        if (getDef() != null) {
            builder.append('\n')
                   .append("Default: ")
                   .append(getExportConverter().apply(getDef()));
        }

        if (getCheckerOrNull() != null) {
            builder.append('\n')
                   .append("Check: ")
                   .append(getCheckerOrNull().explain());
        }

        if (getDescription() != null) {
            builder.append("\n\n")
                   .append(getDescription());
        }
        return builder.toString();
    }

    /**
     * @return The content validation type.
     */
    public ContentValidation.Type getContentValidationType() {
        return cvType;
    }

    /**
     * @return The content validation operator.
     */
    public ContentValidation.Operator getContentValidationOperator() {
        return cvOperator;
    }

    /**
     * @return The content validation values.
     */
    public List<String> getContentValidationValues() {
        return cvValues;
    }

    @Override
    public String toString() {
        return "[" + getLabel()
                + " " + getUsage()
                + " " + getDataType().getSimpleName()
                + "]";
    }

    /**
     * @return A new Builder initialized with data from this Template.
     */
    public Builder<T> newBuilder() {
        return builder(this);
    }

    /**
     * Creates an empty new Builder.
     *
     * @param <T> The data type.
     * @param dataType The data class.
     * @return A new Builder typed with {@code dataType}.
     */
    public static <T> Builder<T> builder(Class<T> dataType) {
        return new Builder<>(dataType);
    }

    /**
     * Creates a new Builder initialized from an existing Template.
     *
     * @param <T> The data type.
     * @param model The Template model.
     * @return A new Builder initialized with {@code model}.
     */
    public static <T> Builder<T> builder(ColumnTemplate<T> model) {
        return new Builder<>(model);
    }

    public static <E> ListBuilder<E> listBuilder(Class<E> elementType) {
        return new ListBuilder<E>();
    }

    public static <E> SetBuilder<E> setBuilder(Class<E> elementType) {
        return new SetBuilder<E>();
    }

    protected static abstract class BaseBuilder<B extends BaseBuilder<B, T>, T> {
        private final Class<T> dataType;
        private HeaderCell header;
        private Usage usage = Usage.OPTIONAL_RW_ATT;
        private T def = null;
        private String description;
        private Checker<T> checker;
        private IssueSeverity checkFailureSeverity = IssueSeverity.CRITICAL;
        protected Function<String, T> importConverter;
        protected Function<T, String> exportConverter;
        protected ContentValidation.Type cvType = ContentValidation.Type.ANY;
        protected ContentValidation.Operator cvOperator = ContentValidation.Operator.NONE;
        protected final List<String> cvValues = new ArrayList<>();

        private static class Bucket {
            final Function<String, ?> importConverter;
            final ContentValidation.Type cvType;
            final ContentValidation.Operator cvOperator;
            final List<String> cvValues;

            Bucket(Function<String, ?> importConverter,
                   ContentValidation.Type cvType,
                   ContentValidation.Operator cvOperator,
                   List<String> cvValues) {
                this.importConverter = importConverter;
                this.cvType = cvType;
                this.cvOperator = cvOperator;
                this.cvValues = cvValues;
            }
        }

        private static final Map<Class<?>, Bucket> BUCKETS = new HashMap<>();

        static {
            BUCKETS.put(Boolean.class,
                        new Bucket(StringConversion::asBoolean,
                                   ContentValidation.Type.ANY,
                                   ContentValidation.Operator.NONE,
                                   Collections.emptyList()));
            BUCKETS.put(boolean.class,
                        new Bucket(StringConversion::asBoolean,
                                   ContentValidation.Type.ANY,
                                   ContentValidation.Operator.NONE,
                                   Collections.emptyList()));
            BUCKETS.put(Character.class,
                        new Bucket(StringConversion::asChar,
                                   ContentValidation.Type.TEXT_LENGTH,
                                   ContentValidation.Operator.EQUAL,
                                   List.of("1")));
            BUCKETS.put(char.class,
                        new Bucket(StringConversion::asChar,
                                   ContentValidation.Type.TEXT_LENGTH,
                                   ContentValidation.Operator.EQUAL,
                                   List.of("1")));
            BUCKETS.put(Byte.class,
                        new Bucket(StringConversion::asByte,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Byte.toString(Byte.MIN_VALUE),
                                           Byte.toString(Byte.MAX_VALUE))));
            BUCKETS.put(byte.class,
                        new Bucket(StringConversion::asByte,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Byte.toString(Byte.MIN_VALUE),
                                           Byte.toString(Byte.MAX_VALUE))));
            BUCKETS.put(Short.class,
                        new Bucket(StringConversion::asShort,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Short.toString(Short.MIN_VALUE),
                                           Short.toString(Short.MAX_VALUE))));
            BUCKETS.put(short.class,
                        new Bucket(StringConversion::asShort,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Short.toString(Short.MIN_VALUE),
                                           Short.toString(Short.MAX_VALUE))));
            BUCKETS.put(Integer.class,
                        new Bucket(StringConversion::asInt,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Integer.toString(Integer.MIN_VALUE),
                                           Integer.toString(Integer.MAX_VALUE))));
            BUCKETS.put(int.class,
                        new Bucket(StringConversion::asInt,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Integer.toString(Integer.MIN_VALUE),
                                           Integer.toString(Integer.MAX_VALUE))));
            BUCKETS.put(Long.class,
                        new Bucket(StringConversion::asLong,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Long.toString(Long.MIN_VALUE),
                                           Long.toString(Long.MAX_VALUE))));
            BUCKETS.put(long.class,
                        new Bucket(StringConversion::asLong,
                                   ContentValidation.Type.INTEGER,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Long.toString(Long.MIN_VALUE),
                                           Long.toString(Long.MAX_VALUE))));
            BUCKETS.put(Float.class,
                        new Bucket(StringConversion::asFloat,
                                   ContentValidation.Type.DECIMAL,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Float.toString(-Float.MAX_VALUE),
                                           Float.toString(Float.MAX_VALUE))));
            BUCKETS.put(float.class,
                        new Bucket(StringConversion::asFloat,
                                   ContentValidation.Type.DECIMAL,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Float.toString(-Float.MAX_VALUE),
                                           Float.toString(Float.MAX_VALUE))));
            BUCKETS.put(Double.class,
                        new Bucket(StringConversion::asDouble,
                                   ContentValidation.Type.DECIMAL,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Double.toString(-Double.MAX_VALUE),
                                           Double.toString(Double.MAX_VALUE))));
            BUCKETS.put(double.class,
                        new Bucket(StringConversion::asDouble,
                                   ContentValidation.Type.DECIMAL,
                                   ContentValidation.Operator.BETWEEN,
                                   List.of(Double.toString(-Double.MAX_VALUE),
                                           Double.toString(Double.MAX_VALUE))));
            BUCKETS.put(String.class,
                        new Bucket(Function.identity(),
                                   ContentValidation.Type.ANY,
                                   ContentValidation.Operator.NONE,
                                   Collections.emptyList()));
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        private void buildDefaultData() {
            final Function<String, ?> f;
            final ContentValidation.Type type;
            final ContentValidation.Operator operator;
            final List<String> values;
            if (this.dataType.isEnum()) {
                @SuppressWarnings("unchecked")
                final Class<Enum<?>> c = (Class<Enum<?>>) dataType;
                f = s -> StringConversion.asRawEnum(s, c);
                type = ContentValidation.Type.LIST;
                operator = ContentValidation.Operator.NONE;
                values = new ArrayList<>();
                for (final Enum<?> e : c.getEnumConstants()) {
                    values.add(e.name());
                }
            } else {
                final Bucket bucket = BUCKETS.get(dataType);
                if (bucket == null) {
                    f = null;
                    type = ContentValidation.Type.ANY;
                    operator = ContentValidation.Operator.NONE;
                    values = Collections.emptyList();
                } else {
                    f = bucket.importConverter;
                    type = bucket.cvType;
                    operator = bucket.cvOperator;
                    values = bucket.cvValues;
                }
            }
            @SuppressWarnings("unchecked")
            final Function<String, T> g = (Function<String, T>) f;
            this.importConverter = g;
            this.cvType = type;
            this.cvOperator = operator;
            this.cvValues.addAll(values);
        }

        protected BaseBuilder(Class<T> dataType) {
            this.dataType = dataType;
            buildDefaultData();
        }

        protected BaseBuilder(ColumnTemplate<T> model) {
            this(model.dataType);

            header(model.header);
            usage(model.usage);
            def(model.def);
            description(model.description);
            checker(model.checker);
            checkFailureSeverity(model.checkFailureSeverity);
        }

        public final B header(HeaderCell header) {
            this.header = header;
            return self();
        }

        public final B name(String name) {
            this.header = HeaderCell.name(name);
            return self();
        }

        public final B pattern(String pattern) {
            this.header = HeaderCell.pattern(pattern);
            return self();
        }

        public final B usage(Usage usage) {
            this.usage = usage;
            return self();
        }

        public final B def(T def) {
            this.def = def;
            return self();
        }

        public final B description(String description) {
            this.description = description;
            return self();
        }

        public final B checker(Checker<T> checker) {
            this.checker = checker;
            return self();
        }

        public final B checkFailureSeverity(IssueSeverity checkFailureSeverity) {
            this.checkFailureSeverity = checkFailureSeverity;
            return self();
        }

        public final ColumnTemplate<T> build() {
            return new ColumnTemplate<>(this);
        }
    }

    /**
     * Standard builder.
     *
     * @param <T> The column data type.
     */
    public static final class Builder<T> extends BaseBuilder<Builder<T>, T> {
        protected Builder(Class<T> dataType) {
            super(dataType);
        }

        protected Builder(ColumnTemplate<T> model) {
            super(model);
            importConverter(model.importConverter);
            exportConverter(model.exportConverter);
            cvType(model.cvType);
            cvOperator(model.cvOperator);
            cvValues(model.cvValues);
        }

        public Builder<T> importConverter(Function<String, T> converter) {
            this.importConverter = converter;
            return self();
        }

        public Builder<T> exportConverter(Function<T, String> converter) {
            this.exportConverter = converter;
            return self();
        }

        public Builder<T> cvType(ContentValidation.Type cvType) {
            this.cvType = cvType;
            return self();
        }

        public Builder<T> cvOperator(ContentValidation.Operator cvOperator) {
            this.cvOperator = cvOperator;
            return self();
        }

        public Builder<T> cvValue(String value) {
            this.cvValues.clear();
            this.cvValues.add(value);
            return self();
        }

        public Builder<T> cvValues(String value1,
                                   String value2) {
            this.cvValues.clear();
            this.cvValues.add(value1);
            this.cvValues.add(value2);
            return self();
        }

        public Builder<T> cvValues(String... values) {
            this.cvValues.clear();
            Collections.addAll(this.cvValues, values);
            return self();
        }

        public Builder<T> cvValues(List<String> values) {
            this.cvValues.clear();
            this.cvValues.addAll(values);
            return this;
        }
    }

    /**
     * Builder for lists of elements.
     *
     * @param <E> The element data type.
     */
    public static final class ListBuilder<E> extends BaseBuilder<ListBuilder<E>, List<E>> {
        protected ListBuilder() {
            super(Introspection.uncheckedCast(List.class));
        }

        public ListBuilder<E> exportConverter(Function<E, String> converter,
                                              String delimiter) {
            this.exportConverter = list -> list == null
                    ? null
                    : list.stream()
                          .map(converter)
                          .collect(Collectors.joining(delimiter));
            return self();
        }

        public ListBuilder<E> importConverter(Function<String, E> converter,
                                              String delimiterRegex) {
            final Pattern pattern = Pattern.compile(delimiterRegex);
            this.importConverter = s -> s == null
                    ? null
                    : Stream.of(pattern.split(s))
                            .map(converter)
                            .toList();
            return self();
        }
    }

    /**
     * Builder for sets of elements.
     *
     * @param <E> The element data type.
     */
    public static final class SetBuilder<E> extends BaseBuilder<SetBuilder<E>, Set<E>> {
        protected SetBuilder() {
            super(Introspection.uncheckedCast(Set.class));
        }

        public SetBuilder<E> exportConverter(Function<E, String> converter,
                                             String delimiter,
                                             Comparator<E> comparator) {
            this.exportConverter = list -> list == null
                    ? null
                    : list.stream()
                          .sorted(comparator)
                          .map(converter)
                          .collect(Collectors.joining(delimiter));
            return self();
        }

        public SetBuilder<E> exportConverter(Function<E, String> converter,
                                             String delimiter) {
            this.exportConverter = list -> list == null
                    ? null
                    : list.stream()
                          .map(converter)
                          .sorted()
                          .collect(Collectors.joining(delimiter));
            return self();
        }

        public SetBuilder<E> importConverter(Function<String, E> converter,
                                             String delimiterRegex) {
            final Pattern pattern = Pattern.compile(delimiterRegex);
            this.importConverter = s -> s == null
                    ? null
                    : Stream.of(pattern.split(s))
                            .map(converter)
                            .collect(Collectors.toSet());
            return self();
        }
    }
}