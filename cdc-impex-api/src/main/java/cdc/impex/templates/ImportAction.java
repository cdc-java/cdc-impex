package cdc.impex.templates;

/**
 * Enumeration of possible actions for each row.
 *
 * @author Damien Carbonne
 */
public enum ImportAction {
    /**
     * The row describes the creation of something.
     * <p>
     * It must contain all identification and read-only-attributes.
     */
    CREATE,
    /**
     * The row describes the update of something.
     * <p>
     * It must contain all identification attributes and those that must be changed.
     */
    UPDATE,

    /**
     * The row describes the removal of something.
     * <p>
     * It must contain identification attributes.
     */
    DELETE,

    /**
     * The row must be ignored.
     * <p>
     * <b>Note:</b> an empty action is also ignored.
     */
    IGNORE
}
