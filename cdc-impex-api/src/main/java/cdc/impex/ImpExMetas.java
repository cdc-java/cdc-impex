package cdc.impex;

public final class ImpExMetas {
    private ImpExMetas() {
    }

    public static final String WORKBOOK = "workbook";
    public static final String SHEET = "sheet";
    public static final String COLUMN = "column";
}