package cdc.impex.imports;

import cdc.impex.ImpExStatus;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public class CheckedSheetImporter implements SheetImporter {
    protected ImpExStatus status = ImpExStatus.WORKBOOK;

    public CheckedSheetImporter() {
        super();
    }

    protected void checkStatus(ImpExStatus expected) {
        ImpExStatus.checkStatus(status, expected);
    }

    @Override
    public void beginSheetImport(String systemId,
                                 String sheetName,
                                 SheetTemplate template,
                                 IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;
    }

    @Override
    public void importHeader(SheetTemplateInstance templateInstance,
                             IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.SHEET);
    }

    @Override
    public void importRow(ImportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.SHEET);
    }

    @Override
    public void endSheetImport(String systemId,
                               String sheetName,
                               SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.WORKBOOK;
    }
}