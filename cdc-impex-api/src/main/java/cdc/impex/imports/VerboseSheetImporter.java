package cdc.impex.imports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public class VerboseSheetImporter implements SheetImporter {
    private static final Logger LOGGER = LogManager.getLogger(VerboseSheetImporter.class);

    private final SheetImporter delegate;

    public VerboseSheetImporter(SheetImporter delegate) {
        this.delegate = delegate;
    }

    @Override
    public void beginSheetImport(String systemId,
                                 String sheetName,
                                 SheetTemplate template,
                                 IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("beginSheetImport({}, {}, {})", systemId, sheetName, template);
        delegate.beginSheetImport(systemId, sheetName, template, issuesHandler);
    }

    @Override
    public void importHeader(SheetTemplateInstance templateInstance,
                             IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("importHeader({})", templateInstance.getHeader());
        delegate.importHeader(templateInstance, issuesHandler);
    }

    @Override
    public void importRow(ImportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("importRow({})", row);
        delegate.importRow(row, issuesHandler);
    }

    @Override
    public void endSheetImport(String systemId,
                               String sheetName,
                               SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("endSheetImport({}, {}, {})", systemId, sheetName, templateInstance);
        delegate.endSheetImport(systemId, sheetName, templateInstance, issuesHandler);
    }
}