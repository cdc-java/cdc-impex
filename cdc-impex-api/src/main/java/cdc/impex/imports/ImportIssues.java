package cdc.impex.imports;

import cdc.issues.Issue;

public final class ImportIssues {
    public static final String DOMAIN = "Import";

    private ImportIssues() {
    }

    public static Issue.Builder<?> builder() {
        return Issue.builder().domain(DOMAIN);
    }
}