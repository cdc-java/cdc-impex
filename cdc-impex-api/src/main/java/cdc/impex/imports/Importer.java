package cdc.impex.imports;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import cdc.impex.ImpExCatalog;
import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.templates.SheetTemplate;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.events.ProgressController;

/**
 * Interface implemented by classes that can analyze a file to prepare data for import.
 * <p>
 * An importer analyzes input files, detects issues in them, find the appropriate
 * template, and calls the passed SheetImporter.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface Importer {
    /**
     * Invoked to analyze a file that contains data to import.
     *
     * @param file The file to import.
     * @param templates The templates for which matching sheets must be loaded.<br>
     *            Non-matching sheets are ignored.
     * @param workbookImporter The workbook importer .<br>
     *            <b>Note:</b> one should use {@link WorkbookImporter#fromDelegates(WorkbookImporter, java.util.function.Function)}
     *            or {@link WorkbookImporter#fromDelegates(WorkbookImporter, java.util.Map)} to compose individual SheetImporters
     *            into one WorkbookImporter.
     * @param issuesHandler The issues handler.
     * @param controller The progress controller.
     * @throws IllegalArgumentException When {@code file}, {@code templates}, {@code sheetImporter} or {@code issuesHandler} is
     *             {@code null}.
     * @throws IOException When an IO error occurs.
     */
    public void importData(File file,
                           Set<SheetTemplate> templates,
                           WorkbookImporter workbookImporter,
                           IssuesHandler<Issue> issuesHandler,
                           ProgressController controller) throws IOException;

    public static void importData(File file,
                                  Set<String> templateNames,
                                  ImpExCatalog catalog,
                                  IssuesHandler<Issue> issuesHandler,
                                  ProgressController controller,
                                  ImpExFactoryFeatures features) throws IOException {
        final ImpExFactory factory = new ImpExFactory(features);
        final Importer importer = factory.createImporter(file);

        importer.importData(file,
                            catalog.getTemplatesAsSet(templateNames),
                            catalog.createWorkbookImporterFor(templateNames),
                            issuesHandler,
                            controller);
    }
}