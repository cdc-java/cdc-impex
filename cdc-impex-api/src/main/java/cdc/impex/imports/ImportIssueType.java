package cdc.impex.imports;

import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.ImportAction;
import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;

public enum ImportIssueType implements IssueSeverityItem {

    /**
     * A workbook is being loaded.
     */
    LOAD_WORKBOOK(IssueSeverity.INFO),

    /**
     * A workbook has been loaded.
     */
    LOADED_WORKBOOK(IssueSeverity.INFO),

    /**
     * A sheet is being loaded.
     */
    LOAD_SHEET(IssueSeverity.INFO),

    /**
     * A sheet has been loaded.
     */
    LOADED_SHEET(IssueSeverity.INFO),

    /**
     * A sheet of the loaded workbook is ignored because there is no associated template.
     */
    IGNORED_SHEET(IssueSeverity.INFO),

    /**
     * No templates were passed, and nothing can be done.
     */
    NO_TEMPLATES(IssueSeverity.CRITICAL),

    /**
     * Too many templates were passed with a XSV file.
     */
    TOO_MANY_TEMPLATES(IssueSeverity.CRITICAL),

    /**
     * A cell contains ERASE pattern and it should not.<br>
     * <p>
     * ERASE should only be used when action is {@link ImportAction#UPDATE}.<br>
     * It shall never be used with mandatory columns.
     */
    UNEXPECTED_ERASE(IssueSeverity.CRITICAL),

    /**
     * A cell contains data and it should not.
     */
    UNEXPECTED_DATA(IssueSeverity.CRITICAL),

    /**
     * An always mandatory column is absent in sheet header.
     */
    MISSING_MANDATORY_COLUMN(IssueSeverity.CRITICAL),

    /**
     * A partially mandatory column is absent.
     */
    MISSING_PARTIALLY_MANDATORY_COLUMN(IssueSeverity.INFO),

    /**
     * An optional column is absent.
     */
    MISSING_OPTIONAL_COLUMN(IssueSeverity.INFO),

    /**
     * The action column is absent.<br>
     * This disables all actions when default action is undefined or is {@link ImportAction#IGNORE}.
     */
    MISSING_ACTION_COLUMN(IssueSeverity.INFO),

    /**
     * A column that is not declared in template has been found.<br>
     * It will be ignored.
     */
    IGNORED_COLUMN(IssueSeverity.INFO),

    /**
     * No data for a mandatory column in a row.
     */
    MISSING_MANDATORY_DATA(IssueSeverity.CRITICAL),

    /**
     * Data can not be converted to expected declared data type.
     */
    NON_CONVERTIBLE_DATA(IssueSeverity.CRITICAL),

    /**
     * Data is rejected by the checker.<br>
     * The severity can be any of {@link IssueSeverity}, as set by {@link ColumnTemplate#getCheckFailureSeverity()}.
     */
    NON_COMPLIANT_DATA(null),

    /**
     * The import was cancelled.
     */
    IMPORT_CANCELLED(IssueSeverity.INFO),

    /**
     * The application failed to execute the required action, for reasons such as:
     * <ul>
     * <li>The {@link ImportAction#CREATE} would have caused a duplication.
     * <li>The {@link ImportAction#UPDATE} or {@link ImportAction#DELETE}
     * referenced data does not exist.
     * <li>The user does not have sufficient privileges.
     * <li>Any application error.
     * </ul>
     */
    APP_FAILURE(null),

    /**
     * The application sends an informative message.
     */
    APP_INFO(IssueSeverity.INFO);

    private final IssueSeverity severity;

    private ImportIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    @Override
    public IssueSeverity getSeverity() {
        return severity;
    }
}