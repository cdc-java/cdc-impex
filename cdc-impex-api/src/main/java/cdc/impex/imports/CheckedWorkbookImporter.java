package cdc.impex.imports;

import cdc.impex.ImpExStatus;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.lang.Checks;

public class CheckedWorkbookImporter extends CheckedSheetImporter implements WorkbookImporter {
    public CheckedWorkbookImporter() {
        status = ImpExStatus.INIT;
    }

    @Override
    protected void checkStatus(ImpExStatus expected) {
        Checks.isTrue(this.status == expected, "Invalid status " + status + ", expected " + expected);
    }

    @Override
    public void beginImport(String systemId,
                            IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;
    }

    @Override
    public void endImport(String systemId,
                          IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.INIT;
    }
}