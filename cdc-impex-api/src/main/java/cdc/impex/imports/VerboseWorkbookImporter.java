package cdc.impex.imports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public class VerboseWorkbookImporter implements WorkbookImporter {
    private static final Logger LOGGER = LogManager.getLogger(VerboseWorkbookImporter.class);

    private final WorkbookImporter delegate;

    public VerboseWorkbookImporter(WorkbookImporter delegate) {
        this.delegate = delegate;
    }

    @Override
    public void beginImport(String systemId,
                            IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("beginImport({})", systemId);
        delegate.beginImport(systemId, issuesHandler);
    }

    @Override
    public void beginSheetImport(String systemId,
                                 String sheetName,
                                 SheetTemplate template,
                                 IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("beginSheetImport({}, {}, {})", systemId, sheetName, template);
        delegate.beginSheetImport(systemId, sheetName, template, issuesHandler);
    }

    @Override
    public void importHeader(SheetTemplateInstance templateInstance,
                             IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("importHeader({})", templateInstance.getHeader());
        delegate.importHeader(templateInstance, issuesHandler);
    }

    @Override
    public void importRow(ImportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("importRow({})", row);
        delegate.importRow(row, issuesHandler);
    }

    @Override
    public void endSheetImport(String systemId,
                               String sheetName,
                               SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("endSheetImport({}, {}, {})", systemId, sheetName, templateInstance);
        delegate.endSheetImport(systemId, sheetName, templateInstance, issuesHandler);
    }

    @Override
    public void endImport(String systemId,
                          IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("endImport({})", systemId);
        delegate.endImport(systemId, issuesHandler);
    }
}