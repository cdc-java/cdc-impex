package cdc.impex.imports;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import cdc.impex.templates.SheetTemplate;
import cdc.util.events.ProgressController;

@FunctionalInterface
public interface ImportAnalyzer {
    /**
     * Loads and analyzes a candidate import input file, and generates analysis file.
     *
     * @param file The file that is candidate to be imported.
     * @param templates The templates for which matching sheets must be loaded.<br>
     *            Non-matching sheets are ignored.
     * @param issuesFile The file containing analysis issues.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void analyze(File file,
                        Set<SheetTemplate> templates,
                        File issuesFile,
                        ProgressController controller) throws IOException;
}