package cdc.impex.imports;

import java.util.Map;
import java.util.function.Function;

import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.lang.Checks;

/**
 * This interface adds workbook level notifications to {@link SheetImporter}.
 *
 * @author Damien Carbonne
 */
public interface WorkbookImporter extends SheetImporter {
    public static final WorkbookImporter QUIET_VOID = new CheckedWorkbookImporter();
    public static final WorkbookImporter VERBOSE_VOID = new VerboseWorkbookImporter(QUIET_VOID);

    /**
     * Invoked to notify the beginning of import.
     *
     * @param systemId The system if of the imported data.
     * @param issuesHandler The issues handler that should be used by the application to
     *            generate new issues.
     */
    public default void beginImport(String systemId,
                                    IssuesHandler<Issue> issuesHandler) {
        // Ignore
    }

    /**
     * Invoked to notify the end of import.
     *
     * @param systemId The system if of the imported data.
     * @param issuesHandler The issues handler that should be used by the application to
     *            generate new issues.
     */
    public default void endImport(String systemId,
                                  IssuesHandler<Issue> issuesHandler) {
        // Ignore
    }

    /**
     * Creates a delegating workbook importer.
     * <p>
     * When the import of a sheet starts, an appropriate sheet importer is selected,
     * if possible, among {@code delegates}.
     * If no appropriate sheet importer is found, the default workbook importer is used. The default will usually ignore things.
     *
     * @param def The default workbook importer.<br>
     *            It can be {@link #QUIET_VOID}, {@link #VERBOSE_VOID},
     *            or any other valid WorkbookImporter.
     * @param function The function that maps template names to the associated sheet importers.
     * @return A new WorkbookImporter that delegates import if possible, or use the default workbook importer otherwise.
     */
    public static WorkbookImporter fromDelegates(WorkbookImporter def,
                                                 Function<String, SheetImporter> function) {
        Checks.isNotNull(def, "def");
        Checks.isNotNull(function, "function");

        return new WorkbookImporter() {
            private SheetImporter delegate = null;

            private SheetImporter get() {
                return delegate == null ? def : delegate;
            }

            @Override
            public void beginImport(String systemId,
                                    IssuesHandler<Issue> issuesHandler) {
                def.beginImport(systemId, issuesHandler);
            }

            @Override
            public void beginSheetImport(String systemId,
                                         String sheetName,
                                         SheetTemplate template,
                                         IssuesHandler<Issue> issuesHandler) {
                delegate = function.apply(template.getName());
                get().beginSheetImport(systemId, sheetName, template, issuesHandler);
            }

            @Override
            public void importHeader(SheetTemplateInstance templateInstance,
                                     IssuesHandler<Issue> issuesHandler) {
                get().importHeader(templateInstance, issuesHandler);
            }

            @Override
            public void importRow(ImportRow row,
                                  IssuesHandler<Issue> issuesHandler) {
                get().importRow(row, issuesHandler);
            }

            @Override
            public void endSheetImport(String systemId,
                                       String sheetName,
                                       SheetTemplateInstance templateInstance,
                                       IssuesHandler<Issue> issuesHandler) {
                get().endSheetImport(systemId, sheetName, templateInstance, issuesHandler);
                delegate = null;
            }

            @Override
            public void endImport(String systemId,
                                  IssuesHandler<Issue> issuesHandler) {
                def.endImport(systemId, issuesHandler);
            }
        };
    }

    public static WorkbookImporter fromDelegates(Function<String, SheetImporter> function) {
        return fromDelegates(QUIET_VOID, function);
    }

    /**
     * Creates a delegating workbook importer.
     *
     * @param def The default workbook importer.<br>
     *            It can be {@link #QUIET_VOID}, {@link #VERBOSE_VOID},
     *            or any other valid WorkbookImporter.
     * @param map The map from template names to the associated sheet importers.
     * @return A new WorkbookImporter that delegates import if possible, or use the default workbook importer otherwise.
     */
    public static WorkbookImporter fromDelegates(WorkbookImporter def,
                                                 Map<String, SheetImporter> map) {
        return fromDelegates(def, map::get);
    }

    public static WorkbookImporter fromDelegates(Map<String, SheetImporter> map) {
        return fromDelegates(map::get);
    }
}