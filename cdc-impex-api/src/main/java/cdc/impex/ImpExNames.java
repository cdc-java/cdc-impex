package cdc.impex;

public final class ImpExNames {
    private ImpExNames() {
    }

    public static final String CLS = "cls";
    public static final String COLUMN = "column";
    public static final String NAME = "name";
}