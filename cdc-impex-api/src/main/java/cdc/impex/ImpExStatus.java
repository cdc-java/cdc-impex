package cdc.impex;

import cdc.util.lang.Checks;

public enum ImpExStatus {
    INIT,
    WORKBOOK,
    SHEET,
    ROW;

    public static void checkStatus(ImpExStatus actual,
                                   ImpExStatus expected) {
        Checks.isTrue(actual == expected, "Invalid status " + actual + ", expected " + expected);
    }
}