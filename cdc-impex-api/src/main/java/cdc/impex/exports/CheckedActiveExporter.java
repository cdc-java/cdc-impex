package cdc.impex.exports;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import cdc.impex.ImpExFormat;
import cdc.impex.ImpExStatus;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesHandler;
import cdc.util.events.ProgressController;

/**
 * Implementation of ActiveExporter that do some checks and delegates the real job to another ActiveExporter.
 *
 * @author Damien Carbonne
 */
public class CheckedActiveExporter implements ActiveExporter {
    private final ActiveExporter delegate;
    private ImpExStatus status = ImpExStatus.INIT;

    public CheckedActiveExporter(ActiveExporter delegate) {
        this.delegate = delegate;
    }

    protected void checkStatus(ImpExStatus expected) {
        ImpExStatus.checkStatus(status, expected);
    }

    @Override
    public void beginExport(File file,
                            IssuesHandler<Issue> issuesHandler,
                            ProgressController controller) throws IOException {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;
        delegate.beginExport(file, issuesHandler, controller);
    }

    @Override
    public void beginExport(OutputStream out,
                            String systemId,
                            ImpExFormat format,
                            IssuesHandler<Issue> issuesHandler,
                            ProgressController controller) throws IOException {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;
        delegate.beginExport(out, systemId, format, issuesHandler, controller);
    }

    @Override
    public void beginSheet(SheetTemplateInstance templateInstance,
                           long numberOfRows) throws IOException {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;
        delegate.beginSheet(templateInstance, numberOfRows);
    }

    @Override
    public void beginSheet(SheetTemplate template,
                           long numberOfRows) throws IOException {
        beginSheet(SheetTemplateInstance.of(template), numberOfRows);
    }

    @Override
    public ExportRow nextRow() throws IOException {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.ROW;
        return delegate.nextRow();
    }

    @Override
    public void addRow() throws IOException {
        checkStatus(ImpExStatus.ROW);
        delegate.addRow();
        status = ImpExStatus.SHEET;
    }

    @Override
    public void endSheet() throws IOException {
        checkStatus(ImpExStatus.SHEET);
        delegate.endSheet();
        status = ImpExStatus.WORKBOOK;
    }

    @Override
    public void endExport() throws IOException {
        checkStatus(ImpExStatus.WORKBOOK);
        delegate.endExport();
        status = ImpExStatus.INIT;
    }

    @Override
    public IssuesHandler<Issue> getIssuesHandler() {
        return delegate.getIssuesHandler();
    }

    @Override
    public void issue(ExportIssueType type,
                      IssueSeverity severity,
                      String description) {
        delegate.issue(type, severity, description);
    }

    @Override
    public void issue(ExportIssueType type,
                      String description) {
        delegate.issue(type, description);
    }
}