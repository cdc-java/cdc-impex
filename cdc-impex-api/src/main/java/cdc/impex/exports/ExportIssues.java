package cdc.impex.exports;

import cdc.issues.Issue;

public final class ExportIssues {
    private ExportIssues() {
    }

    public static final String DOMAIN = "Export";

    public static Issue.Builder<?> builder() {
        return Issue.builder().domain(DOMAIN);
    }
}