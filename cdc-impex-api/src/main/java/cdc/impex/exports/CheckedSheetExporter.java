package cdc.impex.exports;

import cdc.impex.ImpExStatus;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

/**
 * Implementation of {@link SheetExporter} that inserts checks.
 * <p>
 * It can be extended and used like this:
 * <pre>{@code
 * public class FooExporter extends CheckedSheetExporter {
 *     &#64;Override
 *     public void beginSheetExport(SheetTemplate template,
 *                                  IssuesHandler<? super ExportIssue> issuesHandler) {
 *         super.beginSheetExport(template, issuesHandler);
 *         // Add your code here
 *     }
 *
 *     &#64;Override
 *     public int getNumberOfRemainingRows() {
 *        checkStatus(ImpExStatus.SHEET);
 *        return ...;
 *     }
 *
 *     &#64;Override
 *     public void exportRow(ExportRow row,
 *                           IssuesHandler<? super ExportIssue> issuesHandler) {
 *         super.exportRow(row, issuesHandler);
 *         // Add your code here
 *     }
 *
 *     &#64;Override
 *     public void endSheetExport(SheetTemplate template,
 *                                IssuesHandler<? super ExportIssue> issuesHandler) {
 *         super.endSheetExport(template, issuesHandler);
 *         // Add your code here
 *     }
 * }
 * }</pre>
 *
 * @author Damien Carbonne
 */
public class CheckedSheetExporter implements SheetExporter {
    protected ImpExStatus status = ImpExStatus.WORKBOOK;

    public CheckedSheetExporter() {
        super();
    }

    protected void checkStatus(ImpExStatus expected) {
        ImpExStatus.checkStatus(status, expected);
    }

    @Override
    public void beginSheetExport(SheetTemplateInstance templateInstance,
                                 IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.SHEET;
    }

    @Override
    public int getNumberOfRemainingRows() {
        checkStatus(ImpExStatus.SHEET);
        return 0;
    }

    @Override
    public void exportRow(ExportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.SHEET);
        throw new UnsupportedOperationException();
    }

    @Override
    public void endSheetExport(SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.SHEET);
        status = ImpExStatus.WORKBOOK;
    }
}