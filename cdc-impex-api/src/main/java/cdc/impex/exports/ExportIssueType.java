package cdc.impex.exports;

import cdc.impex.templates.ColumnTemplate;
import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;

public enum ExportIssueType implements IssueSeverityItem {

    /**
     * A workbook is being created.
     */
    GENERATE_WORKBOOK(IssueSeverity.INFO),

    /**
     * A workbook has been created.
     */
    GENERATED_WORKBOOK(IssueSeverity.INFO),

    /**
     * A sheet is being created.
     */
    GENERATE_SHEET(IssueSeverity.INFO),

    /**
     * A sheet has been created.
     */
    GENERATED_SHEET(IssueSeverity.INFO),

    IGNORED_SHEET(IssueSeverity.INFO),

    /**
     * No data for a mandatory column in a row.
     */
    MISSING_MANDATORY_DATA(IssueSeverity.CRITICAL),

    /**
     * Data can not be converted to expected declared data type.
     */
    NON_CONVERTIBLE_DATA(IssueSeverity.CRITICAL),

    /**
     * Data is rejected by the checker.<br>
     * The severity can be any of {@link IssueSeverity}, as set by {@link ColumnTemplate#getCheckFailureSeverity()}.
     */
    NON_COMPLIANT_DATA(null),

    /**
     * The application failed to execute the required action, for reasons such as:
     */
    APP_FAILURE(null),

    /**
     * The application sends an informative message.
     */
    APP_INFO(IssueSeverity.INFO);

    private final IssueSeverity severity;

    private ExportIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    @Override
    public IssueSeverity getSeverity() {
        return severity;
    }
}