package cdc.impex.exports;

import cdc.impex.ImpExStatus;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public class CheckedWorkbookExporter extends CheckedSheetExporter implements WorkbookExporter {
    public CheckedWorkbookExporter() {
        status = ImpExStatus.INIT;
    }

    @Override
    public void beginExport(IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.INIT);
        status = ImpExStatus.WORKBOOK;
    }

    @Override
    public void endExport(IssuesHandler<Issue> issuesHandler) {
        checkStatus(ImpExStatus.WORKBOOK);
        status = ImpExStatus.INIT;
    }
}