package cdc.impex.exports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public class VerboseSheetExporter implements SheetExporter {
    private static final Logger LOGGER = LogManager.getLogger(VerboseSheetExporter.class);
    protected final SheetExporter delegate;

    public VerboseSheetExporter(SheetExporter delegate) {
        this.delegate = delegate;
    }

    @Override
    public void beginSheetExport(SheetTemplateInstance templateInstance,
                                 IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("{}.beginSheetExtraction({})", this, templateInstance.getTemplate().getName());
        delegate.beginSheetExport(templateInstance, issuesHandler);
    }

    @Override
    public int getNumberOfRemainingRows() {
        return delegate.getNumberOfRemainingRows();
    }

    @Override
    public void exportRow(ExportRow row,
                          IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("{}.extractRow({})", this, row.getNumber());
        delegate.exportRow(row, issuesHandler);
    }

    @Override
    public void endSheetExport(SheetTemplateInstance templateInstance,
                               IssuesHandler<Issue> issuesHandler) {
        LOGGER.info("{}.endSheetExtraction({})", this, templateInstance.getTemplate().getName());
        delegate.endSheetExport(templateInstance, issuesHandler);
    }
}