package cdc.impex.exports;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFormat;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.events.ProgressController;

public class VerboseExporter implements Exporter {
    private static final Logger LOGGER = LogManager.getLogger(VerboseExporter.class);
    private final Exporter delegate;

    public VerboseExporter(Exporter delegate) {
        this.delegate = delegate;
    }

    @Override
    public void exportData(File file,
                           List<SheetTemplateInstance> templateInstances,
                           WorkbookExporter workbookExporter,
                           IssuesHandler<Issue> issuesHandler,
                           ProgressController controller) throws IOException {
        LOGGER.info("Export {}", file);
        delegate.exportData(file,
                            templateInstances,
                            workbookExporter,
                            issuesHandler,
                            controller);
        LOGGER.info("Exported {}", file);
    }

    @Override
    public void exportData(OutputStream out,
                           String systemId,
                           ImpExFormat format,
                           List<SheetTemplateInstance> templateInstances,
                           WorkbookExporter workbookExporter,
                           IssuesHandler<Issue> issuesHandler,
                           ProgressController controller) throws IOException {
        LOGGER.info("Export {}", systemId);
        delegate.exportData(out,
                            systemId,
                            format,
                            templateInstances,
                            workbookExporter,
                            issuesHandler,
                            controller);
        LOGGER.info("Exported {}", systemId);
    }
}