package cdc.impex.tools.swing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.table.AbstractTableModel;

import cdc.impex.templates.SheetTemplate;
import cdc.impex.tools.Settings;
import cdc.util.function.Consumers;

public class SheetTemplatesTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAMES = {
            "Selected",
            "Domain",
            "Name" };
    private static final int COLUMN_SELECTED = 0;
    private static final int COLUMN_DOMAIN = 1;
    private static final int COLUMN_NAME = 2;

    private final transient List<SheetTemplate> values = new ArrayList<>();
    private final transient Set<SheetTemplate> selected = new HashSet<>();

    private final Consumers<Event> listeners = new Consumers<>();

    public SheetTemplatesTableModel() {
        super();

        Settings.CATALOG.getTemplates()
                        .stream()
                        .sorted(SheetTemplate.DOMAIN_NAME_COMPARATOR)
                        .forEach(this::add);
    }

    public enum Event {
        CONTENT_CHANGED,
        SELECTION_CHANGED
    }

    public void addListener(Consumer<Event> listener) {
        listeners.add(listener);
    }

    public void removeListener(Consumer<Event> listener) {
        listeners.remove(listener);
    }

    public void add(SheetTemplate value) {
        this.values.add(value);
        fireTableRowsInserted(values.size() - 1, values.size() - 1);
        listeners.accept(Event.CONTENT_CHANGED);
    }

    public void add(List<SheetTemplate> values) {
        if (!values.isEmpty()) {
            final int previous = this.values.size();
            this.values.addAll(values);
            final int current = this.values.size();
            fireTableRowsInserted(previous, current - 1);
            listeners.accept(Event.CONTENT_CHANGED);
        }
    }

    public void clear() {
        if (!values.isEmpty()) {
            final int size = this.values.size();
            this.values.clear();
            fireTableRowsDeleted(0, size - 1);
            listeners.accept(Event.CONTENT_CHANGED);
        }
        selected.clear();
    }

    public SheetTemplate getTemplate(int index) {
        if (index >= 0 && index < values.size()) {
            return values.get(index);
        } else {
            return null;
        }
    }

    public void selectAll() {
        selected.addAll(values);
        fireTableDataChanged();
        listeners.accept(Event.SELECTION_CHANGED);
    }

    public void select(String name) {
        for (int index = 0; index < values.size(); index++) {
            final SheetTemplate template = values.get(index);
            if (template.getName().equals(name)) {
                if (!selected.contains(template)) {
                    selected.add(template);
                    fireTableRowsUpdated(index, index);
                    listeners.accept(Event.SELECTION_CHANGED);
                }
                break;
            }
        }
    }

    public void deselectAll() {
        selected.clear();
        fireTableDataChanged();
        listeners.accept(Event.SELECTION_CHANGED);
    }

    public Set<SheetTemplate> getSelectedTemplates() {
        return selected;
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        if (column >= 0 && column < getColumnCount()) {
            return COLUMN_NAMES[column];
        } else {
            return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case COLUMN_DOMAIN:
        case COLUMN_NAME:
            return String.class;
        case COLUMN_SELECTED:
            return Boolean.class;
        default:
            return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex,
                             int columnIndex) {
        final SheetTemplate value = values.get(rowIndex);
        switch (columnIndex) {
        case COLUMN_DOMAIN:
            return value.getDomain();
        case COLUMN_NAME:
            return value.getName();
        case COLUMN_SELECTED:
            return selected.contains(value);
        default:
            return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex,
                                  int columnIndex) {
        return columnIndex == COLUMN_SELECTED;
    }

    @Override
    public void setValueAt(Object aValue,
                           int rowIndex,
                           int columnIndex) {
        if (columnIndex == COLUMN_SELECTED) {
            final SheetTemplate value = values.get(rowIndex);
            final boolean s = (Boolean) aValue;
            if (s) {
                selected.add(value);
            } else {
                selected.remove(value);
            }
        }
    }
}