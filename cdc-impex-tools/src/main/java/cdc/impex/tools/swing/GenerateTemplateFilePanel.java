package cdc.impex.tools.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.ImpExFormat;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.impex.templates.TemplateGenerator;
import cdc.ui.swing.GridBagConstraintsBuilder;
import cdc.ui.swing.app.ExceptionDialog;
import cdc.ui.swing.enums.EnumTypeComboBoxModel;
import cdc.ui.swing.props.PropertiesPanel;
import cdc.ui.swing.widgets.HtmlFrame;
import cdc.util.strings.StringUtils;

/**
 * Sub-panel used to generate template files.
 *
 * @author Damien Carbonne
 */
class GenerateTemplateFilePanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(GenerateTemplateFilePanel.class);
    static final Set<ImpExFormat> IMPEX_FORMATS = EnumSet.of(ImpExFormat.CSV, ImpExFormat.XLSX);
    private final ImpExToolboxFrame wFrame;
    private final JTextField wPrefix = new JTextField();
    private final JComboBox<ImpExFormat> wFormat =
            new JComboBox<>(EnumTypeComboBoxModel.create(ImpExFormat.class)
                                                 .withPredicate(IMPEX_FORMATS::contains));
    private final JButton wGenerate = new JButton("Generate");

    public GenerateTemplateFilePanel(ImpExToolboxFrame frame) {
        this.wFrame = frame;
        setLayout(new GridBagLayout());
        final PropertiesPanel wProperties = new PropertiesPanel();

        add(wProperties,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(0)
                                     .weightx(1.0)
                                     .weighty(0.0)
                                     .insets(5, 5, 0, 5)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());

        wProperties.addProperty("Template prefix", wPrefix, true);
        wProperties.addProperty("Template format", wFormat, false);

        wFormat.setSelectedItem(ImpExFormat.XLSX);
        wPrefix.setText(wFrame.margs.prefix);

        add(wGenerate,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(1)
                                     .weightx(1.0)
                                     .weighty(1.0)
                                     .insets(5, 0, 0, 5)
                                     .anchor(GridBagConstraints.FIRST_LINE_END)
                                     .build());

        wGenerate.addActionListener(e -> generate());
    }

    private void generate() {
        final File outputDir = wFrame.wOutputDir.getFile();
        final String prefix = StringUtils.isNullOrEmpty(wPrefix.getText())
                ? "impex"
                : wPrefix.getText();
        final ImpExFormat format = (ImpExFormat) wFormat.getSelectedItem();
        final List<SheetTemplate> templates = new ArrayList<>(wFrame.wSheetTemplates.getModel().getSelectedTemplates());

        final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.BEST);

        final File file = new File(outputDir, prefix + "." + format.name().toLowerCase());
        final TemplateGenerator generator = factory.createTemplateGenerator(file);
        try {
            LOGGER.info("Generate {}", file);
            generator.generate(file, templates.stream().map(SheetTemplateInstance::replace).toList());
            LOGGER.info("Generated {}", file);
            HtmlFrame.showFrame(GenerateTemplateFilePanel.this,
                                "Generated template file",
                                "Generated",
                                file);

        } catch (final IOException e) {
            ExceptionDialog.showExceptionDialog(this, e, null, null);
        }
    }
}