package cdc.impex.tools.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import cdc.impex.tools.ImpExToolbox;
import cdc.ui.swing.GridBagConstraintsBuilder;
import cdc.ui.swing.SwingUtils;
import cdc.ui.swing.props.PropertiesPanel;
import cdc.ui.swing.widgets.FileSelector;

/**
 * Main frame of Swing version of ImpExToolbox.
 *
 * @author Damien Carbonne
 */
public class ImpExToolboxFrame extends JFrame {
    private static final long serialVersionUID = 1L;

    final ImpExToolbox.MainArgs margs;
    final FileSelector wOutputDir = new FileSelector(FileSelector.Mode.DIRECTORIES_ONLY);
    final SheetTemplatesPanel wSheetTemplates;
    final SheetTemplateViewer wSheetTemplate = new SheetTemplateViewer();
    private About wAbout = null;

    public ImpExToolboxFrame(ImpExToolbox.MainArgs margs) {
        this.margs = margs;

        setTitle("ImpEx Toolbox");
        setIconImage(SwingUtils.getApplicationImage());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(1200, 800));

        this.wSheetTemplates = new SheetTemplatesPanel(this);
        this.wSheetTemplates.getTable().getSelectionModel().addListSelectionListener(event -> {
            if (!event.getValueIsAdjusting()) {
                final int rowIndex = wSheetTemplates.getTable().getSelectedRow();
                this.wSheetTemplate.setTemplate(this.wSheetTemplates.getModel().getTemplate(rowIndex));
            }
        });
        buildContent();

        buildMenus();
        pack();
    }

    private void buildContent() {
        final JSplitPane wSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        getContentPane().add(wSplitPane);

        wSplitPane.setTopComponent(buildTop());
        wSplitPane.setBottomComponent(buildDown());
    }

    private JComponent buildTop() {
        final JSplitPane wSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        wSplitPane.setOneTouchExpandable(true);

        wSplitPane.setLeftComponent(buildTopLeft());
        wSplitPane.setRightComponent(buildTopRight());
        return wSplitPane;
    }

    private JComponent buildTopLeft() {
        final JPanel wPanel = new JPanel();
        wPanel.setLayout(new GridBagLayout());

        final PropertiesPanel wProperties = new PropertiesPanel();
        wProperties.addProperty("Output directory", wOutputDir, true);
        wProperties.setLabelToolTipTextAt(0, "Directory into which files will be generated.");
        wOutputDir.setFile(margs.outputDir);

        wPanel.add(wProperties,
                   GridBagConstraintsBuilder.builder()
                                            .gridx(0)
                                            .gridy(0)
                                            .weightx(1.0)
                                            .weighty(0.0)
                                            .insets(5, 5, 0, 5)
                                            .fill(GridBagConstraints.BOTH)
                                            .build());
        final JLabel wSheetTemplatesLabel = new JLabel("Sheet Templates");
        wSheetTemplatesLabel.setToolTipText("Available templates. Select the templates that must be used.");
        wPanel.add(wSheetTemplatesLabel,
                   GridBagConstraintsBuilder.builder()
                                            .gridx(0)
                                            .gridy(1)
                                            .insets(5, 5, 0, 0)
                                            .anchor(GridBagConstraints.LINE_START)
                                            .build());

        wPanel.add(wSheetTemplates,
                   GridBagConstraintsBuilder.builder()
                                            .gridx(0)
                                            .gridy(2)
                                            .weightx(1.0)
                                            .weighty(1.0)
                                            .insets(5, 20, 0, 5)
                                            .fill(GridBagConstraints.BOTH)
                                            .build());
        wPanel.setPreferredSize(new Dimension(525, 200));
        return wPanel;
    }

    private JComponent buildTopRight() {
        return new JScrollPane(wSheetTemplate);
    }

    private JComponent buildDown() {
        final JTabbedPane wTabbedPane = new JTabbedPane();
        wTabbedPane.addTab("Check Import Files",
                           new CheckImportFilesPanel(this));

        wTabbedPane.addTab("Generate Template File",
                           new GenerateTemplateFilePanel(this));
        return wTabbedPane;
    }

    private final void buildMenus() {
        final JMenuBar wMenuBar = new JMenuBar();
        setJMenuBar(wMenuBar);
        buildFileMenu(wMenuBar);
        wMenuBar.add(Box.createGlue());
        buildHelpMenu(wMenuBar);
    }

    private static void buildFileMenu(JMenuBar wMenuBar) {
        final JMenu wMenu = new JMenu("File");
        wMenuBar.add(wMenu);
        final JMenuItem wQuit = new JMenuItem("Quit");
        wQuit.addActionListener(e -> System.exit(0));
        wMenu.add(wQuit);
    }

    private void buildHelpMenu(JMenuBar wMenuBar) {
        final JMenu wMenu = new JMenu("Help");
        wMenuBar.add(wMenu);

        final JMenuItem wMenuItem = new JMenuItem("About");
        wMenuItem.addActionListener(e -> showAbout());
        wMenu.add(wMenuItem);
    }

    protected void showAbout() {
        if (wAbout == null) {
            wAbout = new About("ImpEx Toolbox");
            wAbout.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    wAbout = null;
                }
            });
            final Rectangle selfBounds = getBounds();
            final Rectangle aboutBounds = wAbout.getBounds();
            wAbout.setLocation((int) (selfBounds.x + (selfBounds.getWidth() - aboutBounds.getWidth()) / 2),
                               (int) (selfBounds.y + (selfBounds.getHeight() - aboutBounds.getHeight()) / 2));

        }
        wAbout.setVisible(true);
    }
}