package cdc.impex.tools.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.Importer;
import cdc.impex.imports.WorkbookImporter;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.IssuesHandler;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactory;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.ui.swing.IssuesPanel;
import cdc.ui.swing.FileNameSuffixFilter;
import cdc.ui.swing.GridBagConstraintsBuilder;
import cdc.ui.swing.app.ExceptionDialog;
import cdc.ui.swing.enums.EnumTypeComboBoxModel;
import cdc.ui.swing.progress.SwingProgressController;
import cdc.ui.swing.props.PropertiesPanel;
import cdc.ui.swing.widgets.HtmlFrame;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressEvent;
import cdc.util.files.Files;

/**
 * Sub-panel used to check import files.
 *
 * @author Damien Carbonne
 */
class CheckImportFilesPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(CheckImportFilesPanel.class);
    static final Set<IssuesFormat> ISSUES_FORMATS = EnumSet.of(IssuesFormat.CSV, IssuesFormat.XLSX);
    private final ImpExToolboxFrame wFrame;
    private final JComboBox<IssuesFormat> wFormat =
            new JComboBox<>(EnumTypeComboBoxModel.create(IssuesFormat.class)
                                                 .withPredicate(ISSUES_FORMATS::contains));
    private final JButton wSelectAndCheck = new JButton("Select and check files");
    private final IssuesPanel wIssues = new IssuesPanel();
    private File inputDir = Files.currentDir();

    public CheckImportFilesPanel(ImpExToolboxFrame frame) {
        this.wFrame = frame;
        setLayout(new GridBagLayout());

        final PropertiesPanel wProperties = new PropertiesPanel();

        add(wProperties,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(0)
                                     .weightx(1.0)
                                     .weighty(0.0)
                                     .insets(5, 5, 0, 5)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());

        wFormat.setSelectedItem(IssuesFormat.XLSX);
        wProperties.addProperty("Issues format", wFormat, false);

        wSelectAndCheck.addActionListener(e -> selectAndCheckFiles());

        add(wSelectAndCheck,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(1)
                                     .weightx(1.0)
                                     .insets(5, 0, 5, 5)
                                     .anchor(GridBagConstraints.LINE_END)
                                     .build());

        add(new JLabel("Issues"),
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(2)
                                     .insets(0, 5, 5, 5)
                                     .anchor(GridBagConstraints.LINE_START)
                                     .build());

        add(wIssues,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(3)
                                     .weightx(1.0)
                                     .weighty(1.0)
                                     .insets(0, 20, 5, 5)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());
    }

    private void selectAndCheckFiles() {
        final JFileChooser wChooser = new JFileChooser();
        wChooser.setMultiSelectionEnabled(true);
        wChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        final FileNameSuffixFilter filter = new FileNameSuffixFilter("*.xslx, *.csv",
                                                                     "xlsx",
                                                                     "csv");
        wChooser.addChoosableFileFilter(filter);
        wChooser.setAcceptAllFileFilterUsed(false);

        wChooser.setCurrentDirectory(inputDir);
        final int result = wChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            inputDir = wChooser.getCurrentDirectory();
            check(wChooser.getSelectedFiles());
        }
    }

    private void check(File... files) {
        final IssuesCollector<Issue> issuesCollector = new IssuesCollector<>();

        final List<Future<?>> futures = new ArrayList<>();
        final ExecutorService executor = Executors.newFixedThreadPool(1);

        for (final File file : files) {
            final FileChecker checker = new FileChecker(file, issuesCollector);
            futures.add(executor.submit(checker));
        }

        final IssuesSaver saver = new IssuesSaver(issuesCollector);
        futures.add(executor.submit(saver));
    }

    /**
     * SwingWorker that checks one file.
     *
     * @author Damien Carbonne
     */
    private class FileChecker extends SwingWorker<Void, Object> {
        private final File file;
        private final IssuesCollector<Issue> issuesCollector;
        private final SwingProgressController controller;

        public FileChecker(File file,
                           IssuesCollector<Issue> issuesCollector) {
            this.file = file;
            this.issuesCollector = issuesCollector;
            this.controller = new SwingProgressController(wFrame, "Check " + file);
        }

        @Override
        protected Void doInBackground() throws IOException {
            LOGGER.info("FileChecker.doInBackground()");
            controller.setMillisToPopup(100);

            final ProgressController progressPublisher = new ProgressController() {
                @Override
                public void onProgress(ProgressEvent event) {
                    LOGGER.info("onProgress({})", event);
                    publish(event);
                }

                @Override
                public boolean isCancelled() {
                    // TODO
                    return false;
                }
            };

            final IssuesHandler<Issue> issuesPublisher = i -> {
                issuesCollector.issue(i);
                publish(i);
            };
            final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
            final Importer importer = factory.createImporter(file);
            importer.importData(file,
                                wFrame.wSheetTemplates.getModel().getSelectedTemplates(),
                                WorkbookImporter.QUIET_VOID,
                                issuesPublisher,
                                progressPublisher);
            LOGGER.info("FileChecker.doInBackground() done");
            return null;
        }

        @Override
        protected void process(List<Object> chunks) {
            for (final Object chunk : chunks) {
                if (chunk instanceof Issue) {
                    wIssues.getModel().add((Issue) chunk);
                } else if (chunk instanceof ProgressEvent) {
                    controller.onProgress((ProgressEvent) chunk);
                }
            }
        }

        @Override
        protected void done() {
            LOGGER.info("FileChecker.done()");
            try {
                get();
            } catch (final ExecutionException e) {
                final Throwable cause = e.getCause();
                ExceptionDialog.showExceptionDialog(wFrame,
                                                    cause,
                                                    "Failed to check '" + file + "'",
                                                    null);
            } catch (final InterruptedException e) {
                LOGGER.catching(e);
            }
        }
    }

    /**
     * SwingWorker that saves issues.
     *
     * @author Damien Carbonne
     */
    private class IssuesSaver extends SwingWorker<Void, Void> {
        private final IssuesCollector<Issue> issuesCollector;
        private File issuesFile;

        public IssuesSaver(IssuesCollector<Issue> issuesCollector) {
            this.issuesCollector = issuesCollector;
        }

        @Override
        protected Void doInBackground() throws IOException {
            LOGGER.info("IssuesSaver.doInBackground()");
            issuesFile = new File(wFrame.wOutputDir.getFile(),
                                  "issues." + wFormat.getSelectedItem().toString().toLowerCase());
            final IssuesIoFactory issuesFactory = new IssuesIoFactory(IssuesIoFactoryFeatures.UTC_FASTEST);
            final IssuesWriter issuesWriter = issuesFactory.createIssuesWriter(issuesFile);
            final SwingProgressController controller = new SwingProgressController(wFrame, "Save issues to " + issuesFile);
            controller.setMillisToPopup(100);
            issuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesCollector.getIssues()),
                              OutSettings.ALL_DATA_ANSWERS,
                              controller);
            LOGGER.info("IssuesSaver.doInBackground() done");
            HtmlFrame.showFrame(CheckImportFilesPanel.this,
                                "Generated issues file",
                                "Generated",
                                issuesFile);
            return null;
        }

        @Override
        protected void done() {
            LOGGER.info("IssuesSaver.done()");
            try {
                get();
            } catch (final ExecutionException e) {
                final Throwable cause = e.getCause();
                ExceptionDialog.showExceptionDialog(wFrame,
                                                    cause,
                                                    "Failed to save issues to '" + issuesFile + "'",
                                                    null);
            } catch (final InterruptedException e) {
                LOGGER.catching(e);
            }
        }
    }
}