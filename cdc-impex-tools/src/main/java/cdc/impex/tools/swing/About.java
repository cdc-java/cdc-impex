package cdc.impex.tools.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cdc.ui.swing.GridBagConstraintsBuilder;
import cdc.ui.swing.SwingUtils;

class About extends JFrame {
    private static final long serialVersionUID = 1L;

    public About(String title) {
        setTitle("About " + title);
        setIconImage(SwingUtils.getApplicationImage());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        final JPanel wPanel = new JPanel();
        wPanel.setLayout(new GridBagLayout());
        wPanel.setOpaque(true);
        wPanel.setBackground(Color.WHITE);
        final JLabel wLabel = new JLabel();
        wLabel.setFont(wLabel.getFont().deriveFont(Font.PLAIN));
        final StringBuilder builder = new StringBuilder();
        builder.append("<html>")
               .append("<b>" + title + "</b><br>")
               .append("<b>Version:</b> " + cdc.impex.tools.Config.VERSION + " (Swing)<br>")
               .append("<br>")
               .append("(c) Copyright 2021, Damien Carbonne")
               .append("</html>");
        wLabel.setText(builder.toString());
        wPanel.add(wLabel,
                   GridBagConstraintsBuilder.builder()
                                            .gridx(1)
                                            .anchor(GridBagConstraints.FIRST_LINE_START)
                                            .fill(GridBagConstraints.HORIZONTAL)
                                            .weightx(1.0)
                                            .weighty(1.0)
                                            .insets(5, 5, 5, 5)
                                            .build());

        setContentPane(wPanel);
        pack();
    }
}