package cdc.impex.tools;

import cdc.impex.ImpExCatalog;

public final class Settings {
    private Settings() {
    }

    public static final ImpExCatalog CATALOG = new ImpExCatalog();
}