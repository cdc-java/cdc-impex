package cdc.impex.tools;

import java.util.function.Function;

import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.Usage;
import cdc.issues.IssueSeverity;
import cdc.validation.checkers.defaults.IsInRange;

final class Demo {
    private Demo() {
    }

    public static void elaborate() {
        // Ignore
    }

    static final ColumnTemplate<String> ID =
            ColumnTemplate.builder(String.class)
                          .name("Id")
                          .description("Identifier")
                          .usage(Usage.MANDATORY_RO_ID)
                          .importConverter(Function.identity())
                          .build();

    static final ColumnTemplate<String> RO =
            ColumnTemplate.builder(String.class)
                          .name("Read Only")
                          .description("Read-only attribute")
                          .usage(Usage.MANDATORY_RO_ATT)
                          .importConverter(Function.identity())
                          .build();

    static final ColumnTemplate<String> STRING =
            ColumnTemplate.builder(String.class)
                          .name("String")
                          .description("Define a string in this column")
                          .importConverter(Function.identity())
                          .build();
    static final ColumnTemplate<Float> FLOAT =
            ColumnTemplate.builder(float.class)
                          .name("Float")
                          .checker(IsInRange.from(0.0f, 10.5f))
                          .checkFailureSeverity(IssueSeverity.MINOR)
                          .description("Define\n a\n float \nin \nthis\n column")
                          .importConverter(Float::valueOf)
                          .build();
    static final ColumnTemplate<Boolean> BOOLEAN =
            ColumnTemplate.builder(boolean.class)
                          .name("Boolean")
                          .importConverter(Boolean::valueOf)
                          .build();
    static final ColumnTemplate<Double> DOUBLE =
            ColumnTemplate.builder(double.class)
                          .name("Double")
                          .checker(IsInRange.from(0.0, 10.0))
                          .importConverter(Double::valueOf)
                          .exportConverter(x -> {
                              if (((int) x.doubleValue()) % 2 == 0) {
                                  throw new IllegalArgumentException();
                              } else {
                                  return Double.toString(x);
                              }
                          })
                          .build();
    static final ColumnTemplate<String> META =
            ColumnTemplate.builder(String.class)
                          .pattern("Meta:.+")
                          .description("Define a set of meta data columns")
                          .build();

    static final SheetTemplate TEMPLATE1 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template1")
                         .description("Description of template 1.\nA very long long line that contains too many words that should be wrapped.")
                         .column(ID)
                         .column(RO)
                         .column(STRING)
                         .column(FLOAT)
                         .build();
    static final SheetTemplate TEMPLATE2 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template2")
                         .description("Description of template 2")
                         .column(BOOLEAN)
                         .column(DOUBLE)
                         .column(META)
                         .build();
    static {
        Settings.CATALOG.register(TEMPLATE1)
                        .register(TEMPLATE2);
    }
}