package cdc.impex.demos;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.templates.TemplateGenerator;

public class TemplateGenerationDemo extends DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(TemplateGenerationDemo.class);

    private static void generateTemplate(String filename,
                                         boolean skipActionColumn) throws IOException {
        LOGGER.info("Generate template file {}", filename);
        TemplateGenerator.generate(new File(filename),
                                   Arrays.asList(TEMPLATE1.getName(), TEMPLATE2.getName()),
                                   CATALOG,
                                   DemoSupport::instancier,
                                   skipActionColumn ? ImpExFactoryFeatures.BEST_NO_ACTION : ImpExFactoryFeatures.BEST);
    }

    public static void main(String... args) throws IOException {
        LOGGER.info("Start");
        generateTemplate("target/template.json", false);
        generateTemplate("target/template.xlsx", false);
        generateTemplate("target/template.xls", false);
        generateTemplate("target/template.csv", false);

        generateTemplate("target/template-no-action.json", true);
        generateTemplate("target/template-no-action.xlsx", true);
        generateTemplate("target/template-no-action.xls", true);
        generateTemplate("target/template-no-action.csv", true);

        LOGGER.info("End");
    }
}