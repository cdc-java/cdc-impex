package cdc.impex.demos;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.exports.Exporter;
import cdc.impex.exports.VerboseExporter;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.VoidIssuesHandler;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;

/**
 * Export dDemonstration.
 *
 * @author Damien Carbonne
 */
public class ExportDemo extends DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(ExportDemo.class);

    private static void exportData(String filename,
                                   IssuesCollector<Issue> issuesHandler,
                                   ImpExFactoryFeatures features) throws IOException {
        LOGGER.info("Export file {}", filename);
        final File file = new File(filename);
        final ImpExFactory factory = new ImpExFactory(features);
        final Exporter exporter = new VerboseExporter(factory.createExporter(file));

        final List<SheetTemplateInstance> templateInstances = List.of(TEMPLATE1_INSTANCE, TEMPLATE2_INSTANCE);

        exporter.exportData(file,
                            templateInstances,
                            CATALOG.createWorkbookExporterFor(TEMPLATE1.getName(), TEMPLATE2.getName()),
                            issuesHandler,
                            ProgressController.VERBOSE);
    }

    public static void main(String... args) throws IOException {
        LOGGER.info("Start");

        final IssuesCollector<Issue> issuesHandler =
                new IssuesCollector<>(VoidIssuesHandler.INSTANCE);

        // Workbooks
        exportData("target/export-best.xlsx", issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/export-best-no_action.xlsx", issuesHandler, ImpExFactoryFeatures.BEST_NO_ACTION);
        exportData("target/export-best.xls", issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/export-best.csv", issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/export-fastest.xlsx", issuesHandler, ImpExFactoryFeatures.FASTEST);
        exportData("target/export-fastest.xls", issuesHandler, ImpExFactoryFeatures.FASTEST);
        exportData("target/export-fastest.csv", issuesHandler, ImpExFactoryFeatures.FASTEST);
        // JSON
        exportData("target/export-best.json", issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/export-best-no-action.json", issuesHandler, ImpExFactoryFeatures.BEST_NO_ACTION);
        exportData("target/export-fastest.json", issuesHandler, ImpExFactoryFeatures.FASTEST);
        // XML
        exportData("target/export-best.xml", issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/export-best-no-action.xml", issuesHandler, ImpExFactoryFeatures.BEST_NO_ACTION);
        exportData("target/export-fastest.xml", issuesHandler, ImpExFactoryFeatures.FASTEST);

        LOGGER.info("Save issues");
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/export-issues.xlsx"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/export-issues.csv"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/export-issues.json"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/export-issues.xml"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);

        LOGGER.info("End");
    }
}