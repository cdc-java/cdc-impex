package cdc.impex.demos;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.ImportAnalyzer;
import cdc.impex.templates.SheetTemplate;
import cdc.util.events.ProgressController;

/**
 * Import analysis demonstration.
 *
 * @author Damien Carbonne
 */
public class ImportAnalyzerDemo extends DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(ImportAnalyzerDemo.class);

    private static void analyzeImport(String filename,
                                      String issuesFilename,
                                      Set<SheetTemplate> templates) throws IOException {
        LOGGER.info("=================================================");
        LOGGER.info("Analyze {} into {} with templates {}", filename, issuesFilename, templates);
        final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.BEST);
        final File file = new File(filename);
        final ImportAnalyzer analyzer = factory.createImportAnalyzer(file);
        final File issuesFile = new File(issuesFilename);
        analyzer.analyze(file,
                         templates,
                         issuesFile,
                         ProgressController.VERBOSE);
        LOGGER.info("Generated {}", issuesFile);
    }

    public static void main(String... args) throws IOException {

        LOGGER.info("Start");

        for (final File file : SAMPLE_FILES) {
            analyzeImport(file.getPath(),
                          "target/" + file.getName().replace('.', '-') + "-analysis-issues-a.xlsx",
                          Collections.emptySet());

            analyzeImport(file.getPath(),
                          "target/" + file.getName().replace('.', '-') + "-analysis-issues-b.xlsx",
                          CATALOG.getTemplatesAsSet(TEMPLATE1.getName(), TEMPLATE2.getName()));
        }

        LOGGER.info("End");
    }
}