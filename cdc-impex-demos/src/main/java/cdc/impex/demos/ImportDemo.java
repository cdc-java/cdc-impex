package cdc.impex.demos;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.imports.Importer;
import cdc.impex.templates.SheetTemplate;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.VoidIssuesHandler;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;

/**
 * Import demonstration.
 *
 * @author Damien Carbonne
 */
public class ImportDemo extends DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(ImportDemo.class);

    private static void importData(String filename,
                                   Set<SheetTemplate> templates,
                                   IssuesCollector<Issue> issuesHandler) throws IOException {
        LOGGER.info("Import {} with templates {}", filename, templates);
        final ImpExFactory factory = new ImpExFactory(ImpExFactoryFeatures.FASTEST);
        final Importer importer = factory.createImporter(new File(filename));
        importer.importData(new File(filename),
                            templates,
                            CATALOG.createWorkbookImporterFor(TEMPLATE1.getName(), TEMPLATE2.getName()),
                            issuesHandler,
                            ProgressController.VERBOSE);
    }

    public static void main(String... args) throws IOException {

        LOGGER.info("Start");

        final IssuesCollector<Issue> issuesHandler =
                new IssuesCollector<>(VoidIssuesHandler.INSTANCE);

        for (final File file : SAMPLE_FILES) {
            importData(file.getPath(),
                       Collections.emptySet(),
                       issuesHandler);

            importData(file.getPath(),
                       CATALOG.getTemplatesAsSet(TEMPLATE1.getName(), TEMPLATE2.getName()),
                       issuesHandler);

            importData(file.getPath(),
                       CATALOG.getTemplatesAsSet(TEMPLATE1.getName()),
                       issuesHandler);
        }

        LOGGER.info("Save issues");
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/import-issues.xlsx"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/import-issues.json"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          new File("target/import-issues.xml"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);

        LOGGER.info("End");
    }
}