package cdc.impex.demos;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExFactory;
import cdc.impex.ImpExFactoryFeatures;
import cdc.impex.exports.ActiveExporter;
import cdc.impex.exports.CheckedActiveExporter;
import cdc.impex.exports.ExportRow;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.VoidIssuesHandler;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;

public class ActiveExportDemo extends DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(ActiveExportDemo.class);

    private static void exportData(String filename,
                                   int rows,
                                   IssuesCollector<Issue> issuesHandler,
                                   ImpExFactoryFeatures features) throws IOException {
        LOGGER.info("Export file {}", filename);
        final File file = new File(filename);
        final ImpExFactory factory = new ImpExFactory(features);
        final ActiveExporter exporter = new CheckedActiveExporter(factory.createActiveExporter(file));

        exporter.beginExport(file, issuesHandler, ProgressController.VERBOSE);
        exporter.beginSheet(TEMPLATE1, rows);
        for (int index = 0; index < rows; index++) {
            final ExportRow row = exporter.nextRow();
            DemoSupport.DemoExporter.fill(row);
            exporter.addRow();
        }
        exporter.endSheet();
        exporter.beginSheet(TEMPLATE2_INSTANCE, rows);
        for (int index = 0; index < rows; index++) {
            final ExportRow row = exporter.nextRow();
            DemoSupport.DemoExporter.fill(row);
            exporter.addRow();
        }
        exporter.endSheet();
        exporter.endExport();
    }

    public static void main(String... args) throws IOException {
        LOGGER.info("Start");

        final IssuesCollector<Issue> issuesHandler =
                new IssuesCollector<>(VoidIssuesHandler.INSTANCE);

        final int rows = 200;
        // Workbooks
        exportData("target/active-export-best.xlsx", rows, issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/active-export-best.xls", rows, issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/active-export-best.csv", rows, issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/active-export-fastest.xlsx", rows, issuesHandler, ImpExFactoryFeatures.FASTEST);
        exportData("target/active-export-fastest.xls", rows, issuesHandler, ImpExFactoryFeatures.FASTEST);
        exportData("target/active-export-fastest.csv", rows, issuesHandler, ImpExFactoryFeatures.FASTEST);
        // JSON
        exportData("target/active-export-best.json", rows, issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/active-export-fastest.json", rows, issuesHandler, ImpExFactoryFeatures.FASTEST);
        // XML
        exportData("target/active-export-best.xml", rows, issuesHandler, ImpExFactoryFeatures.BEST);
        exportData("target/active-export-fastest.xml", rows, issuesHandler, ImpExFactoryFeatures.FASTEST);

        exportData("target/active-export-150_000.xls", 150_000, issuesHandler, ImpExFactoryFeatures.FASTEST);

        final File issues = new File("target/active-export-issues.xlsx");
        LOGGER.info("Save issues: {}", issues);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issuesHandler.getIssues()),
                          OutSettings.ALL_DATA_ANSWERS,
                          issues,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);

        LOGGER.info("End");
    }
}