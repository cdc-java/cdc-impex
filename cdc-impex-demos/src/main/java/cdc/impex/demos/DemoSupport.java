package cdc.impex.demos;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.impex.ImpExCatalog;
import cdc.impex.ImpExStatus;
import cdc.impex.exports.CheckedSheetExporter;
import cdc.impex.exports.ExportRow;
import cdc.impex.imports.BatchSheetImporter;
import cdc.impex.imports.SheetImporter;
import cdc.impex.templates.ColumnTemplate;
import cdc.impex.templates.SheetTemplate;
import cdc.impex.templates.SheetTemplateInstance;
import cdc.impex.templates.Usage;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesHandler;
import cdc.office.ss.ContentValidation;
import cdc.office.tables.Header;
import cdc.util.lang.Checks;
import cdc.validation.checkers.defaults.IsInRange;

class DemoSupport {
    private static final Logger LOGGER = LogManager.getLogger(DemoSupport.class);

    protected DemoSupport() {
    }

    enum Enum1 {
        A,
        B,
        C
    }

    /**
     * An enum that can not be encoded in an explicit validation list.
     */
    enum Enum2 {
        LONG_NAME_00,
        LONG_NAME_01,
        LONG_NAME_02,
        LONG_NAME_03,
        LONG_NAME_04,
        LONG_NAME_05,
        LONG_NAME_06,
        LONG_NAME_07,
        LONG_NAME_08,
        LONG_NAME_09,
        LONG_NAME_10,
        LONG_NAME_11,
        LONG_NAME_12,
        LONG_NAME_13,
        LONG_NAME_14,
        LONG_NAME_15,
        LONG_NAME_16,
        LONG_NAME_17,
        LONG_NAME_18,
        LONG_NAME_19,
        LONG_NAME_20,
        LONG_NAME_21,
        LONG_NAME_22,
        LONG_NAME_23,
        LONG_NAME_24,
        LONG_NAME_25,
        LONG_NAME_26,
        LONG_NAME_27,
        LONG_NAME_28,
        LONG_NAME_29
    }

    static final File[] SAMPLE_FILES = {
            new File("src/main/resources/demo-import.json"),
            new File("src/main/resources/demo-import.csv"),
            new File("src/main/resources/demo-import.xlsx")
    };

    static class VerboseBatchImporter extends BatchSheetImporter {
        public VerboseBatchImporter(SheetImporter delegate) {
            super(delegate);
        }

        @Override
        public void batch() {
            LOGGER.info("batch");
        }
    }

    static class DemoExporter extends CheckedSheetExporter {
        private final int rows;
        private int next = 0;

        public DemoExporter(int rows) {
            this.rows = rows;
        }

        public static void fill(ExportRow row) {
            final Header header = row.getTemplateInstance().getHeader();
            final SheetTemplate template = row.getTemplate();
            for (final String name : header.getSortedNames()) {
                final ColumnTemplate<?> column = template.getMatchingColumn(name);
                final Class<?> dataType = column.getDataType();
                final Object data;
                if (dataType.equals(String.class)) {
                    if (column.isPattern()) {
                        data = "String " + row.getNumber() + " for pattern column";
                    } else {
                        data = "String " + row.getNumber();
                    }
                } else if (dataType.equals(Boolean.class) || dataType.equals(boolean.class)) {
                    data = true;
                } else if (dataType.equals(Byte.class) || dataType.equals(byte.class)) {
                    data = (byte) row.getNumber();
                } else if (dataType.equals(Short.class) || dataType.equals(short.class)) {
                    data = (short) row.getNumber();
                } else if (dataType.equals(Integer.class) || dataType.equals(int.class)) {
                    data = row.getNumber();
                } else if (dataType.equals(Long.class) || dataType.equals(long.class)) {
                    data = (long) row.getNumber();
                } else if (dataType.equals(Double.class) || dataType.equals(double.class)) {
                    data = (double) row.getNumber();
                } else if (dataType.equals(List.class)) {
                    data = List.of(1.0, 2.0);
                } else if (dataType.equals(Float.class) || dataType.equals(float.class)) {
                    data = (float) row.getNumber();
                } else if (dataType.isEnum()) {
                    data = dataType.getEnumConstants()[row.getNumber() % dataType.getEnumConstants().length];
                } else {
                    data = null;
                }
                row.setData(name, data);
            }
        }

        @Override
        public void beginSheetExport(SheetTemplateInstance templateInstance,
                                     IssuesHandler<Issue> issuesHandler) {
            super.beginSheetExport(templateInstance, issuesHandler);
            next = 1;
        }

        @Override
        public int getNumberOfRemainingRows() {
            checkStatus(ImpExStatus.SHEET);
            return rows - next + 1;
        }

        @Override
        public void exportRow(ExportRow row,
                              IssuesHandler<Issue> issuesHandler) {
            checkStatus(ImpExStatus.SHEET);

            Checks.assertTrue(next <= rows, "No more rows");
            fill(row);
            next++;
        }
    }

    static final ColumnTemplate<String> ID =
            ColumnTemplate.builder(String.class)
                          .name("Id")
                          .description("Identifier whose length is in range [5,10]")
                          .usage(Usage.MANDATORY_RO_ID)
                          .cvType(ContentValidation.Type.TEXT_LENGTH)
                          .cvOperator(ContentValidation.Operator.BETWEEN)
                          .cvValues("5", "10")
                          .build();

    static final ColumnTemplate<String> DERIVED =
            ColumnTemplate.builder(String.class)
                          .name("Derived")
                          .description("Derived attribute.")
                          .usage(Usage.DERIVED_RO_ATT)
                          .build();

    static final ColumnTemplate<String> RO =
            ColumnTemplate.builder(String.class)
                          .name("Read Only")
                          .description("Read-only attribute")
                          .usage(Usage.MANDATORY_RO_ATT)
                          .build();

    static final ColumnTemplate<String> STRING =
            ColumnTemplate.builder(String.class)
                          .name("String")
                          .description("Define a string in this column")
                          .build();
    static final ColumnTemplate<String> META =
            ColumnTemplate.builder(String.class)
                          .pattern("Meta:.+")
                          .description("Define a set of meta data columns")
                          .build();
    static final ColumnTemplate<Float> FLOAT =
            ColumnTemplate.builder(float.class)
                          .name("Float")
                          .checker(IsInRange.from(0.0f, 10.5f))
                          .checkFailureSeverity(IssueSeverity.MINOR)
                          .description("Define\n a\n float \nin \nthis\n column")
                          .cvOperator(ContentValidation.Operator.BETWEEN)
                          .cvValues("0.0", "10.5")
                          .build();
    static final ColumnTemplate<Boolean> BOOLEAN =
            ColumnTemplate.builder(boolean.class)
                          .name("Boolean")
                          .build();
    static final ColumnTemplate<Enum1> ENUM1 =
            ColumnTemplate.builder(Enum1.class)
                          .name("Enum1")
                          .build();
    static final ColumnTemplate<Enum2> ENUM2 =
            ColumnTemplate.builder(Enum2.class)
                          .name("Enum2")
                          .description("A enum that can not be encoded in an explicit validation list.")
                          .build();
    static final ColumnTemplate<Double> DOUBLE =
            ColumnTemplate.builder(double.class)
                          .name("Double")
                          .checker(IsInRange.from(0.0, 10.0))
                          .exportConverter(x -> {
                              if (((int) x.doubleValue()) % 2 == 0) {
                                  throw new IllegalArgumentException();
                              } else {
                                  return Double.toString(x);
                              }
                          })
                          .cvOperator(ContentValidation.Operator.BETWEEN)
                          .cvValues("0.0", "10.0")
                          .build();

    static final ColumnTemplate<List<Double>> DOUBLE_LIST =
            ColumnTemplate.listBuilder(double.class)
                          .name("Double List")
                          .exportConverter(x -> Double.toString(x), " ")
                          .importConverter(Double::valueOf, " \n")
                          .description("List of doubles, separated by a single space or newline")
                          .build();

    static final SheetTemplate TEMPLATE1 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template1")
                         .description("Description of template 1")
                         .column(ID)
                         .column(RO)
                         .column(STRING)
                         .column(FLOAT)
                         .column(ENUM1)
                         .column(ENUM2)
                         .column(DERIVED)
                         .build();
    static final SheetTemplate TEMPLATE2 =
            SheetTemplate.builder()
                         .domain("Demo")
                         .name("Template2")
                         .description("Description of template 2 that uses patterns")
                         .column(BOOLEAN)
                         .column(DOUBLE)
                         .column(DOUBLE_LIST)
                         .column(META)
                         .build();
    static final SheetTemplateInstance TEMPLATE1_INSTANCE =
            SheetTemplateInstance.replace(TEMPLATE1);
    static final SheetTemplateInstance TEMPLATE2_INSTANCE =
            SheetTemplateInstance.replace(TEMPLATE2, "Meta:Meta1", "Meta:Meta2");

    public static SheetTemplateInstance instancier(SheetTemplate template) {
        if (template == TEMPLATE1) {
            return TEMPLATE1_INSTANCE;
        } else if (template == TEMPLATE2) {
            return TEMPLATE2_INSTANCE;
        } else {
            return null;
        }
    }

    static final ImpExCatalog CATALOG =
            new ImpExCatalog().register(TEMPLATE1, new VerboseBatchImporter(SheetImporter.VERBOSE_VOID).setBatchSize(4))
                              .register(TEMPLATE2, new VerboseBatchImporter(SheetImporter.VERBOSE_VOID).setBatchSize(4))
                              .register(TEMPLATE1, new DemoExporter(250))
                              .register(TEMPLATE2, new DemoExporter(500));
}