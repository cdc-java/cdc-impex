package cdc.impex.demos;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class DemoTest {
    @Test
    void testImport() throws IOException {
        ImportDemo.main();
        assertTrue(true);
    }

    @Test
    void testExport() throws IOException {
        ExportDemo.main();
        assertTrue(true);
    }

    @Test
    void testActiveExport() throws IOException {
        ActiveExportDemo.main();
        assertTrue(true);
    }

    @Test
    void testTemplateGeneration() throws IOException {
        TemplateGenerationDemo.main();
        assertTrue(true);
    }

    @Test
    void testImportAnalyzer() throws IOException {
        ImportAnalyzerDemo.main();
        assertTrue(true);
    }
}